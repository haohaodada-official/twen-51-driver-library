#include <TWEN32F0xx.h>
#include "HardwareSerial.h"
#include "myLib/MB_Master.h"

MBMaster mb(&Serial2,9600,USART_PARITY_EVEN);

int8_t temp;
uint8_t buf[10];

int main(void)
{
    TW32F0xx_init();
    Serial1.begin(9600);
    mb.Init();
    while(1){
        temp = mb.ReqReadCoils(1, 0, 8, buf, 500);
        if(temp == 1){
            Serial1.println("ReadCoils ok");
            Serial1.println(buf[0]);
        }else{
            Serial1.println("ReadCoils err");
        }
        delay(1000);
    }
    return 1;
}







