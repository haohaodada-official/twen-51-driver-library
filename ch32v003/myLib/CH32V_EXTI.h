#ifndef _CH32V_EXTI_H_
#define _CH32V_EXTI_H_

#include <CH32V003.h>

typedef void (*voidFuncPtr)(void);
volatile static voidFuncPtr exti_fun[10] = {NULL};

//========================================================================
// 描述: 引脚中断初始化函数.
// 参数: pin：引脚; interrupt_type:触发类型; userFunc:回调函数.
// 返回: none.
//========================================================================
void Pin_attachInterrupt(PIN_Name pin, EXTITrigger_TypeDef interrupt_type, void (*userFunc)(void))
{
    uint8_t EXT_LINEX;

    EXTI_InitTypeDef EXTI_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);

    if (interrupt_type == EXTI_Trigger_Rising)
    {
        pinMode(pin, GPIO_Mode_IPD);
    }
    else if (interrupt_type == EXTI_Trigger_Falling)
    {
        pinMode(pin, GPIO_Mode_IPU);
    }
    else
    {
        pinMode(pin, GPIO_Mode_IN_FLOATING);
    }

    EXT_LINEX = pin % 8;
    if(pin <= PA7){
        GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, EXT_LINEX);
    }else if(pin <= PC7){
        GPIO_EXTILineConfig(GPIO_PortSourceGPIOC, EXT_LINEX);
    }else if(pin <= PD7){
        GPIO_EXTILineConfig(GPIO_PortSourceGPIOD, EXT_LINEX);
    }

    EXTI_InitStructure.EXTI_Line = (1 << EXT_LINEX);
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = interrupt_type;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);

    NVIC_InitStructure.NVIC_IRQChannel = EXTI7_0_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
    exti_fun[EXT_LINEX] = userFunc;
}

//========================================================================
// 描述: 引脚中断处理函数.
// 参数: none.
// 返回: none.
//========================================================================
#ifdef __cplusplus
extern "C" {
#endif

void EXTI7_0_IRQHandler(void) __attribute__((interrupt("WCH-Interrupt-fast")));
void EXTI7_0_IRQHandler(void)
{
    if (EXTI_GetITStatus(EXTI_Line0) != RESET)
    {
        exti_fun[0]();
        EXTI_ClearITPendingBit(EXTI_Line0);
    }else if(EXTI_GetITStatus(EXTI_Line1) != RESET){
        exti_fun[1]();
        EXTI_ClearITPendingBit(EXTI_Line1);
    }else if(EXTI_GetITStatus(EXTI_Line2) != RESET){
        exti_fun[2]();
        EXTI_ClearITPendingBit(EXTI_Line2);
    }else if(EXTI_GetITStatus(EXTI_Line3) != RESET){
        exti_fun[3]();
        EXTI_ClearITPendingBit(EXTI_Line3);
    }else if(EXTI_GetITStatus(EXTI_Line4) != RESET){
        exti_fun[4]();
        EXTI_ClearITPendingBit(EXTI_Line4);
    }else if(EXTI_GetITStatus(EXTI_Line5) != RESET){
        exti_fun[5]();
        EXTI_ClearITPendingBit(EXTI_Line5);
    }else if(EXTI_GetITStatus(EXTI_Line6) != RESET){
        exti_fun[6]();
        EXTI_ClearITPendingBit(EXTI_Line6);
    }else if(EXTI_GetITStatus(EXTI_Line7) != RESET){
        exti_fun[7]();
        EXTI_ClearITPendingBit(EXTI_Line7);
    }else if(EXTI_GetITStatus(EXTI_Line8) != RESET){
        exti_fun[8]();
        EXTI_ClearITPendingBit(EXTI_Line8);
    }else if(EXTI_GetITStatus(EXTI_Line9) != RESET){
        exti_fun[9]();
        EXTI_ClearITPendingBit(EXTI_Line9);
    }
}

#ifdef __cplusplus
}
#endif

#endif //CH32V003_exti.h