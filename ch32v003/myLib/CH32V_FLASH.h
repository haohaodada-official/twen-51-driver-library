/****************************************************************************
 * @file CH32V_FLASH.h
 * @brief ch32v003的内部flash读写库
 * @details ch32v003的闪存地址从0x08000000到0x080000FF。
 *           分为256页(页0 - 页255),每页64字节大小。
 * @version 0.1
 * @date 2022-10-25
 * @author Blue
 * @copyright Copyright (c) 2021 TWen51 Technology Co., Ltd.
 ****************************************************************************/
#ifndef _CH32V_FLASH_H
#define _CH32V_FLASH_H

#include "CH32V003.h"
#include "string.h"

/* Global define */
#define FLASH_SIZE             (256*64)     //整个flash大小
#define FLASH_PAGE_NUM         256          //总共页数
#define FLASH_PAGE_SIZE        64           //每页大小
#define FLASH_START_ADDR       ((uint32_t)0x08000000)   //flash起始地址
#define FLASH_END_ADDR         ((uint32_t)0x08004000)   //flash结束地址

#define FLASH_PAGES_TO_BE_PROTECTED  FLASH_WRProt_Pages240to255

class INTERNAL_FLASH {
public:
	INTERNAL_FLASH(void);
	~INTERNAL_FLASH(void);
		
	FLASH_Status erase_page(uint32_t Page_Address);
	FLASH_Status FLASH_erase_page_all(void);  //擦除flash全部数据

	/* flash读函数 */
	FLASH_Status read(uint32_t data_addr, uint8_t *buf, uint32_t length);
	FLASH_Status readChar(uint32_t data_addr,char *Data,uint32_t count);
	FLASH_Status readUChar(uint32_t data_addr,uint8_t *Data,uint32_t count);
	FLASH_Status readShort(uint32_t data_addr,int16_t *Data,uint32_t count);
	FLASH_Status readUShort(uint32_t data_addr,uint16_t *Data,uint32_t count);
	FLASH_Status readInt(uint32_t data_addr,int32_t *Data,uint32_t count);
	FLASH_Status readUInt(uint32_t data_addr,uint32_t *Data,uint32_t count);
	FLASH_Status readLong64(uint32_t data_addr,int64_t *Data,uint32_t count);
	FLASH_Status readULong64(uint32_t data_addr,uint64_t *Data,uint32_t count);
	FLASH_Status readFloat(uint32_t data_addr,float *Data,uint32_t count);
	FLASH_Status readDouble(uint32_t data_addr,double *Data,uint32_t count);

	int8_t readChar(uint32_t data_addr);
	uint8_t readUChar(uint32_t data_addr);
	int16_t readShort(uint32_t data_addr);
	uint16_t readUShort(uint32_t data_addr);
	int32_t readInt(uint32_t data_addr);
	uint32_t readUInt(uint32_t data_addr);
	int64_t readLong64(uint32_t data_addr);
	uint64_t readULong64(uint32_t data_addr);
	float readFloat(uint32_t data_addr);
	double readDouble(uint32_t data_addr);
	template <class T> T readALL (uint32_t address, T &);

	/* flash写函数 */
	void write(uint32_t data_addr, uint8_t *buf, uint16_t num_bytes);
	FLASH_Status writeChar(uint32_t data_addr,char *Data,uint32_t count);
	FLASH_Status writeUChar(uint32_t data_addr,uint8_t *Data,uint32_t count);
	FLASH_Status writeShort(uint32_t data_addr,int16_t *Data,uint32_t count);
	FLASH_Status writeUShort(uint32_t data_addr,uint16_t *Data,uint32_t count);
	FLASH_Status writeInt(uint32_t data_addr,int32_t *Data,uint32_t count);
	FLASH_Status writeUInt(uint32_t data_addr,uint32_t *Data,uint32_t count);
	FLASH_Status writeLong64(uint32_t data_addr,int64_t *Data,uint32_t count);
	FLASH_Status writeULong64(uint32_t data_addr,uint64_t *Data,uint32_t count);
	FLASH_Status writeFloat(uint32_t data_addr,float *Data,uint32_t count);
	FLASH_Status writeDouble(uint32_t data_addr,double *Data,uint32_t count);

	FLASH_Status writeChar(uint32_t data_addr,char Data);
	FLASH_Status writeUChar(uint32_t data_addr,uint8_t Data);
	FLASH_Status writeShort(uint32_t data_addr,int16_t Data);
	FLASH_Status writeUShort(uint32_t data_addr,uint16_t Data);
	FLASH_Status writeInt(uint32_t data_addr,int32_t Data);
	FLASH_Status writeUInt(uint32_t data_addr,uint32_t Data);
	FLASH_Status writeLong64(uint32_t data_addr,int64_t Data);
	FLASH_Status writeULong64(uint32_t data_addr,uint64_t Data);
	FLASH_Status writeFloat(uint32_t data_addr,float Data);
	FLASH_Status writeDouble(uint32_t data_addr,double Data);

	/* 写半字函数 */
	FLASH_Status write_half_word(uint32_t data_addr, uint16_t buf);
	/* 写64字节函数 */
	void write_64bytes(uint32_t data_addr, uint8_t *dat);
protected:		
	volatile FLASH_Status FLASHStatus = FLASH_COMPLETE;
};

INTERNAL_FLASH::INTERNAL_FLASH(void)
{
}

INTERNAL_FLASH::~INTERNAL_FLASH() 
{
}

//========================================================================
// 描述: flash整片擦除.
// 参数: none
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::FLASH_erase_page_all(void)
{
	uint32_t WRPR_Value;
    FLASH_Unlock();
    WRPR_Value = FLASH_GetWriteProtectionOptionByte();    //写保护
    if ( (WRPR_Value & FLASH_PAGES_TO_BE_PROTECTED) != 0x00){
        FLASH_ClearFlag(FLASH_FLAG_BSY | FLASH_FLAG_EOP|FLASH_FLAG_WRPRTERR);
        FLASHStatus = FLASH_EraseAllPages();  //整片擦除
    }
    FLASH_Lock();
    return FLASHStatus;
}

//========================================================================
// 描述: flash页擦除.
// 参数: page：擦除哪一页（CH32V003一共256页(0-255)，每页64byte）
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::erase_page(uint32_t Page_Address)
{
    FLASH_Unlock_Fast();
    FLASH_ErasePage_Fast(Page_Address);  //擦除页
    FLASH_Lock_Fast();
    return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash从指定地址读出指定长度的数据.
// 参数: data_addr:地址; 
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::read(uint32_t data_addr, uint8_t *buf, uint32_t length)
{
	// memcpy((void*)buf,(const void*)(data_addr),(size_t)length);
	for(uint32_t i = 0; i < length;i++)
	{
		buf[i] = (*(__IO uint8_t*)(data_addr+i));
	}
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash读取多个有符号8位数据.
// 参数: data_addr:要读入的flash地址
//       Data:要读取的数据指针
//       count:要读取的数据长度  
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::readChar(uint32_t data_addr,char *Data,uint32_t count)
{
	read(data_addr, (uint8_t *)Data,  count);
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash读取多个无符号8位数据.
// 参数: data_addr:要读取的flash地址
//       Data:要读取的数据指针
//       count:要读取的数据长度  
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::readUChar(uint32_t data_addr,uint8_t *Data,uint32_t count)
{
	read(data_addr, (uint8_t *)Data,  count);
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash读取多个有符号16位数据.
// 参数: data_addr:要读取的flash地址
//       Data:要读取的数据指针
//       count:要读取的数据长度  
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::readShort(uint32_t data_addr,int16_t *Data,uint32_t count)
{
	read(data_addr, (uint8_t *)Data,  count*sizeof(int16_t));
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash读取多个无符号16位数据.
// 参数: data_addr:要读取的flash地址
//       Data:要读取的数据指针
//       count:要读取的数据长度  
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::readUShort(uint32_t data_addr,uint16_t *Data,uint32_t count)
{
	read(data_addr, (uint8_t *)Data,  count*sizeof(uint16_t));
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash读取多个有符号32位数据.
// 参数: data_addr:要读取的flash地址
//       Data:要读取的数据指针
//       count:要读取的数据长度  
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::readInt(uint32_t data_addr,int32_t *Data,uint32_t count)
{
	read(data_addr, (uint8_t *)Data,  count*sizeof(int32_t));
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash读取多个无符号32位数据.
// 参数: data_addr:要读取的flash地址
//       Data:要读取的数据指针
//       count:要读取的数据长度  
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::readUInt(uint32_t data_addr,uint32_t *Data,uint32_t count)
{
	read(data_addr, (uint8_t *)Data,  count*sizeof(uint32_t));
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash读取多个有符号64位数据.
// 参数: data_addr:要读取的flash地址
//       Data:要读取的数据指针
//       count:要读取的数据长度  
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::readLong64(uint32_t data_addr,int64_t *Data,uint32_t count)
{
	read(data_addr, (uint8_t *)Data,  count*sizeof(int64_t));
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash读取多个无符号64位数据.
// 参数: data_addr:要读取的flash地址
//       Data:要读取的数据指针
//       count:要读取的数据长度  
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::readULong64(uint32_t data_addr,uint64_t *Data,uint32_t count)
{
	read(data_addr, (uint8_t *)Data,  count*sizeof(uint64_t));
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash读取多个float类型数据.
// 参数: data_addr:要读取的flash地址
//       Data:要读取的数据指针
//       count:要读取的数据长度  
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::readFloat(uint32_t data_addr,float *Data,uint32_t count)
{
	read(data_addr, (uint8_t *)Data,  count*sizeof(float));
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash读取多个double类型数据.
// 参数: data_addr:要读取的flash地址
//       Data:要读取的数据指针
//       count:要读取的数据长度  
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::readDouble(uint32_t data_addr,double *Data,uint32_t count)
{
	read(data_addr, (uint8_t *)Data,  count*sizeof(double));
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash读取一个有符号8位数据.
// 参数: data_addr:地址
// 返回: none.
//========================================================================
int8_t INTERNAL_FLASH::readChar(uint32_t data_addr)
{
	int8_t value = 0;
	return readALL(data_addr,value);
}

//========================================================================
// 描述: flash读取一个无符号8位数据.
// 参数: data_addr:地址
// 返回: none.
//========================================================================
uint8_t INTERNAL_FLASH::readUChar(uint32_t data_addr)
{
	uint8_t value = 0;
	return readALL(data_addr,value);
}

//========================================================================
// 描述: flash读取一个有符号16位数据.
// 参数: data_addr:地址
// 返回: none.
//========================================================================
int16_t INTERNAL_FLASH::readShort(uint32_t data_addr)
{
	int16_t value = 0;
	return readALL(data_addr,value);
}

//========================================================================
// 描述: flash读取一个无符号16位数据.
// 参数: data_addr:地址
// 返回: none.
//========================================================================
uint16_t INTERNAL_FLASH::readUShort(uint32_t data_addr)
{
    uint16_t value=0;
    return readALL(data_addr,value);
}

//========================================================================
// 描述: flash读取一个有符号32位数据.
// 参数: data_addr:地址
// 返回: none.
//========================================================================
int32_t INTERNAL_FLASH::readInt(uint32_t data_addr)
{
    int32_t value=0;
    return readALL(data_addr,value);
}

//========================================================================
// 描述: flash读取一个无符号32位数据.
// 参数: data_addr:地址
// 返回: none.
//========================================================================
uint32_t INTERNAL_FLASH::readUInt(uint32_t data_addr)
{
    uint32_t value=0;
    return readALL(data_addr,value);
}

//========================================================================
// 描述: flash读取一个有符号64位数据.
// 参数: data_addr:地址
// 返回: none.
//========================================================================
int64_t INTERNAL_FLASH::readLong64(uint32_t data_addr)
{
    int64_t value=0;
    return readALL(data_addr,value);
}

//========================================================================
// 描述: flash读取一个无符号64位数据.
// 参数: data_addr:地址
// 返回: none.
//========================================================================
uint64_t INTERNAL_FLASH::readULong64(uint32_t data_addr)
{
    uint64_t value=0;
    return readALL(data_addr,value);
}

//========================================================================
// 描述: flash读取一个float类型小数.
// 参数: data_addr:地址
// 返回: none.
//========================================================================
float INTERNAL_FLASH::readFloat(uint32_t data_addr)
{
    float value=0;
    return readALL(data_addr,value);
}

//========================================================================
// 描述: flash读取一个double类型小数.
// 参数: data_addr:地址
// 返回: none.
//========================================================================
double INTERNAL_FLASH::readDouble(uint32_t data_addr)
{
    double value = 0;
    return readALL(data_addr,value);
}

//========================================================================
// 描述: flash读取数据模板.
// 参数: data_addr:地址
// 返回: none.
//========================================================================
template <class T> T INTERNAL_FLASH::readALL(uint32_t data_addr,T &value)
{
    if( (data_addr-FLASH_START_ADDR + sizeof(T)) > FLASH_SIZE )
        return value;
    memcpy((uint8_t*) &value,(uint8_t*)(data_addr),sizeof(T));
    return value;
}

//========================================================================
// 描述: flash写入一个半字数据.
// 参数: data_addr:地址
//       dat:半字数据
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::write_half_word(uint32_t data_addr, uint16_t buf)
{
    FLASH_Unlock();
    FLASHStatus = FLASH_ProgramHalfWord(data_addr, buf);
    FLASH_Lock();
    return FLASHStatus;
}

//========================================================================
// 描述: flash快速写入64字节数据.
// 参数: data_addr:地址
//       buf:数组指针（64字节）
// 返回: none.
//========================================================================
void INTERNAL_FLASH::write_64bytes(uint32_t data_addr, uint8_t *buf)
{
	uint32_t *page_buf = (uint32_t *)buf;
    FLASH_Unlock_Fast();
    FLASH_BufReset();
    for(int i=0;i<16;i++){
        FLASH_BufLoad(data_addr+4*i, page_buf[i]);
    }
    FLASH_ProgramPage_Fast(data_addr);
    FLASH_Lock_Fast();
}

//========================================================================
// 描述: flash在指定地址写入指定长度的数据.
// 参数: data_addr:地址
//       buf:数据缓冲区
//		 num_bytes：要写入的字节数
// 返回: none.
//========================================================================
void INTERNAL_FLASH::write(uint32_t data_addr, uint8_t *buf, uint16_t num_bytes)
{
	uint32_t secpos;	//页地址
	uint16_t secoff;  //在页地址的偏移
	uint16_t secremain;	   
 	uint16_t i;
	uint8_t flash_buf[FLASH_PAGE_SIZE];   
	data_addr =  data_addr - FLASH_START_ADDR;
	secpos = data_addr/FLASH_PAGE_SIZE;  //页地址
	secoff = data_addr%FLASH_PAGE_SIZE;  //在页地址内的偏移
	secremain = FLASH_PAGE_SIZE-secoff;//该页的剩余空间大小   
	if(num_bytes <= secremain)	
	{
		secremain = num_bytes;
	}
	while(1)
	{
		read(FLASH_START_ADDR+secpos*FLASH_PAGE_SIZE, flash_buf, FLASH_PAGE_SIZE);	//读出整个扇区的内容
		for(i=0;i<secremain;i++)
		{
			if(flash_buf[secoff+i]!=0XFF){
				erase_page(secpos);	//擦除整页
				break;
			}
		}
		for(i=0;i<secremain;i++)	    //复制
		{
			flash_buf[secoff+i] = buf[i];	  
		}
		write_64bytes(FLASH_START_ADDR+secpos*FLASH_PAGE_SIZE, flash_buf);
		if(num_bytes == secremain)break;	//全部写入完成
		else{
			secpos++;	//页地址增1
			secoff = 0;	//偏移清零
		   	buf+=secremain;  //要写入的数据地址偏移
			data_addr+=secremain;//flash地址偏移	  
			num_bytes-=secremain;	//要写入的字节数递减
			if(num_bytes>FLASH_PAGE_SIZE)secremain = FLASH_PAGE_SIZE;	//下一页还是写不完
			else secremain = num_bytes;	  //下一个扇区可以写完了
		}				
	}
}

//========================================================================
// 描述: flash写入多个有符号8位数据.
// 参数: data_addr:要写入的flash地址
//       Data:要写入的数据指针
//       count:要写入的数据长度  
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::writeChar(uint32_t data_addr,char *Data,uint32_t count)
{
    if (data_addr + count*sizeof(uint8_t) > FLASH_END_ADDR) 	//超出flash范围
	{	
		return FLASH_ERROR_PG;
    }
	write(data_addr, (uint8_t *)Data,  count);
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash写入多个无符号8位数据.
// 参数: data_addr:要写入的flash地址
//       Data:要写入的数据指针
//       count:要写入的数据长度  
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::writeUChar(uint32_t data_addr,uint8_t *Data,uint32_t count)
{
    if (data_addr + count*sizeof(uint8_t) > FLASH_END_ADDR) 	//超出flash范围
	{	
		return FLASH_ERROR_PG;
    }
	write(data_addr, (uint8_t *)Data,  count);
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash写入多个有符号16位数据.
// 参数: data_addr:要写入的flash地址
//       Data:要写入的数据指针
//       count:要写入的数据长度  
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::writeShort(uint32_t data_addr,int16_t *Data,uint32_t count)
{
    if (data_addr + count*sizeof(int16_t) > FLASH_END_ADDR) 	//超出flash范围
	{	
		return FLASH_ERROR_PG;
    }
	write(data_addr, (uint8_t *)Data,  count*sizeof(int16_t));
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash写入多个无符号16位数据.
// 参数: data_addr:要写入的flash地址
//       Data:要写入的数据指针
//       count:要写入的数据长度  
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::writeUShort(uint32_t data_addr,uint16_t *Data,uint32_t count)
{
    if (data_addr + count*sizeof(uint16_t) > FLASH_END_ADDR) 	//超出flash范围
	{	
		return FLASH_ERROR_PG;
    }
	write(data_addr, (uint8_t *)Data,  count*sizeof(uint16_t));
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash写入多个有符号32位数据.
// 参数: data_addr:要写入的flash地址
//       Data:要写入的数据指针
//       count:要写入的数据长度  
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::writeInt(uint32_t data_addr,int32_t *Data,uint32_t count)
{
    if (data_addr + count*sizeof(int32_t) > FLASH_END_ADDR) 	//超出flash范围
	{	
		return FLASH_ERROR_PG;
    }
	write(data_addr, (uint8_t *)Data,  count*sizeof(int32_t));
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash写入多个无符号32位数据.
// 参数: data_addr:要写入的flash地址
//       Data:要写入的数据指针
//       count:要写入的数据长度  
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::writeUInt(uint32_t data_addr,uint32_t *Data,uint32_t count)
{
    if (data_addr + count*sizeof(uint32_t) > FLASH_END_ADDR) 	//超出flash范围
	{	
		return FLASH_ERROR_PG;
    }
	write(data_addr, (uint8_t *)Data,  count*sizeof(uint32_t));
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash写入多个有符号64位数据.
// 参数: data_addr:要写入的flash地址
//       Data:要写入的数据指针
//       count:要写入的数据长度  
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::writeLong64(uint32_t data_addr,int64_t *Data,uint32_t count)
{
    if (data_addr + count*sizeof(int64_t) > FLASH_END_ADDR) 	//超出flash范围
	{	
		return FLASH_ERROR_PG;
    }
	write(data_addr, (uint8_t *)Data,  count*sizeof(int64_t));
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash写入多个无符号64位数据.
// 参数: data_addr:要写入的flash地址
//       Data:要写入的数据指针
//       count:要写入的数据长度  
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::writeULong64(uint32_t data_addr,uint64_t *Data,uint32_t count)
{
    if (data_addr + count*sizeof(uint64_t) > FLASH_END_ADDR) 	//超出flash范围
	{	
		return FLASH_ERROR_PG;
    }
	write(data_addr, (uint8_t *)Data,  count*sizeof(uint64_t));
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash写入多个float类型数据.
// 参数: data_addr:要写入的flash地址
//       Data:要写入的数据指针
//       count:要写入的数据长度  
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::writeFloat(uint32_t data_addr,float *Data,uint32_t count)
{
    if (data_addr + count*sizeof(float) > FLASH_END_ADDR) 	//超出flash范围
	{	
		return FLASH_ERROR_PG;
    }
	write(data_addr, (uint8_t *)Data,  count*sizeof(float));
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash写入多个double类型数据.
// 参数: data_addr:要写入的flash地址
//       Data:要写入的数据指针
//       count:要写入的数据长度  
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::writeDouble(uint32_t data_addr,double *Data,uint32_t count)
{
    if (data_addr + count*sizeof(double) > FLASH_END_ADDR) 	//超出flash范围
	{	
		return FLASH_ERROR_PG;
    }
	write(data_addr, (uint8_t *)Data,  count*sizeof(double));
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash写入一个有符号8位数据.
// 参数: data_addr:要写入的flash地址
//       Data:要写入的数据
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::writeChar(uint32_t data_addr,char Data)
{
	char temp[1];
	temp[0]=Data;
	write(data_addr, (uint8_t *)temp,sizeof(char));
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash写入无符号8位数据.
// 参数: data_addr:要写入的flash地址
//       Data:要写入的数据  
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::writeUChar(uint32_t data_addr,uint8_t Data)
{
	uint8_t temp[1];
	temp[0]=Data;
	write(data_addr, (uint8_t *)temp,sizeof(uint8_t));
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash写入有符号16位数据.
// 参数: data_addr:要写入的flash地址
//       Data:要写入的数据
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::writeShort(uint32_t data_addr,int16_t Data)
{
	int16_t temp[1];
	temp[0]=Data;
	write(data_addr, (uint8_t *)temp,sizeof(int16_t));
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash写入无符号16位数据.
// 参数: data_addr:要写入的flash地址
//       Data:要写入的数据 
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::writeUShort(uint32_t data_addr,uint16_t Data)
{
	uint16_t temp[1];
	temp[0]=Data;
	write(data_addr, (uint8_t *)temp,sizeof(uint16_t));
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash写入有符号32位数据.
// 参数: data_addr:要写入的flash地址
//       Data:要写入的数据
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::writeInt(uint32_t data_addr,int32_t Data)
{
	int32_t temp[1];
	temp[0]=Data;
	write(data_addr, (uint8_t *)temp,sizeof(int32_t));
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash写入无符号32位数据.
// 参数: data_addr:要写入的flash地址
//       Data:要写入的数据 
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::writeUInt(uint32_t data_addr,uint32_t Data)
{
	uint32_t temp[1];
	temp[0]=Data;
	write(data_addr, (uint8_t *)temp,sizeof(uint32_t));
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash写入有符号64位数据.
// 参数: data_addr:要写入的flash地址
//       Data:要写入的数据 
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::writeLong64(uint32_t data_addr,int64_t Data)
{
	int64_t temp[1];
	temp[0]=Data;
	write(data_addr, (uint8_t *)temp,sizeof(int64_t));
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash写入无符号64位数据.
// 参数: data_addr:要写入的flash地址
//       Data:要写入的数据 
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::writeULong64(uint32_t data_addr,uint64_t Data)
{
	uint64_t temp[1];
	temp[0]=Data;
	write(data_addr, (uint8_t *)temp,sizeof(uint64_t));
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash写入float类型数据.
// 参数: data_addr:要写入的flash地址
//       Data:要写入的数据 
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::writeFloat(uint32_t data_addr,float Data)
{
	float temp[1];
	temp[0]=Data;
	write(data_addr, (uint8_t *)temp,sizeof(float));
	return FLASH_COMPLETE;
}

//========================================================================
// 描述: flash写入double类型数据.
// 参数: data_addr:要写入的flash地址
//       Data:要写入的数据
// 返回: none.
//========================================================================
FLASH_Status INTERNAL_FLASH::writeDouble(uint32_t data_addr,double Data)
{
	double temp[1];
	temp[0]=Data;
	write(data_addr, (uint8_t *)temp,sizeof(double));
	return FLASH_COMPLETE;
}


#endif









