#include <CH32V003.h>
#include "CH32V_FLASH.h"
#include "HardwareSerial.h"

int16_t data = 0;
char write_buf[5]={'h','e','l','l','o'};
char read_buf[5];

INTERNAL_FLASH flash;

int main(void)
{
  CH32_Init();
  Serial1.begin(9600);
  //ch32v003的闪存地址从0x08000000到0x08003FFF，分为256页(页0 - 页255),每页64字节大小。
  //每页的范围请查看芯片手册，FLASH擦除为按页擦除。
  //范例取0x08003FC0-0x08003FFF 最后一页的64字节来演示。
  flash.erase_page(0);
  flash.writeChar(0x08003FC0,write_buf,5);
  flash.writeShort(0,32);
  while(1){
    flash.readChar(0x08003FC0,read_buf,5);
    for (int i = 0; i < 5; i = i + 1) {
      Serial1.print(read_buf[(int)(i)]);
    }
    Serial1.println(";");
    delay(1000);
    Serial1.println((flash.readShort(0)));
  }
  return 1;
}
