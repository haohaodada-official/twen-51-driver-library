/**
 * @file wiring.c
 * @brief CH32V003基本硬件驱动函数定义
 * @version 0.1
 * @date 2022-12-05
 * 
 * @copyright Copyright (c) 2022 杭州天问五幺科技有限公司
 * 
 */
#include "CH32V003.h"
#include "wiring.h"

/**
 * @brief 获取系统运行时间（毫秒级）
 * @note 基于内核的32位加计数器（SysTick）
 * @return 运行时间（毫秒）
 */
uint32_t millis()
{
    return (SysTick->CNT / clockCyclesPerMillisecond());
}

/**
 * @brief 获取系统运行时间（微秒级）
 * @note 基于内核的32位加计数器（SysTick）
 * @return 运行时间（微秒）
 */
uint32_t micros(void)
{
    return (SysTick->CNT / clockCyclesPerMicrosecond());
}

/**
 * @brief 毫秒级延迟
 * @note 注意延时等待时，系统处于停滞状态，除了中断
 * @param ms 延时时间（毫秒）
 */
void delay(uint32_t ms)
{
    uint32_t start = millis();

    while (millis() - start < ms);
}

/**
 * @brief 微秒级延迟
 * @note 注意延时等待时，系统处于停滞状态，除了中断
 * @param ms 延时时间（微秒）
 */
void delayMicroseconds(uint32_t us)
{
    uint32_t start = micros();

    while (micros() - start < us);
}

/**
 * @brief 串口1初始化
 * @note 默认使用PD5/PD6，如需要使用其它引脚，请自己修改
 * @param baudrate 波特率
 *                  1200
 *                  2400
 *                  4800
 *                  9600
 *                  19200
 *                  38400
 *                  57600
 *                  115200
 */
void CH32V_UART1_Init(uint32_t baudrate)
{
    GPIO_InitTypeDef  GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD | RCC_APB2Periph_USART1, ENABLE);

    /* USART1 RX-->PD6  TX-->PD5 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOD, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOD, &GPIO_InitStructure);

    USART_InitStructure.USART_BaudRate = baudrate;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;

    USART_Init(USART1, &USART_InitStructure);
    USART_Cmd(USART1, ENABLE);

    USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

/**
 * @brief 串口1发送字符
 * @param c 字符
 */
void CH32V_UART1_SendCh(unsigned char c)
{
    while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
    USART1->DATAR = (c & (uint16_t)0x01FF);
}

/**
 * @brief GPIO引脚初始化
 * @param GPIOx GPIO端口号
 *              GPIOA
 *              GPIOC
 *              GPIOD
 * @param GPIO_Pin GPIO引脚号(0~8)
 * @param mode 引脚模式
 *              GPIO_Mode_AIN         模拟输入
 *              GPIO_Mode_Out_PP      通用推免输出
 *              GPIO_Mode_IPU         上拉输入
 *              GPIO_Mode_IPD         下拉输入
 *              GPIO_Mode_IN_FLOATING 浮空输入
 *              GPIO_Mode_Out_OD      通用开漏输出
 *              GPIO_Mode_AF_PP       复用推免输出
 *              GPIO_Mode_AF_OD       复用开漏输出
 * @param speed IO速度
 *              GPIO_Speed_10MHz
 *              GPIO_Speed_2MHz
 *              GPIO_Speed_50MHz
 */
void CH32V_GPIO_Init(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin, GPIOMode_TypeDef mode, GPIOSpeed_TypeDef speed)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Pin = (1 << GPIO_Pin);
    GPIO_InitStructure.GPIO_Mode = mode;
    GPIO_InitStructure.GPIO_Speed = speed;
    GPIO_Init(GPIOx, &GPIO_InitStructure);
}

/**
 * @brief GPIO端口初始化
 * @param GPIOx GPIO端口号
 *              GPIOA
 *              GPIOC
 *              GPIOD
 * @param mode 引脚模式
 *              GPIO_Mode_AIN         模拟输入
 *              GPIO_Mode_Out_PP      通用推免输出
 *              GPIO_Mode_IPU         上拉输入
 *              GPIO_Mode_IPD         下拉输入
 *              GPIO_Mode_IN_FLOATING 浮空输入
 *              GPIO_Mode_Out_OD      通用开漏输出
 *              GPIO_Mode_AF_PP       复用推免输出
 *              GPIO_Mode_AF_OD       复用开漏输出
 * @param speed IO速度
 *              GPIO_Speed_10MHz
 *              GPIO_Speed_2MHz
 *              GPIO_Speed_50MHz
 */
void CH32V_GPIOX_Init(GPIO_TypeDef *GPIOx,  GPIOMode_TypeDef mode, GPIOSpeed_TypeDef speed)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All;
    GPIO_InitStructure.GPIO_Mode = mode;
    GPIO_InitStructure.GPIO_Speed = speed;
    GPIO_Init(GPIOx, &GPIO_InitStructure);
}

/**
 * @brief GPIO引脚初始化
 * @note Arduino引脚初始化接口函数
 * @param pin 引脚名
 *              PA0
 *              PA1
 *              PA2
 *              PC0
 *              PC1
 *              PC2
 *              PC3
 *              PC4
 *              PC5
 *              PC6
 *              PC7
 *              PD0
 *              PD1
 *              PD2
 *              PD3
 *              PD4
 *              PD5
 *              PD6
 *              PD7
 * @param mode 引脚模式
 *              GPIO_Mode_AIN         模拟输入
 *              GPIO_Mode_Out_PP      通用推免输出
 *              GPIO_Mode_IPU         上拉输入
 *              GPIO_Mode_IPD         下拉输入
 *              GPIO_Mode_IN_FLOATING 浮空输入
 *              GPIO_Mode_Out_OD      通用开漏输出
 *              GPIO_Mode_AF_PP       复用推免输出
 *              GPIO_Mode_AF_OD       复用开漏输出
 */
void pinMode(PIN_Name pin, GPIOMode_TypeDef mode)
{
    CH32V_GPIO_Init((GPIO_TypeDef *)DIGITAL_PIN_PORT[pin], DIGITAL_PIN_NUM[pin], mode, GPIO_Speed_50MHz);
}
   
/**
 * @brief 设置GPIO引脚电平状态
 * @note Arduino引脚设置接口函数
 * @param pin 引脚名
 *              PA0
 *              PA1
 *              PA2
 *              PC0
 *              PC1
 *              PC2
 *              PC3
 *              PC4
 *              PC5
 *              PC6
 *              PC7
 *              PD0
 *              PD1
 *              PD2
 *              PD3
 *              PD4
 *              PD5
 *              PD6
 *              PD7
 * @return 电平状态
 *          HIGH:高
 *          LOW：低
 */
void digitalWrite(PIN_Name pin, uint8_t state)
{
    if (state == HIGH)
    {
        GPIO_SetBits((GPIO_TypeDef *)DIGITAL_PIN_PORT[pin], 1 << DIGITAL_PIN_NUM[pin]);
    }
    else
    {
        GPIO_ResetBits((GPIO_TypeDef *)DIGITAL_PIN_PORT[pin], 1 << DIGITAL_PIN_NUM[pin]);
    }
}

/**
 * @brief 获取GPIO引脚电平状态
 * @note Arduino引脚状态读取接口函数
 * @param pin 引脚名
 *              PA0
 *              PA1
 *              PA2
 *              PC0
 *              PC1
 *              PC2
 *              PC3
 *              PC4
 *              PC5
 *              PC6
 *              PC7
 *              PD0
 *              PD1
 *              PD2
 *              PD3
 *              PD4
 *              PD5
 *              PD6
 *              PD7
 * @return 电平状态
 *          HIGH:高
 *          LOW：低
 */
uint8_t digitalRead(PIN_Name pin)
{
    return GPIO_ReadInputDataBit((GPIO_TypeDef *)DIGITAL_PIN_PORT[pin], 1 << DIGITAL_PIN_NUM[pin]);
}

/**
 * @brief ADC初始化
 * @note 采用单次转换模式、单通道模式
 * @param adcn adc通道选择
 *              ADC_PA1
 *              ADC_PA2
 *              ADC_PC4
 *              ADC_PD2
 *              ADC_PD3
 *              ADC_PD4
 *              ADC_PD5
 *              ADC_PD6
 * @param sample 采样周期 
 *              ADC_SAMPLE_3
 *              ADC_SAMPLE_9
 *              ADC_SAMPLE_15
 *              ADC_SAMPLE_30
 *              ADC_SAMPLE_43
 *              ADC_SAMPLE_57
 *              ADC_SAMPLE_73
 *              ADC_SAMPLE_241
 */

void CH32V_ADC_Init(ADC_Name adcn, uint32_t sample)
{
    ADC_InitTypeDef ADC_InitStructure;          //定义ADC结构体
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
    RCC_ADCCLKConfig(RCC_PCLK2_Div8);

    pinMode(ANALOG_PIN_NUM[adcn], GPIO_Mode_AIN);
    ADC1->SAMPTR2 = sample << (3 * adcn);

    ADC_DeInit(ADC1);                            //复位ADC1，重设为默认缺省值
    
    ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;                  //ADC工作模式:ADC1和ADC2工作在独立模式
    ADC_InitStructure.ADC_ScanConvMode = DISABLE;                       //模数转换工作在单通道模式
    ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;                 //模数转换工作在单次转换模式
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None; //转换由软件而不是外部触发启动
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;              //ADC数据右对齐
    ADC_InitStructure.ADC_NbrOfChannel = 1;                             //顺序进行规则转换的ADC通道的数目
    ADC_Init(ADC1,&ADC_InitStructure);                                  //初始化ADC1
    
    ADC_Cmd(ADC1,ENABLE);                        //使能ADC1
    
    ADC_ResetCalibration(ADC1);                  //使能复位校准
    while(ADC_GetResetCalibrationStatus(ADC1));  //等待复位校准结束
    ADC_StartCalibration(ADC1);                  //开启AD校准
    while(ADC_GetCalibrationStatus(ADC1));       //等待校准结束
}

/**
 * @brief 读ADC值
 * @param adcn adc通道选择
 *              ADC_PA1
 *              ADC_PA2
 *              ADC_PC4
 *              ADC_PD2
 *              ADC_PD3
 *              ADC_PD4
 *              ADC_PD5
 *              ADC_PD6
 * @return 10位ADC值
 */
uint16_t ADC_Read(ADC_Name adcn)
{
    //配置ADCx，ADC通道，规则采样顺序，采样时间
    ADC_RegularChannelConfig(ADC1,adcn,1,ADC_SampleTime_241Cycles);
    ADC_SoftwareStartConvCmd(ADC1,ENABLE);          //使能ADC1的软件转换启动功能
    while(!ADC_GetFlagStatus(ADC1,ADC_FLAG_EOC));   //等待转换结束
    return ADC_GetConversionValue(ADC1);            //返回最近一次ADC1规则组的转换结果
}

/**
 * @brief 系统初始化
 */
void CH32_Init(void)
{
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA |RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD, ENABLE);//默认打开全部IO外设，如需低功耗，请自己优化，关闭不必要的外设时钟
    SysTick->CNT = 0;//计数器清零
    SysTick->CTLR = 1;//启动系统计数器
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//设置中断组2，2 bits for pre-emption priority，2 bits for subpriority
}

/**
 * @brief 支持C++对象构造函数
 */
void _init(void)
{
}

/**
 * @brief 支持C++对象析构函数
 */
void _fini(void)
{
}
