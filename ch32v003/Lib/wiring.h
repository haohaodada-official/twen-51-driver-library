/*

*/

#ifndef Wiring_h
#define Wiring_h

#ifdef __cplusplus
extern "C"{
#endif

#include "ch32v00x_conf.h"
#include "pins_arduino.h"

#define  ADC_SAMPLE_3      ADC_SampleTime_3Cycles 		//3采样周期 
#define  ADC_SAMPLE_9      ADC_SampleTime_9Cycles 		//9采样周期
#define  ADC_SAMPLE_15     ADC_SampleTime_15Cycles 		//15采样周期
#define  ADC_SAMPLE_30     ADC_SampleTime_30Cycles 	 	//30采样周期
#define  ADC_SAMPLE_43     ADC_SampleTime_43Cycles 		//43采样周期
#define  ADC_SAMPLE_57     ADC_SampleTime_57Cycles 		//57采样周期
#define  ADC_SAMPLE_73     ADC_SampleTime_73Cycles 		//73采样周期
#define  ADC_SAMPLE_241    ADC_SampleTime_241Cycles 	//241采样周期

typedef enum
{
	ADC_PA2 = 0,
	ADC_PA1,
	ADC_PC4, 
	ADC_PD2,
	ADC_PD3,
	ADC_PD5,
	ADC_PD6,
	ADC_PD4
} ADC_Name;

void CH32V_ADC_Init(ADC_Name adcn, uint32_t sample);
uint16_t ADC_Read(ADC_Name adcn);

uint32_t millis();
uint32_t micros(void);
void delay(uint32_t ms);
void delayMicroseconds(uint32_t us);
void CH32V_UART1_Init(uint32_t baudrate);
void CH32V_UART1_SendCh(unsigned char c);
void CH32V_GPIO_Init(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin, GPIOMode_TypeDef mode, GPIOSpeed_TypeDef speed);
void CH32V_GPIOX_Init(GPIO_TypeDef *GPIOx,  GPIOMode_TypeDef mode, GPIOSpeed_TypeDef speed);
void pinMode(PIN_Name pin, GPIOMode_TypeDef mode);
void digitalWrite(PIN_Name pin, uint8_t state);
uint8_t digitalRead(PIN_Name pin);
void CH32V_ADC_Init(ADC_Name adcn, uint32_t sample);
uint16_t ADC_Read(ADC_Name adcn);
void CH32_Init(void);
void _init(void);
void _fini(void);

#ifdef __cplusplus
} // extern "C"
#endif


#endif
