#ifndef Pins_Arduino_h
#define Pins_Arduino_h

#include "ch32v00x.h"

#define NUM_DIGITAL_PINS            18
#define NUM_PWM_PINS                4
#define NUM_ANALOG_INPUTS           8
// #define analogInputToDigitalPin(p)  ((p < 6) ? (p) + 18 : (p) - 6)

// #define digitalPinHasPWM(p)         ((p) == PA1 || (p) == PC0 || (p) == PC3 || (p) == PC4 || (p) == PC5 || (p) == PC7 || (p) == PD2 || (p) == PD3)

// #define LED_BUILTIN 17

typedef enum
{
    PA0 = 0,
    PA1,
    PA2,
    PA3,
    PA4,
    PA5,
    PA6,
    PA7,
    PC0 = 16,
    PC1,
    PC2,
    PC3,
    PC4,
    PC5,
    PC6,
    PC7,
    PD0,
    PD1,
    PD2,
    PD3,
    PD4,
    PD5,
    PD6,
    PD7
} PIN_Name;

static const uint32_t DIGITAL_PIN_PORT[32]={
  GPIOA_BASE,  //PA0
  GPIOA_BASE,  //PA1
  GPIOA_BASE,  //PA2
  GPIOA_BASE,  //PA3
  GPIOA_BASE,  //PA4
  GPIOA_BASE,  //PA5
  GPIOA_BASE,  //PA6
  GPIOA_BASE,  //PA7
  0,           //PB占位
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  GPIOC_BASE,  //PC0
  GPIOC_BASE,  //PC1
  GPIOC_BASE,  //PC2
  GPIOC_BASE,  //PC3
  GPIOC_BASE,  //PC4
  GPIOC_BASE,  //PC5
  GPIOC_BASE,  //PC6
  GPIOC_BASE,  //PC7
  GPIOD_BASE,  //PD0
  GPIOD_BASE,  //PD1
  GPIOD_BASE,  //PD1
  GPIOD_BASE,  //PD0
  GPIOD_BASE,  //PD1
  GPIOD_BASE,
  GPIOD_BASE,  //PD0
  GPIOD_BASE
};

const uint32_t DIGITAL_PIN_NUM[32]={
  0, //PA0
  1, //PA1
  2, //PA2
  3, //PA3
  4, //PA4
  5, //PA5
  6, //PA6
  7, //PA7
  0, //PB0占位
  1, //PB1
  2, //PB2
  3, //PB3
  4, //PB4
  5, //PB5
  6, //PB6
  7, //PB7   
  0, //PC0
  1, //PC1
  2, //PC2
  3, //PC3
  4, //PC4
  5, //PC5
  6, //PC6
  7, //PC7
  0, //PD0
  1, //PD1
  2, //PD2
  3, //PD3
  4, //PD4
  5, //PD5
  6, //PD6
  7, //PD7
};

const uint32_t ANALOG_PIN_NUM[NUM_ANALOG_INPUTS]={
  PA2,    //A0
  PA1,    //A1
  PC4,    //A2
  PD2,    //A3
  PD3,    //A4
  PD5,    //A5
  PD6,    //A6
  PD4     //A7
};	

// #define PIN_SPI_SS    (PC1)
// #define PIN_SPI_MOSI  (PC6)
// #define PIN_SPI_MISO  (PC7)
// #define PIN_SPI_SCK   (PC5)

// static const uint8_t SS   = PIN_SPI_SS;
// static const uint8_t MOSI = PIN_SPI_MOSI;
// static const uint8_t MISO = PIN_SPI_MISO;
// static const uint8_t SCK  = PIN_SPI_SCK;

// #define PIN_WIRE_SDA        (PC1)
// #define PIN_WIRE_SCL        (PC2)

// static const uint8_t SDA = PIN_WIRE_SDA;
// static const uint8_t SCL = PIN_WIRE_SCL;


#endif