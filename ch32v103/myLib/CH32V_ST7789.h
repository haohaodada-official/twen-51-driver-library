/**
 * @file ST7789.h
 * @brief ST7789彩屏驱动头文件
 * @details 8位并口模式,16位并口模式没有经过测试
 * @version 0.1
 * @date 2022-08-16
 * @author hhdd
 * @copyright Copyright (c) 2021 TWen51 Technology Co., Ltd.
 */
#ifndef _ST7789_H
#define _ST7789_H

#include "twen_gfx.h"

/************************************* 用户配置区 *****************************************/ 
#ifndef ST7789_RGB_ORDER			
#define	ST7789_RGB_ORDER			0	//RGB和BGR切换位
#endif
#define ST7789_DATA_PORT            GPIOB  //数据端口寄存器
#define ST7789_DATA_PORT_TYPE       1   //数据端口地址偏移


//LCD重要参数集
typedef struct
{
	uint16_t width;		//LCD 宽度
	uint16_t height;	//LCD 高度
	uint16_t id;			//LCD ID
	uint8_t  dir;			//彩屏显示方向。	
	/* 保存真正显示区域的地址信息 */
	uint16_t x_start_width; 	//x方向起始地址到x起始边界的距离
	uint16_t x_end_width; 	//x方向结束地址到x起始边界的距离
	uint16_t y_start_width;	//y方向起始地址到y起始边界的距离
	uint16_t y_end_width; 	//y方向结束地址到y起始边界的距离
}_lcd_dev_s;

class ST7789 : public TWEN_GFX
{
public:
	//8位并口模式
	ST7789(uint16_t w, uint16_t h, int8_t cs_pin, int8_t wr_pin, int8_t rs_pin, int8_t rd_pin,  \
            int8_t res_pin, int8_t blk_pin=-1);

	void init();
	void start_write();	//开始写入
	void end_write();	//结束写入
	void backlight(uint8_t bright);	//控制背光
	void set_direction(uint8_t direction);	//设置显示方向
	void set_windows(uint16_t , uint16_t , uint16_t , uint16_t ); //设置显示窗口
	void clear(uint16_t color);	//清屏
	void draw_point(int16_t x, int16_t y, uint16_t color); //画点
	void show_picture(uint16_t x,uint16_t y,uint16_t length,uint16_t width,const uint8_t pic[]);	//显示图片
	void fill_rectangle(int16_t x0, int16_t y0, int16_t x1, int16_t y1, int16_t color);//填充矩形
	void write_8bit(uint16_t dat);	//写入8位数据
	void write_16bit(uint16_t dat);  //写入16位数据

    uint16_t read_data();
    uint16_t read_reg(uint16_t reg);
    uint16_t read_point(uint16_t x, uint16_t y);

    void clear_fast(uint16_t color);

	_lcd_dev_s lcddev;	//管理LCD重要参数
private:
	uint8_t _cs_pin;	//引脚
	uint8_t _rs_pin;
	uint8_t _wr_pin;
	uint8_t _rd_pin;	
	uint8_t _res_pin;
	uint8_t _blk_pin;      //
	void write_cmd(uint16_t com);	//写命令

    inline void st7789_w_data(uint16_t dat)			
    {
    #if ST7789_DATA_PORT_TYPE==0     //低8位
        ST7789_DATA_PORT->OUTDR = dat | (ST7789_DATA_PORT->OUTDR &~((uint32_t)(0xff)));
    #elif  ST7789_DATA_PORT_TYPE==1      //高8位
        ST7789_DATA_PORT->OUTDR = (dat<<8) | (ST7789_DATA_PORT->OUTDR &~((uint32_t)(0xff<<8)));
    #else   //16位并口
        ST7789_DATA_PORT->OUTDR = ((uint16_t)dat);
    #endif
    }
};

//========================================================================
// 描述: 彩屏设置引脚.
// 参数: w,h:宽度和高度。
//       cs_pin,re_pin,wr_pin,rd_pin,res_pin,blk_pin:控制引脚。
//       GPIOx:8位或16位并口引脚。
//       porttype:0使用GPIOx的低8位，1使用GPIOx的高8位，2使用GPIOx的16位。
// 返回: none.
//========================================================================
ST7789::ST7789(uint16_t w, uint16_t h, int8_t cs_pin, int8_t wr_pin, int8_t rs_pin, int8_t rd_pin,  \
                    int8_t res_pin, int8_t blk_pin):TWEN_GFX(w, h)
{

	lcddev.width = w;
	lcddev.height = h;
	_cs_pin = cs_pin;
	_rs_pin = rs_pin;
	_wr_pin = wr_pin;
	_rd_pin = rd_pin;	
	_res_pin = res_pin;
	if(blk_pin != -1)
	{
		_blk_pin = blk_pin;
	}
}

//========================================================================
// 描述: LCD开始写入数据.
// 参数: none.
// 返回: none.
//========================================================================
void ST7789::start_write()
{
	((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BCR = 1<<(_cs_pin%16);   //CS=0;
}

//========================================================================
// 描述: LCD结束写入数据.
// 参数: none.
// 返回: none.
//========================================================================
void ST7789::end_write()
{
	((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BSHR = 1<<(_cs_pin%16);	//CS=1;
}

//========================================================================
// 描述: LCD写入8位数据.
// 参数: VAL:写入的字节数据.
// 返回: none.
//========================================================================
void ST7789::write_8bit(uint16_t dat)			
{
	((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rs_pin/16)))->BSHR = 1<<(_rs_pin%16);  //RS=1;
	((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BSHR = 1<<(_rd_pin%16);  //RD=1;
    st7789_w_data(dat);
	((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BCR = 1<<(_cs_pin%16);  //CS=0;
	((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_wr_pin/16)))->BCR = 1<<(_wr_pin%16);  //WR=0;
	((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_wr_pin/16)))->BSHR = 1<<(_wr_pin%16); //WR=1;
	((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BSHR = 1<<(_cs_pin%16); //CS=1;
}


//========================================================================
// 描述: LCD写入命令.
// 参数: Reg:写入的命令.
// 返回: none.
//========================================================================
void ST7789::write_cmd(uint16_t com)			
{
	((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rs_pin/16)))->BCR = 1<<(_rs_pin%16);  //RS=0;
	((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BSHR = 1<<(_rd_pin%16);  //RD=1;
    st7789_w_data(com);
	((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BCR = 1<<(_cs_pin%16);  //CS=0;
	((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_wr_pin/16)))->BCR = 1<<(_wr_pin%16);  //WR=0;
	((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_wr_pin/16)))->BSHR = 1<<(_wr_pin%16); //WR=1;
	((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BSHR = 1<<(_cs_pin%16); //CS=1;
}

//========================================================================
// 描述: LCD写入16位数据.
// 参数: Data:写入的数据.
// 返回: none.
//========================================================================
void ST7789::write_16bit(uint16_t dat)		
{
	((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rs_pin/16)))->BSHR = 1<<(_rs_pin%16);  //RS=1;
	((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BSHR = 1<<(_rd_pin%16);  //RD=1;
#if (ST7789_DATA_PORT_TYPE==0)||(ST7789_DATA_PORT_TYPE==1)     //8位
	st7789_w_data(dat>>8);
	((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BCR = 1<<(_cs_pin%16);  //CS=0;
	((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_wr_pin/16)))->BCR = 1<<(_wr_pin%16);  //WR=0;
	((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_wr_pin/16)))->BSHR = 1<<(_wr_pin%16); //WR=1;
	st7789_w_data(dat);
#else   //16位并口
    st7789_w_data(dat);
#endif
	((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BCR = 1<<(_cs_pin%16);  //CS=0;
	((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_wr_pin/16)))->BCR = 1<<(_wr_pin%16);  //WR=0;
	((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_wr_pin/16)))->BSHR = 1<<(_wr_pin%16); //WR=1;
	((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BSHR = 1<<(_cs_pin%16); //CS=1;
}


//========================================================================
// 描述: LCD设置窗口.
// 参数: x0,y0:起始坐标; x1,y1:终点坐标.
// 返回: none.
//========================================================================
void ST7789::set_windows(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2)
{
	write_cmd( 0x2a );
	write_16bit( x1 );
	write_16bit( x2 );
	write_cmd( 0x2b );
	write_16bit( y1 );
	write_16bit( y2 );
	write_cmd( 0x2c );
}

//========================================================================
// 描述: LCD初始化.
// 参数: none.
// 返回: none.
//========================================================================
void ST7789::init(void)
{
	//端口初始化
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    #if ST7789_DATA_PORT_TYPE==0     //低8位
        GPIO_InitStructure.GPIO_Pin = 0x00FF;
    #elif  ST7789_DATA_PORT_TYPE==1      //高8位
        GPIO_InitStructure.GPIO_Pin = 0xFF00;
    #else   //16位并口
        GPIO_InitStructure.GPIO_Pin = 0xFFFF;
    #endif
    GPIO_Init(ST7789_DATA_PORT, &GPIO_InitStructure);
	pinMode(_cs_pin, GPIO_Mode_Out_PP);
	pinMode(_wr_pin, GPIO_Mode_Out_PP);
    pinMode(_rs_pin, GPIO_Mode_Out_PP);
    pinMode(_rd_pin, GPIO_Mode_Out_PP);
    pinMode(_res_pin, GPIO_Mode_Out_PP);

	digitalWrite(_res_pin,0);
	delay(50);
	digitalWrite(_res_pin,1);
	delay(50);

	//*************2.4inch ST7789初始化**********//	
	write_cmd(0x11);   //sleep out
	delay(120); 
	//----------------display and color format setting------------------//
    write_cmd(0x36);
    write_8bit(0x00);
    write_cmd(0x3a);
    write_8bit(0x05);
	//----------------------ST7789 Frame rate setting--------------------//
    write_cmd(0xb2);
    write_8bit(0x0c);
    write_8bit(0x0c);
    write_8bit(0x00);
    write_8bit(0x33);
    write_8bit(0x33);
    write_cmd(0xb7);
    write_8bit(0x35);
    //----------------------ST7789 Power setting--------------------------//
    write_cmd(0xbb);
    write_8bit(0x1c);
    write_cmd(0xc0);
    write_8bit(0x2c);
    write_cmd(0xc2);
    write_8bit(0x01);
    write_cmd(0xc3);
    write_8bit(0x0b);
    write_cmd(0xc4);
    write_8bit(0x20);
    write_cmd(0xc6);
    write_8bit(0x0f);
    write_cmd(0xd0);
    write_8bit(0xa4);
    write_8bit(0xa1);
    //-----------------------ST7789 gamma setting--------------------------//
    write_cmd(0xe0);
    write_8bit(0xd0);
    write_8bit(0x00);
    write_8bit(0x03);
    write_8bit(0x09);
    write_8bit(0x13);
    write_8bit(0x1c);
    write_8bit(0x3a);
    write_8bit(0x55);
    write_8bit(0x48);
    write_8bit(0x18);
    write_8bit(0x12);
    write_8bit(0x0e);
    write_8bit(0x19);
    write_8bit(0x1e);
    write_cmd(0xe1);
    write_8bit(0xd0);
    write_8bit(0x00);
    write_8bit(0x03);
    write_8bit(0x09);
    write_8bit(0x05);
    write_8bit(0x25);
    write_8bit(0x3a);
    write_8bit(0x55);
    write_8bit(0x50);
    write_8bit(0x3d);
    write_8bit(0x1c);
    write_8bit(0x1d);
    write_8bit(0x1d);
    write_8bit(0x1e);
    write_cmd(0x29);
	set_direction(0);//设置LCD显示方向 
}

//========================================================================
// 描述: LCD背光控制.
// 参数: 0：关闭背光；其他值：打开背光.
// 返回: none.
//========================================================================
void ST7789::backlight(uint8_t bright)
{
    pinMode(_blk_pin,GPIO_Mode_Out_PP);
	if(bright == 0)
	{
		digitalWrite(_blk_pin,0);
	}else{
		digitalWrite(_blk_pin,1);
	}
}

//========================================================================
// 描述: 设置显示方向.
// 参数: direction:0~3;
// 返回: none.
//========================================================================
void ST7789::set_direction(uint8_t direction)
{
	lcddev.dir = direction;
	switch (lcddev.dir) {
		case 0:
			lcddev.width = lcddev.width;
			lcddev.height = lcddev.height;
            write_cmd(0x36);
            /* BIT7:MY; BIT6:MX; BIT5:MV(行列交换); BIT4:ML; BIT3:0,RGB,1,BGR; BIT2:MH */
            write_8bit((0<<7)|(0<<6)|(0<<5)|(0<<4)|(ST7789_RGB_ORDER<<3));
			break;
		case 1:
			lcddev.width = lcddev.width;
			lcddev.height = lcddev.height;
            write_cmd(0x36);
            write_8bit((1<<7)|(0<<6)|(1<<5)|(0<<4)|(ST7789_RGB_ORDER<<3));
			break;
		case 2:
			lcddev.width = lcddev.width;
			lcddev.height = lcddev.height;
			write_cmd(0x36);
			write_8bit((1<<7)|(1<<6)|(0<<5)|(0<<4)|(ST7789_RGB_ORDER<<3));
			break;
		case 3:
			lcddev.width = lcddev.width;
			lcddev.height = lcddev.height;
			write_cmd(0x36);
			write_8bit((0<<7)|(1<<6)|(1<<5)|(0<<4)|(ST7789_RGB_ORDER<<3));
			break;
		default:
			break;
	}
}

//========================================================================
// 描述: LCD清屏.
// 参数: color:清屏的颜色.
// 返回: none.
//========================================================================
void ST7789::clear(uint16_t color)
{
    uint16_t i, j;
    set_windows(0, 0, lcddev.width - 1, lcddev.height - 1);
    for (i = 0; i < lcddev.width; i++)
    {
        for (j = 0; j < lcddev.height; j++)
        {
            write_16bit(color);
        }
    }
}

//========================================================================
// 描述: LCD快速清屏函数，用于沁恒官方e_card板快速刷新测试.
//          引脚已经固定，不受初始化控制。
// 参数: color:清屏的颜色.
// 返回: none.
//========================================================================
// void ST7789::clear_fast(uint16_t color)
// {
//     uint16_t i, j;
//     set_windows(0, 0, 239, 319);
//     for (i = 0; i < 240; i++)
//     {
//         for (j = 0; j < 320; j++)
//         {
//             GPIOC->OUTDR =  (1<<8)|(1<<10)|(1<<13)|(color >> 8);  //RD=1;RS=1;RES=1;CS=0;WR=0;
//             (GPIOC->OUTDR |= (GPIO_Pin_9));     //WR=1;
//             GPIOC->OUTDR =  (color&0xff)|(1<<8)|(1<<10)|(1<<13);  //RD=1;RS=1;RES=1;CS=0;WR=0;
//             (GPIOC->OUTDR |= (GPIO_Pin_9));     //WR=1;
//         }
//     }
// }

// void ST7789::show_picture_fast(uint16_t x,uint16_t y,uint16_t length,uint16_t width,const uint8_t pic[])
// {
// 	uint16_t i,j;
// 	uint32_t k=0;
// 	set_windows(x,y,x+length-1,y+width-1);
// 	for(i=0;i<length;i++)
// 	{
// 		for(j=0;j<width;j++)
// 		{
//             GPIOC->OUTDR =  (1<<8)|(1<<10)|(1<<13)|(pic[k*2]);  //RD=1;RS=1;RES=1;CS=0;WR=0;
//             (GPIOC->OUTDR |= (GPIO_Pin_9));     //WR=1;
//             GPIOC->OUTDR =  (pic[k*2+1])|(1<<8)|(1<<10)|(1<<13);  //RD=1;RS=1;RES=1;CS=0;WR=0;
//             (GPIOC->OUTDR |= (GPIO_Pin_9));     //WR=1;
// 			k++;
// 		}
// 	}		
// }

//========================================================================
// 描述: LCD画点.
// 参数: x,y: 坐标.
// 返回: none.
//========================================================================
void ST7789::draw_point(int16_t x, int16_t y, uint16_t color)
{
	set_windows( x, y, x, y );
	write_16bit( color );
}

//========================================================================
// 描述: LCD填充矩形.
// 参数: x0，y0:起始坐标; x1,y1:终止坐标; color:颜色
// 返回: none.
//========================================================================
void ST7789::fill_rectangle(int16_t x0, int16_t y0, int16_t x1, int16_t y1, int16_t color)
{
	uint16_t i, j;
	uint16_t width = x1 - x0 + 1; 		//得到填充的宽度
	uint16_t height = y1 - y0 + 1;		//高度
	set_windows(x0, y0, x1, y1);//设置显示窗口
	for (i = 0; i < height; i++)
	{
		for (j = 0; j < width; j++)
			write_16bit(color);
	}
	set_windows(0, 0, lcddev.width - 1, lcddev.height - 1);//恢复窗口设置为全屏
}

//========================================================================
// 描述: LCD显示图片.
// 参数: none.
// 返回: none.
//========================================================================
void ST7789::show_picture(uint16_t x,uint16_t y,uint16_t length,uint16_t width,const uint8_t pic[])
{
	uint16_t i,j;
	uint32_t k=0;
	set_windows(x,y,x+length-1,y+width-1);
	for(i=0;i<length;i++)
	{
		for(j=0;j<width;j++)
		{
            write_16bit(((uint16_t)pic[k*2+1]<<8)|pic[k*2]);
			k++;
		}
	}		
}

//========================================================================
// 描述: LCD读数据.
// 参数: none.
// 返回: 读到的值.
//========================================================================
uint16_t ST7789::read_data()
{
	uint16_t t;
    GPIO_InitTypeDef GPIO_InitStructure;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rs_pin/16)))->BSHR = 1<<(_rs_pin%16);  //RS=1;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_wr_pin/16)))->BSHR = 1<<(_wr_pin%16);  //WR=1;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
#if ST7789_DATA_PORT_TYPE==0     //低8位
    GPIO_InitStructure.GPIO_Pin = 0x00FF;
    GPIO_Init(ST7789_DATA_PORT, &GPIO_InitStructure);   //端口输入模式
    GPIOB->OUTDR |= 0x00ff;	
    //dummy data
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BCR = 1<<(_cs_pin%16);   //CS=0;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BCR = 1<<(_rd_pin%16);	//RD=0;
    delayMicroseconds(5);
    t = (uint8_t)ST7789_DATA_PORT-> INDR;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BSHR = 1<<(_rd_pin%16);	//RD=1;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BSHR = 1<<(_cs_pin%16);  //CS=1;
    //
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BCR = 1<<(_cs_pin%16);   //CS=0;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BCR = 1<<(_rd_pin%16);	//RD=0;
    delayMicroseconds(5);
    t = (uint8_t)ST7789_DATA_PORT-> INDR;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BSHR = 1<<(_rd_pin%16);	//RD=1;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BSHR = 1<<(_cs_pin%16);   //CS=1;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(ST7789_DATA_PORT, &GPIO_InitStructure);   //端口设置为输出模式
#elif  ST7789_DATA_PORT_TYPE==1      //高8位
    GPIO_InitStructure.GPIO_Pin = 0xFF00;
    GPIO_Init(ST7789_DATA_PORT, &GPIO_InitStructure);   //端口输入模式
    GPIOB->OUTDR |= 0xff00;	
    //dummy data
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BCR = 1<<(_cs_pin%16);   //CS=0;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BCR = 1<<(_rd_pin%16);	//RD=0;
    delayMicroseconds(1);
    t = (uint8_t)ST7789_DATA_PORT-> INDR;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BSHR = 1<<(_rd_pin%16);	//RD=1;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BSHR = 1<<(_cs_pin%16);   //CS=1;
    //
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BCR = 1<<(_cs_pin%16);   //CS=0;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BCR = 1<<(_rd_pin%16);	//RD=0;
    delayMicroseconds(1);
    t = (uint8_t)ST7789_DATA_PORT-> INDR;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BSHR = 1<<(_rd_pin%16);	//RD=1;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BSHR = 1<<(_cs_pin%16);   //CS=1;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(ST7789_DATA_PORT, &GPIO_InitStructure);   //端口设置为输出模式
#else   //16位并口
    GPIO_InitStructure.GPIO_Pin = 0xFFFF;
    GPIO_Init(ST7789_DATA_PORT, &GPIO_InitStructure);   //端口输入模式
    ST7789_DATA_PORT->OUTDR = 0xFFFF;	
    //dummy data
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BCR = 1<<(_cs_pin%16);   //CS=0;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BCR = 1<<(_rd_pin%16);	//RD=0;
    t = ST7789_DATA_PORT-> INDR;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BSHR = 1<<(_rd_pin%16);	//RD=1;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BSHR = 1<<(_cs_pin%16);   //CS=1;
    //
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BCR = 1<<(_cs_pin%16);   //CS=0;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BCR = 1<<(_rd_pin%16);	//RD=0;
    t = ST7789_DATA_PORT-> INDR;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BSHR = 1<<(_rd_pin%16);	//RD=1;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BSHR = 1<<(_cs_pin%16);   //CS=1;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(ST7789_DATA_PORT, &GPIO_InitStructure);   //端口设置为输出模式
#endif
	return t;
}

//========================================================================
// 描述: LCD读寄存器.
// 参数: reg:寄存器编号.
// 返回: 读到的值.
//========================================================================
uint16_t ST7789::read_reg(uint16_t reg)
{
    write_cmd(reg);
    return read_data();
}


//========================================================================
// 描述: 读取个某点的颜色值.
// 参数: x,y:坐标.
// 返回: 此点的颜色.
//========================================================================
uint16_t ST7789::read_point(uint16_t x, uint16_t y)
{
    uint16_t r,g,b;
    if(x>=lcddev.width||y>=lcddev.height)return 0;	//超过了范围,直接返回	
        
	set_windows(x, y, x, y);
	write_cmd(0x2E);
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rs_pin/16)))->BSHR = 1<<(_rs_pin%16);  //RS=1;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_wr_pin/16)))->BSHR = 1<<(_wr_pin%16);  //WR=1;
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
#if ST7789_DATA_PORT_TYPE==0     //低8位
    GPIO_InitStructure.GPIO_Pin = 0x00FF;
    GPIO_Init(ST7789_DATA_PORT, &GPIO_InitStructure);   //端口输入模式
    GPIOB->OUTDR |= 0x00ff;	//端口低8位输出高
    //dummy data
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BCR = 1<<(_cs_pin%16);   //CS=0;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BCR = 1<<(_rd_pin%16);	//RD=0;
    // delayMicroseconds(5);
    r = (uint8_t)ST7789_DATA_PORT-> INDR;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BSHR = 1<<(_rd_pin%16);	//RD=1;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BSHR = 1<<(_cs_pin%16);  //CS=1;
    //r
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BCR = 1<<(_cs_pin%16);   //CS=0;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BCR = 1<<(_rd_pin%16);	//RD=0;
    // delayMicroseconds(5);
    r = (uint8_t)ST7789_DATA_PORT-> INDR;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BSHR = 1<<(_rd_pin%16);	//RD=1;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BSHR = 1<<(_cs_pin%16);   //CS=1;     
    //g
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BCR = 1<<(_cs_pin%16);   //CS=0;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BCR = 1<<(_rd_pin%16);	//RD=0;
    // delayMicroseconds(5);
    g = (uint8_t)ST7789_DATA_PORT-> INDR;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BSHR = 1<<(_rd_pin%16);	//RD=1;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BSHR = 1<<(_cs_pin%16);   //CS=1;
    //b
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BCR = 1<<(_cs_pin%16);   //CS=0;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BCR = 1<<(_rd_pin%16);	//RD=0;
    // delayMicroseconds(5);
    b = (uint8_t)ST7789_DATA_PORT-> INDR;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BSHR = 1<<(_rd_pin%16);	//RD=1;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BSHR = 1<<(_cs_pin%16);   //CS=1;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(ST7789_DATA_PORT, &GPIO_InitStructure);   //端口设置为输出模式
    return ((r & 0xF8) << 8) | ((g & 0xFC) << 3) | ((b & 0xF8) >> 3);
#elif  ST7789_DATA_PORT_TYPE==1      //高8位
    GPIO_InitStructure.GPIO_Pin = 0xFF00;
    GPIO_Init(ST7789_DATA_PORT, &GPIO_InitStructure);   //端口上拉模式
    GPIOB->OUTDR |= 0xFF00;	//端口高8位输出高
    //dummy data
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BCR = 1<<(_cs_pin%16);   //CS=0;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BCR = 1<<(_rd_pin%16);	//RD=0;
    // delayMicroseconds(5);
    r = ST7789_DATA_PORT->INDR & 0xff00;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BSHR = 1<<(_rd_pin%16);	//RD=1;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BSHR = 1<<(_cs_pin%16);   //CS=1;
    //r
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BCR = 1<<(_cs_pin%16);   //CS=0;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BCR = 1<<(_rd_pin%16);	//RD=0;
    // delayMicroseconds(5);
    r = ST7789_DATA_PORT->INDR & 0xff00;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BSHR = 1<<(_rd_pin%16);	//RD=1;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BSHR = 1<<(_cs_pin%16);   //CS=1;
    //g
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BCR = 1<<(_cs_pin%16);   //CS=0;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BCR = 1<<(_rd_pin%16);	//RD=0;
    // delayMicroseconds(5);
    g = ST7789_DATA_PORT->INDR & 0xff00;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BSHR = 1<<(_rd_pin%16);	//RD=1;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BSHR = 1<<(_cs_pin%16);   //CS=1;
    //b
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BCR = 1<<(_cs_pin%16);   //CS=0;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BCR = 1<<(_rd_pin%16);	//RD=0;
    // delayMicroseconds(5);
    b = ST7789_DATA_PORT->INDR & 0xff00;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BSHR = 1<<(_rd_pin%16);	//RD=1;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BSHR = 1<<(_cs_pin%16);   //CS=1;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(ST7789_DATA_PORT, &GPIO_InitStructure);   //端口设置为输出模式
    r >>= 8;
    g >>= 8;
    b >>= 8;
    return ((r & 0xF8) << 8) | ((g & 0xFC) << 3) | ((b & 0xF8) >> 3);
#else   //16位并口
    GPIO_InitStructure.GPIO_Pin = 0xFFFF;
    GPIO_Init(ST7789_DATA_PORT, &GPIO_InitStructure);   //端口上拉模式
    ST7789_DATA_PORT->OUTDR = 0xFFFF;	//端口全部输出高
    //dummy data
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BCR = 1<<(_cs_pin%16);   //CS=0;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BCR = 1<<(_rd_pin%16);	//RD=0;
    // delayMicroseconds(5);
    r = ST7789_DATA_PORT-> INDR;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BSHR = 1<<(_rd_pin%16);	//RD=1;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BSHR = 1<<(_cs_pin%16);   //CS=1;
    //
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BCR = 1<<(_cs_pin%16);   //CS=0;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BCR = 1<<(_rd_pin%16);	//RD=0;
    // delayMicroseconds(5);
    b = ST7789_DATA_PORT-> INDR;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_rd_pin/16)))->BSHR = 1<<(_rd_pin%16);	//RD=1;
    ((GPIO_TypeDef *)(GPIOA_BASE + 0x400 * (_cs_pin/16)))->BSHR = 1<<(_cs_pin%16);   //CS=1;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(ST7789_DATA_PORT, &GPIO_InitStructure);   //端口设置为输出模式

    g = r&0xff;
    g<<=8;
    return (((r>>11)<<11)|((g>>10)<<5)|(b>>11));
#endif
}


#endif  //st7789.h