#ifndef _MB_Master_H
#define _MB_Master_H

#include "HardwareSerial.h"
#include "CRC_16.h"

#define MB_MASTER   1

// #define MBMASTER_USART1      1
// #define MBMASTER_USART2      1
// #define MBMASTER_USART3      1

uint8_t mb_m_rec_temp[32]; //保存主站接收到的数据
uint8_t mb_m_p = 0;   //串口接收到的指针
uint32_t mb_m_rec_tick = 0; //接收到数据暂存时间值单位0.1ms
uint32_t mb_m_tick = 0;     //时间tick单位0.1ms

class MBMaster{
public:
    MBMaster(USART_TypeDef* USARTx,uint32_t baud, uint16_t eParity);
    MBMaster(USART_TypeDef* USARTx,uint32_t baud, uint16_t eParity,int8_t rs485_pin,uint8_t level);
    void Init();     //modbus主机初始化
    int8_t ReqReadCoils(uint8_t MB_id, uint16_t addr, uint16_t uslen, uint8_t *databuf, uint16_t timeout); //读线圈 
    int8_t ReqWriteCoil(uint8_t MB_id, uint16_t addr,  uint8_t _data, uint16_t timeout); //写线圈
    int8_t ReqWriteCoil_S(uint8_t MB_id, uint16_t addr, uint16_t uslen, uint8_t *databuf, uint16_t timeout); //写多个线圈
    int8_t ReqReadDiscreteInputs(uint8_t MB_id, uint16_t addr, uint16_t uslen, uint8_t *databuf, uint16_t timeout); //读离散量输入
    int8_t ReqReadInputRegister(uint8_t MB_id, uint16_t addr, uint16_t uslen, uint8_t *databuf, uint16_t timeout); //读输入寄存器
    int8_t ReqWriteHoldingRegister(uint8_t MB_id, uint16_t addr, uint16_t  _data, uint16_t timeout); //写单个保持寄存器
    int8_t ReqWriteHoldingRegister_S(uint8_t MB_id, uint16_t addr, uint16_t uslen, uint8_t *databuf, uint16_t timeout); //写多个保持寄存器
    int8_t ReqReadHoldingRegister(uint8_t MB_id, uint16_t addr, uint16_t uslen, uint8_t *databuf, uint16_t timeout); //读保持寄存器
    int8_t ReqReadWriteHoldingRegister_S(uint8_t MB_id, uint16_t raddr, uint16_t ruslen,uint16_t waddr, uint16_t wuslen,uint8_t *databuf, uint16_t timeout); //读写保持寄存器
    void TimerCallback(); //定时器回调函数，需要每隔100us回调一次

private:
    uint8_t mb_m_command[32];    //保存主站要发送的数据
    uint8_t mb_m_data_temp[32]; //保存串口发送的数据

    uint8_t mb_m_rec_len = 0;         //接收长度
    uint16_t mb_m_j;                   //临时循环用变量
    uint16_t mb_m_crc_temp = 0; // 16位CRC临时变量

    uint8_t mb_m_rec_timeout = 0;     //超时值一般1.5到2个字
    uint8_t mb_m_rec_en = 0;          //接收到数据并CRC检验有效

    int8_t _rs485_pin = -1;
    uint8_t _level;
    USART_TypeDef* _USARTx;
    uint32_t _baud;
    uint16_t _eParity;
    void SerialInit(); //串口初始化
    void SerialPutByte(uint8_t ucByte); //串口发送字节
    void SerialPutBuff(uint8_t* p, uint16_t len);   //串口发送数组
    uint8_t SerialGetBuff();    //串口接收

    void RecDisable();  //关闭串口接收
    void RecEnble();    //使能串口接收
    void Send(uint8_t *p, uint16_t len);

};

MBMaster::MBMaster(USART_TypeDef* USARTx,uint32_t baud, uint16_t eParity)
{
    _USARTx = USARTx;
    _baud = baud;
    _eParity = eParity;
}

MBMaster::MBMaster(USART_TypeDef* USARTx,uint32_t baud, uint16_t eParity,int8_t rs485_pin,uint8_t level)
{
    _USARTx = USARTx;
    _baud = baud;
    _eParity = eParity;
    _rs485_pin = rs485_pin;
    _level = level;
}


//========================================================================
// 描述: 关闭串口接收.
// 参数: none.
// 返回: none.
//========================================================================
void MBMaster::RecDisable()
{
    USART_ITConfig(_USARTx, USART_IT_RXNE, DISABLE);
    USART_ClearITPendingBit(_USARTx,USART_IT_RXNE);

    mb_m_tick = 0;
    mb_m_rec_tick = 0;
    mb_m_p = 0;
    mb_m_rec_en = 0;
}

//========================================================================
// 描述: 使能串口接收.
// 参数: none.
// 返回: none.
//========================================================================
void MBMaster::RecEnble()
{
    mb_m_tick = 0;
    mb_m_rec_tick = 0;
    mb_m_p = 0;
    mb_m_rec_en = 0;
    USART_ITConfig(_USARTx, USART_IT_RXNE, ENABLE);
}

//========================================================================
// 描述: modbus发送函数(内部调用)
// 参数: p: 发送的数组; len:发送多少字节.
// 返回: none.
//========================================================================
void MBMaster::Send(uint8_t *p, uint16_t len)
{
    uint16_t crc16_temp;
    uint8_t j;
    for (j = 0; j < len; j++)
        mb_m_data_temp[j] = p[j];
    crc16_temp = CRC16(p, len);
    mb_m_data_temp[len] = crc16_temp;
    mb_m_data_temp[len + 1] = (crc16_temp >> 8);
   
    if(_rs485_pin >= 0){
        digitalWrite(_rs485_pin, _level);
    } 
    RecDisable();
    SerialPutBuff(mb_m_data_temp, len + 2); //数组输出
	while(USART_GetFlagStatus(_USARTx, USART_FLAG_TC) == RESET);//等待发送完成，再切换485控制引脚
    RecEnble();
    if(_rs485_pin >= 0){
        digitalWrite(_rs485_pin, !(_level));
    }
}

//========================================================================
// 描述: modbus读线圈函数
// 参数: MB_id:从站ID; addr:地址; uslen:长度; databuf:读回来的线圈值指针; timeout:超时时间.
// 返回: none.
//========================================================================
int8_t MBMaster::ReqReadCoils(uint8_t MB_id, uint16_t addr, uint16_t uslen, uint8_t *databuf, uint16_t timeout)
{
    uint16_t temp;
    uint8_t j;
    
    mb_m_command[0] = MB_id;//站号
    mb_m_command[1] = 0x01;//功能号
    temp = addr;
    mb_m_command[2] = (uint8_t)(temp >> 8);//地址高
    mb_m_command[3] = addr;//地址低
    temp = uslen;
    mb_m_command[4] = (uint8_t)(temp >> 8);//寄存器高
    mb_m_command[5] = uslen;//寄存器低
    Send(mb_m_command, 6);//发送
    
    (uslen % 8) ? (uslen = uslen / 8 + 1) : (uslen = uslen / 8);
    mb_m_j = 0;
    while(mb_m_j < timeout) //判断超时
    {
        delay(1);
        mb_m_j++;
        if(mb_m_rec_en)break;
    }

    if (mb_m_rec_en)//校验成功
    {

        if (mb_m_rec_temp[0] == MB_id && mb_m_rec_temp[1] == 0x01)//返回正确数据
        {
        for (j = 0; j < uslen; j++)
            databuf[j] = mb_m_rec_temp[3 + j];
            return 1;// 成功
        }else{
            return -1;//不正确类型
        }
    }else{
        return 0;//超时
    }
}

//========================================================================
// 描述: modbus写线圈函数
// 参数: MB_id:从站ID; addr:地址; _databuf:线圈值; timeout:超时时间.
// 返回: none.
//========================================================================
int8_t MBMaster::ReqWriteCoil(uint8_t MB_id, uint16_t addr,  uint8_t _data, uint16_t timeout)
{
    uint16_t temp;

    mb_m_command[0] = MB_id;//站号
    mb_m_command[1] = 0x05;//功能号
    temp = addr;
    mb_m_command[2] = (uint8_t)(temp >> 8);//地址高
    mb_m_command[3] = addr;//地址低

    mb_m_command[4] = (_data?0xff:0x00);
    mb_m_command[5] = 0x00;
    Send(mb_m_command, 6);//发送

    
    mb_m_j = 0;
    while(mb_m_j < timeout)//判断超时
    {
        delay(1);
        mb_m_j++;
        if(mb_m_rec_en)break;
    }

    if (mb_m_rec_en)//校验成功
    {

        if (mb_m_rec_temp[0] == MB_id && mb_m_rec_temp[1] == 0x05)//返回正确数据
        {
            return 1;
        }else{
            return -1;//不正确类型
        }
    }else{
        return 0;
    }
}

//========================================================================
// 描述: modbus写多个线圈函数
// 参数: MB_id:从站ID; addr:地址; uslen:长度; _databuf:线圈值; timeout:超时时间.
// 返回: none.
//========================================================================
int8_t MBMaster::ReqWriteCoil_S(uint8_t MB_id, uint16_t addr, uint16_t uslen, uint8_t *databuf, uint16_t timeout)
{
    uint16_t temp;
    uint8_t j;

    mb_m_command[0] = MB_id;//站号
    mb_m_command[1] = 0x0F;//功能号
    temp = addr;
    mb_m_command[2] = (uint8_t)(temp >> 8);//地址高
    mb_m_command[3] = addr;//地址低
    temp = uslen;
    mb_m_command[4] = (uint8_t)(temp >> 8);//寄存器高
    mb_m_command[5] = uslen;//寄存器低
    (uslen % 8) ? (uslen = uslen / 8 + 1) : (uslen = uslen / 8);
    mb_m_command[6] = uslen;//字节数
    for(j=0;j<uslen;j++)mb_m_command[7+j]=databuf[j];
    
    Send(mb_m_command, 7+uslen);//发送

    
    mb_m_j = 0;
    while(mb_m_j < timeout)//判断超时
    {
        delay(1);
        mb_m_j++;
        if(mb_m_rec_en)break;
    }

    if (mb_m_rec_en)//校验成功
    {
        if (mb_m_rec_temp[0] == MB_id && mb_m_rec_temp[1] == 0x0F)//返回正确数据
        {
            return 1;
        }else{
            return -1;//不正确类型
        } 
    }else{
        return 0;
    }
}

//========================================================================
// 描述: modbus读离散量输入函数
// 参数: MB_id:从站ID; addr:地址; uslen:长度; databuf:离散量值指针; timeout:超时时间.
// 返回: none.
//========================================================================
int8_t MBMaster::ReqReadDiscreteInputs(uint8_t MB_id, uint16_t addr, uint16_t uslen, uint8_t *databuf, uint16_t timeout)
{
    uint16_t temp;
    uint8_t j;

    mb_m_command[0] = MB_id;//站号
    mb_m_command[1] = 0x02;//功能号
    temp = addr;
    mb_m_command[2] = (uint8_t)(temp >> 8);//地址高
    mb_m_command[3] = addr;//地址低
    temp = uslen;
    mb_m_command[4] = (uint8_t)(temp >> 8);//寄存器数量高
    mb_m_command[5] = uslen;//寄存器数量低
    Send(mb_m_command, 6);//发送

    (uslen % 8) ? (uslen = uslen / 8 + 1) : (uslen = uslen / 8);

    mb_m_j = 0;
    while(mb_m_j < timeout)//判断超时
    {
        delay(1);
        mb_m_j++;
        if(mb_m_rec_en)break;
    }

    if (mb_m_rec_en)//校验成功
    {

        if (mb_m_rec_temp[0] == MB_id && mb_m_rec_temp[1] == 0x02)//返回正确数据
        {
        for (j = 0; j < uslen; j++)
            databuf[j] = mb_m_rec_temp[3 + j];
            return 1;
        }else{
            return -1;//不正确类型
        }
    }else{
        return 0;
    }
}

//========================================================================
// 描述: modbus读输入寄存器函数
// 参数: MB_id:从站ID; addr:地址; uslen:长度; databuf:输入寄存器值指针; timeout:超时时间.
// 返回: none.
//========================================================================
int8_t MBMaster::ReqReadInputRegister(uint8_t MB_id, uint16_t addr, uint16_t uslen, uint8_t *databuf, uint16_t timeout)
{
    uint16_t temp;
    uint8_t j;

    mb_m_command[0] = MB_id;//站号
    mb_m_command[1] = 0x04;//功能号
    temp = addr;
    mb_m_command[2] = (uint8_t)(temp >> 8);//地址高
    mb_m_command[3] = addr;//地址低
    temp = uslen;
    mb_m_command[4] = (uint8_t)(temp >> 8);//寄存器高
    mb_m_command[5] = uslen;//寄存器低
    Send(mb_m_command, 6);//发送


    mb_m_j = 0;
    while(mb_m_j < timeout)//判断超时
    {
        delay(1);
        mb_m_j++;
        if(mb_m_rec_en)break;
    }

    if (mb_m_rec_en)//校验成功
    {

        if (mb_m_rec_temp[0] == MB_id && mb_m_rec_temp[1] == 0x04)//返回正确数据
        {
        for (j = 0; j < uslen*2; j++)
            databuf[j] = mb_m_rec_temp[3 + j];
            return 1;
        }else{
            return -1;//不正确类型
        }  
    }else{
        return 0;
    }
}

//========================================================================
// 描述: modbus写单个寄存器函数
// 参数: MB_id:从站ID; addr:地址; _data:要写入的寄存器值; timeout:超时时间.
// 返回: none.
//========================================================================
int8_t MBMaster::ReqWriteHoldingRegister(uint8_t MB_id, uint16_t addr, uint16_t  _data, uint16_t timeout)
{
    uint16_t temp;

    mb_m_command[0] = MB_id;//站号
    mb_m_command[1] = 0x06;//功能号
    temp = addr;
    mb_m_command[2] = (uint8_t)(temp >> 8);//地址高
    mb_m_command[3] = addr;//地址低
    temp =_data;
    mb_m_command[4] = (uint8_t)(temp >> 8);//字节高
    mb_m_command[5] = _data;//字节低

    Send(mb_m_command, 6);//发送

    mb_m_j = 0;
    while(mb_m_j < timeout)//判断超时
    {
        delay(1);
        mb_m_j++;
        if(mb_m_rec_en)break;
    }

    if (mb_m_rec_en)//校验成功
    {
        if (mb_m_rec_temp[0] == MB_id && mb_m_rec_temp[1] == 0x06)//返回正确数据
        {
            return 1;
        }else{
            return -1;//不正确类型
        }
    }else{
        return 0;
    }
}

//========================================================================
// 描述: modbus写多个寄存器函数
// 参数: MB_id:从站ID; addr:地址; uslen:长度; databuf:要写的寄存器值指针; timeout:超时时间.
// 返回: none.
//========================================================================
int8_t MBMaster::ReqWriteHoldingRegister_S(uint8_t MB_id, uint16_t addr, uint16_t uslen, uint8_t *databuf, uint16_t timeout)
{
    uint16_t temp;
    uint8_t j;
  
    mb_m_command[0] = MB_id;//站号
    mb_m_command[1] = 0x10;//功能号
    temp = addr;
    mb_m_command[2] = (uint8_t)(temp >> 8);//地址高
    mb_m_command[3] = addr;//地址低
    temp = uslen;
    mb_m_command[4] = (uint8_t)(temp >> 8);//寄存器高
    mb_m_command[5] = uslen;//寄存器低

    mb_m_command[6] = uslen*2;//
    for(j=0;j<uslen*2;j++)mb_m_command[7+j]=databuf[j];
    
    Send(mb_m_command, 7+uslen*2);//发送

    
    mb_m_j = 0;
    while(mb_m_j < timeout)//判断超时
    {
        delay(1);
        mb_m_j++;
        if(mb_m_rec_en)break;
    }

    if (mb_m_rec_en)//校验成功
    {
        if (mb_m_rec_temp[0] == MB_id && mb_m_rec_temp[1] == 0x10)//返回正确数据
        {

            return 1;
        }else{
            return -1;//不正确类型
        }
    }else{
        return 0;
    }
}

//========================================================================
// 描述: modbus读保持寄存器函数
// 参数: MB_id:从站ID; addr:地址; uslen:长度; _data:要写入的寄存器值; timeout:超时时间.
// 返回: none.
//========================================================================
int8_t MBMaster::ReqReadHoldingRegister(uint8_t MB_id, uint16_t addr, uint16_t uslen, uint8_t *databuf, uint16_t timeout)
{
    uint16_t temp;
    uint8_t j;

    mb_m_command[0] = MB_id;//站号
    mb_m_command[1] = 0x03;//功能号
    temp = addr;
    mb_m_command[2] = (uint8_t)(temp >> 8);//地址高
    mb_m_command[3] = addr;//地址低
    temp = uslen;
    mb_m_command[4] = (uint8_t)(temp >> 8);//寄存器高
    mb_m_command[5] = uslen;//寄存器低
    Send(mb_m_command, 6);//发送

    mb_m_j = 0;
    while(mb_m_j < timeout)//判断超时
    {
        delay(1);
        mb_m_j++;
        if(mb_m_rec_en)break;
    }

    if (mb_m_rec_en)//校验成功
    {
        if (mb_m_rec_temp[0] == MB_id && mb_m_rec_temp[1] == 0x03)//返回正确数据
        {
        for (j = 0; j < uslen*2; j++)
            databuf[j] = mb_m_rec_temp[3 + j];
            return 1;
        }else{
            return -1;//不正确类型
        }
    }else{
        return 0;
    }
}

//========================================================================
// 描述: modbus读写多个保持寄存器函数
// 参数: MB_id:从站ID; raddr:读起始地址; ruslen:读的长度; 
//       waddr:写起始地址; wuslen:写长度; databuf:读写指针; timeout:超时时间.
// 返回: none.
//========================================================================
int8_t MBMaster::ReqReadWriteHoldingRegister_S(uint8_t MB_id, uint16_t raddr, uint16_t ruslen,uint16_t waddr, uint16_t wuslen,uint8_t *databuf, uint16_t timeout)
{
    uint16_t temp;
    uint8_t j;
  
    mb_m_command[0] = MB_id;//站号
    mb_m_command[1] = 0x17;//功能号
    temp = raddr;
    mb_m_command[2] = (uint8_t)(temp >> 8);//读起始地址高字节
    mb_m_command[3] = raddr;//读起始地址低字节
    temp = ruslen;
    mb_m_command[4] = (uint8_t)(temp >> 8);//读的数量高字节
    mb_m_command[5] = ruslen;//读的数量低字节

    temp = waddr;
    mb_m_command[6] = (uint8_t)(temp >> 8);//写起始地址高字节
    mb_m_command[7] = waddr;//写起始地址低字节
    temp = wuslen;
    mb_m_command[8] = (uint8_t)(temp >> 8);//写的数量高字节
    mb_m_command[9] = wuslen;//写的数量低字节
    mb_m_command[10] = wuslen*2;//
    
    for(j=0;j<wuslen*2;j++)mb_m_command[11+j]=databuf[j];
    
    Send(mb_m_command, 11+wuslen*2);//发送

    
    mb_m_j = 0;
    while(mb_m_j < timeout)//判断超时
    {
        delay(1);
        mb_m_j++;
        if(mb_m_rec_en)break;
    }

    if (mb_m_rec_en)//校验成功
    {
        if (mb_m_rec_temp[0] == MB_id && mb_m_rec_temp[1] == 0x17)//返回正确数据
        {
            for (j = 0; j < ruslen*2; j++)
                databuf[j] = mb_m_rec_temp[3 + j];
                return 1;
            }else{
                return -1;//不正确类型
            }
    }else{
        return 0;
    }
}

//========================================================================
// 描述: Modbus主站初始化函数
// 参数: none.
// 返回: none.
//========================================================================
void MBMaster::Init()
{
    if(_rs485_pin >= 0){
        pinMode(_rs485_pin, GPIO_Mode_Out_PP);
    }
        
    SerialInit();
    mb_m_rec_timeout = 200000 / _baud; 
    RecDisable();
}

//========================================================================
// 描述: 串口初始化，打开串口中断（内部调用）.
// 参数: uart_n:串口号; uart_rx_pin,uart_tx_pin:引脚; baud:波特率;
//      eParity:校验位
//              USART_Parity_No无校验  USART_Parity_Even：偶校验
//              USART_Parity_Odd：奇校验
// 返回: none.
//========================================================================
void MBMaster::SerialInit()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;
    if (_USARTx == USART1)
    {
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA, ENABLE);
        /* USART1 TX-->A.9   RX-->A.10 */
        GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
        GPIO_Init(GPIOA, &GPIO_InitStructure);
        GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
        GPIO_Init(GPIOA, &GPIO_InitStructure);

        USART_InitStructure.USART_BaudRate = _baud;
        USART_InitStructure.USART_WordLength = USART_WordLength_8b;
        USART_InitStructure.USART_StopBits = USART_StopBits_1;
        USART_InitStructure.USART_Parity = _eParity;
        USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
        USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
        USART_Init(USART1, &USART_InitStructure);
        USART_Cmd(USART1, ENABLE);

        USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
        NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
        NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
        NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
        NVIC_Init(&NVIC_InitStructure);
    }
    else if (_USARTx == USART2)
    {
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
        /* USART2 TX-->A.2   RX-->A.3 */
        GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
        GPIO_Init(GPIOA, &GPIO_InitStructure);
        GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
        GPIO_Init(GPIOA, &GPIO_InitStructure);

        USART_InitStructure.USART_BaudRate = _baud;
        USART_InitStructure.USART_WordLength = USART_WordLength_8b;
        USART_InitStructure.USART_StopBits = USART_StopBits_1;
        USART_InitStructure.USART_Parity = _eParity;
        USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
        USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
        USART_Init(USART2, &USART_InitStructure);
        USART_Cmd(USART2, ENABLE);

        USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
        NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
        NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
        NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
        NVIC_Init(&NVIC_InitStructure);
    }
    else
    {
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
        /* USART3 TX-->B.10  RX-->B.11 */
        GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
        GPIO_Init(GPIOB, &GPIO_InitStructure);
        GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
        GPIO_Init(GPIOB, &GPIO_InitStructure);

        USART_InitStructure.USART_BaudRate = _baud;
        USART_InitStructure.USART_WordLength = USART_WordLength_8b;
        USART_InitStructure.USART_StopBits = USART_StopBits_1;
        USART_InitStructure.USART_Parity = _eParity;
        USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
        USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;

        USART_Init(USART3, &USART_InitStructure);
        USART_Cmd(USART3, ENABLE);
        USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
        NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
        NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
        NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
        NVIC_Init(&NVIC_InitStructure);
    }
}

//========================================================================
// 描述: 串口写1字节函数（内部调用）.
// 参数: none.
// 返回: none.
//========================================================================
void MBMaster::SerialPutByte(uint8_t ucByte)
{
    USART_SendData(_USARTx, ucByte);
    while(USART_GetFlagStatus(_USARTx, USART_FLAG_TXE) == RESET);
}

//========================================================================
// 描述: 串口发送数组（内部调用）.
// 参数: none.
// 返回: none.
//========================================================================
void MBMaster::SerialPutBuff(uint8_t* p, uint16_t len)
{
	while (len--){
        SerialPutByte(*p++);
    }	
}

//========================================================================
// 描述: 串口读1字节函数（内部调用）.
// 参数: none.
// 返回: none.
//========================================================================
uint8_t MBMaster::SerialGetBuff()
{
    return USART_ReceiveData(_USARTx);
}

//========================================================================
// 描述: modbus主站定时器回调函数（在定时器中断中每100us调用一次）
// 参数: none.
// 返回: none.
//========================================================================
void MBMaster::TimerCallback()
{
    mb_m_tick = mb_m_tick + 1;
    if (mb_m_p > 5)
    {
        if (mb_m_tick - mb_m_rec_tick > mb_m_rec_timeout)
        {
            USART_ITConfig(_USARTx, USART_IT_RXNE, DISABLE);
            mb_m_rec_len = mb_m_p;
            mb_m_p = 0;
            mb_m_crc_temp = CRC16(mb_m_rec_temp, mb_m_rec_len - 2);
            if ((mb_m_rec_temp[mb_m_rec_len - 1] == (mb_m_crc_temp >> 8)) && (mb_m_rec_temp[mb_m_rec_len - 2] == (uint8_t)mb_m_crc_temp))
            {
                mb_m_rec_en = 1;
            }
        }
    }
}

//========================================================================
// 描述: 串口中断函数.
// 参数: none.
// 返回: none.
//========================================================================
extern "C"
{
#if (MBMASTER_USART1)
void USART1_IRQHandler(void)
{
    if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
    {
        USART_ClearITPendingBit(USART1,USART_IT_RXNE);
        mb_m_rec_temp[mb_m_p] = USART_ReceiveData(USART1);
        mb_m_p++;
        mb_m_rec_tick = mb_m_tick;
    }
}
#endif

#if (MBMASTER_USART2)
    void USART2_IRQHandler(void)
    {
        if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
        {
            USART_ClearITPendingBit(USART2,USART_IT_RXNE);
            mb_m_rec_temp[mb_m_p] = USART_ReceiveData(USART2);
            mb_m_p++;
            mb_m_rec_tick = mb_m_tick;
        }
    }
#endif

#if (MBMASTER_USART3)
void USART3_IRQHandler(void)
{
    if(USART_GetITStatus(USART3, USART_IT_RXNE) != RESET)
    {
        USART_ClearITPendingBit(USART3,USART_IT_RXNE);
        mb_m_rec_temp[mb_m_p] = USART_ReceiveData(USART3);
        mb_m_p++;
        mb_m_rec_tick = mb_m_tick;
    }
}
#endif
}   //extern "C"


#endif