#ifndef _CH32V_USBHD_H
#define _CH32V_USBHD_H

#include "CHRV3UFI.h"

/* Global Variable */
__attribute__((aligned(4))) UINT8 RxBuffer[MAX_PACKET_SIZE]; // IN, must even address
__attribute__((aligned(4))) UINT8 TxBuffer[MAX_PACKET_SIZE]; // OUT, must even address
__attribute__((aligned(4))) UINT8 Com_Buffer[128];

// extern SetUsbSpeed(uint8_t);  //设置USB速度
void USBHD_init();  //USB主机初始化
int8_t USBHD_UIF_detect();  //检测到USB设备连接或者断开
int8_t USBHD_InitRootDevice(); //初始化根设备
int8_t USBHD_DiskReady(); //磁盘准备完成
int8_t USBHD_create(const char *filename);  //新建文件并打开
int8_t USBHD_open(const char *filename); //打开文件
int8_t USBHD_enum(const char *filename, uint16_t j); //枚举文件
uint16_t USBHD_read(uint8_t *buf,uint16_t len); //读文件
uint16_t USBHD_write(uint8_t *buf,uint16_t len); //写文件
void USBHD_lseek(uint16_t offset); //设置文件偏移
int8_t USBHD_Close(); //关闭文件
int8_t USBHD_erase(const char *filename); //删除并关闭

/*******************************************************************************
* 函数名  : USBHD_ClockCmd
* 描述    : 设置USB时钟.
* 输入    : None
* 返回    : None
*******************************************************************************/
void USBHD_ClockCmd(UINT32 RCC_USBCLKSource, FunctionalState NewState)
{
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, NewState);
    EXTEN->EXTEN_CTR |= EXTEN_USBHD_IO_EN;
    RCC_USBCLKConfig(RCC_USBCLKSource); //USBclk=PLLclk/1.5=48Mhz
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_USBHD, NewState);
}

/*******************************************************************************
* 函数名  : USBHD_Init
* 描述    : 设置USB时钟
* 输入    : None
* 返回    : None
*******************************************************************************/
void USBHD_init()
{
    USBHD_ClockCmd(RCC_USBCLKSource_PLLCLK_1Div5, ENABLE);  //USB时钟设置
    pHOST_RX_RAM_Addr = RxBuffer;   //缓存区设置
    pHOST_TX_RAM_Addr = TxBuffer;
    USB_HostInit();
    CHRV3LibInit();  //初始化CHRV3程序库
}

/*******************************************************************************
* 函数名  : USBHD_UIF_detect
* 描述    : 检测到USB设备连接或者断开
* 输入    : None
* 返回    : 返回1则有设备连接上, -1代表设备断开, 0代表设备没有检测到设备连接或断开
*******************************************************************************/
int8_t USBHD_UIF_detect()
{
    uint8_t s;
    s = ERR_SUCCESS;
    if (R8_USB_INT_FG & RB_UIF_DETECT)  //检测到 USB 设备连接或断开触发
    {
        R8_USB_INT_FG = RB_UIF_DETECT;

        s = AnalyzeRootHub();
        if (s == ERR_USB_CONNECT)
        {
            return 1;
        }
        if (s == ERR_USB_DISCON)
        {
            return -1;
        }
    }
    return 0;
}

/*******************************************************************************
* 函数名  : USBHD_InitRootDevice
* 描述    : 初始化根设备
* 输入    : None
* 返回    : 返回1则初始化根设备成功, 0代表失败
*******************************************************************************/
int8_t USBHD_InitRootDevice()
{
    uint8_t s;
    s = InitRootDevice(Com_Buffer);
    if(s == ERR_SUCCESS){
        return 1;
    }else{
        return 0;
    }
}

/*******************************************************************************
* 函数名  : USBHD_DiskReady
* 描述    : 磁盘准备完成
* 输入    : None
* 返回    : 返回1则初始化根设备成功, 0代表失败
*******************************************************************************/
int8_t USBHD_DiskReady()
{
    uint8_t i, s;
    CHRV3DiskStatus = DISK_USB_ADDR; 
    for (i = 0; i != 10; i++)
    {
        s = CHRV3DiskReady(); //等待设备准备好
        if (s == ERR_SUCCESS)
        {
            break;
        }else{
            return 0;
        }
        Delay_Ms(50);
    }
    if(CHRV3DiskStatus >= DISK_MOUNTED){
        return 1;
    }
    return 0;
}

/*******************************************************************************
* 函数名  : USBHD_create
* 描述    : 新建文件并打开（如果文件已经存在则删除后再新建）
* 输入    : filename:文件名(需要大写)
* 返回    : 返回1则打开文件成功, 0代表失败
*******************************************************************************/
int8_t USBHD_create(const char *filename)
{
    uint8_t s;  
    strcpy( (PCHAR)mCmdParam.Create.mPathName, (char *)filename ); 
    s = CHRV3FileCreate( );   /* 新建文件并打开,如果文件已经存在则先删除后再新建 */
	if ( s == ERR_SUCCESS )
	{
		return  1;    /* 操作成功 */
	}
    return 0;
}


/*******************************************************************************
* 函数名  : USBHD_open
* 描述    : 打开单个文件
* 输入    : filename:文件名(需要大写)
* 返回    : 返回1则打开文件成功, 0代表失败
*******************************************************************************/
int8_t USBHD_open(const char *filename)
{
    uint8_t s;  
    strcpy((char *)mCmdParam.Open.mPathName,(char *)filename);
    s = CHRV3FileOpen();
    if(s == ERR_FOUND_NAME)
    {
        return 1;
    }else if(s == ERR_MISS_FILE || s == ERR_MISS_FILE){   //没有找到文件
        return -1;
    }
    return 0;
}

/*******************************************************************************
* 函数名  : USBHD_enum
* 描述    : 枚举打开文件
* 输入    : filename:文件目录; j:指定搜索/枚举的序号
* 返回    : 返回1则打开文件成功, 0代表失败
*******************************************************************************/
int8_t USBHD_enum(const char *filename, uint16_t j)
{
    uint8_t i, s;  
    strcpy((char *)mCmdParam.Open.mPathName,(char *)filename);
    i = strlen((PCHAR)mCmdParam.Open.mPathName);
    mCmdParam.Open.mPathName[i] = 0xFF; //根据字符串长度将结束符替换为搜索的序号,从0到254,如果是0xFF即255则说明搜索序号在CHRV3vFileSize变量中
    CHRV3vFileSize = j;                 //指定搜索/枚举的序号
    s = CHRV3FileOpen();

    if(s == ERR_FOUND_NAME)
    {
        return 1;
    }else if(s == ERR_MISS_FILE){   //没有找到文件
        return -1;
    }
    return 0;
}

/*******************************************************************************
* 函数名  : USBHD_read
* 描述    : 读文件
* 输入    : buf:读数据缓存指针; len:长度
* 返回    : 返回实际读出的字节数
*******************************************************************************/
uint16_t USBHD_read(uint8_t *buf,uint16_t len)
{
    mCmdParam.ByteRead.mByteCount = len; /* 请求读取的字节数 */
    mCmdParam.ByteRead.mByteBuffer = buf; /* 读数据缓存 */
    CHRV3ByteRead();
    return mCmdParam.ByteRead.mByteCount;
}

/*******************************************************************************
* 函数名  : USBHD_write
* 描述    : 写文件
* 输入    : buf:写数据缓存指针; len:长度
* 返回    : 返回实际写入的字节数
*******************************************************************************/
uint16_t USBHD_write(uint8_t *buf,uint16_t len)
{
    mCmdParam.ByteWrite.mByteCount = len; /* 请求写入的字节数 */
    mCmdParam.ByteWrite.mByteBuffer = buf; /* 写数据缓存 */
    CHRV3ByteWrite();
    return mCmdParam.ByteWrite.mByteCount;
}

/*******************************************************************************
* 函数名  : USBHD_lseek
* 描述    : 以字节为单位移动当前指针
* 输入    : offset:移动的字节（0xffffffff代表移动到文件尾部）
* 返回    : 返回实际写入的字节数
*******************************************************************************/
void USBHD_lseek(uint16_t offset)
{
    mCmdParam.ByteLocate.mByteOffset = offset;
    CHRV3ByteLocate();
}

/*******************************************************************************
* 函数名  : USBHD_close
* 描述    : 关闭文件
* 输入    : none
* 返回    : none
*******************************************************************************/
int8_t USBHD_Close()
{
    return CHRV3FileClose();  
}

/*******************************************************************************
* 函数名  : USBHD_erase
* 描述    : 删除文件并关闭
* 输入    : none
* 返回    : none
*******************************************************************************/
int8_t USBHD_erase(const char *filename)
{
    uint8_t s;
    strcpy( (PCHAR)mCmdParam.Create.mPathName, (PCHAR)filename);  //将被删除的文件名
    s = CHRV3FileErase( );  //删除文件并关闭
    if(s == ERR_SUCCESS){
        return 1;
    }
    return 0;
}



#endif





















