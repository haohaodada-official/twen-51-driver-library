#ifndef __LUNXUN_H
#define __LUNXUN_H


#ifndef  TASKS_MAX
#define  TASKS_MAX			1   //定义通信接口
#endif
//1.定义一个结构体
typedef struct _TASK_COMPONENTS
{
	unsigned char Run;         //计时标志   0-间隔时间未到  1-间隔时间到
	unsigned char Yun;         //程序运行标志   0-不运行  1-运行
	unsigned int Timer;        //计时器
	unsigned int ItvTimer;     //任务运行间隔时间
	void (*TaskHook)(void);    //要运行的任务函数
}TASK_COMPONENTS;

void TaskReMarks(void); //此函数在中断中调用
void TaskProcess(void); //主循环调用

static TASK_COMPONENTS TaskComps[TASKS_MAX];//任务注册


/************************************************************
* 命令名 : TaskReMarks()
* 任务名 : 任务标志处理
* 参数 : 无
* 返回 :无
************************************************************/
void TaskReMarks(void) //此函数在中断中调用
{
	unsigned char i = 0;
	for(i = 0;i < TASKS_MAX; i++)
	{
		if(TaskComps[ i].Timer)
		{
			TaskComps[ i].Timer--;
			if(TaskComps[ i].Timer == 0)
			{
				TaskComps[ i].Timer = TaskComps[ i].ItvTimer;
				TaskComps[ i].Run = 1;   //任务可以运行
			}
		}
	}
}

/************************************************************
* 命令名 : TaskProcess()
* 任务名 : 任务标志处理
* 参数 : 无
* 返回 :无
************************************************************/
void TaskProcess(void) //主循环调用
{
	unsigned char i = 0;

	for(i = 0;i < TASKS_MAX;i++)
	{
		if(TaskComps[ i].Run)
		{
			if(TaskComps[ i].Yun)
		    {
				TaskComps[ i].TaskHook();
				TaskComps[ i].Run = 0;
			}
		}
	}
}





#endif