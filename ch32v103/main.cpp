#include <CH32V103.h>
#include "CH32V_TIM.h"

uint8_t cnt = 0;

void TIM_attachInterrupt_3() {
  TIM_Disable(TIM3);
  cnt = cnt + 1;
  if(cnt % 2){
    digitalWrite(PA15, 1);
    TIM_Duty_Updata(TIM3, 60000);
    TIM_Enable(TIM3);
  }
  else{
    digitalWrite(PA15, 0);
    TIM_Duty_Updata(TIM3, 80000);
    TIM_Enable(TIM3);
  }
}

int main(void)
{
  CH32_Init();
  TIM_attachInterrupt(TIM3, 1000, TIM_attachInterrupt_3);
  pinMode(PA15, GPIO_Mode_Out_PP);
  while(1){

  }
  return 1;
}
