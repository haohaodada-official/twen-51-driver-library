

#include <stdio.h>
#include <string.h>
#include <inttypes.h>

/*---------------------------------------------------------------------------------------------------------*/
/* Includes of local headers                                                                               */
/*---------------------------------------------------------------------------------------------------------*/
#include "CH32V103.h"
#include "HardwareSerial.h"

/*---------------------------------------------------------------------------------------------------------*/
/* Macro, type and constant definitions                                                                    */
/*---------------------------------------------------------------------------------------------------------*/

#define RX_BUFFER_SIZE 64

struct ring_buffer
{
	unsigned char buffer[RX_BUFFER_SIZE];
	int head;
	int tail;
};

ring_buffer rx_buffer1 = {{0}, 0, 0};
ring_buffer rx_buffer2 = {{0}, 0, 0};
ring_buffer rx_buffer3 = {{0}, 0, 0};
inline void store_char(unsigned char c, ring_buffer *rx_buffer)
{
	int i = (rx_buffer->head + 1) % RX_BUFFER_SIZE;

	// if we should be storing the received character into the location
	// just before the tail (meaning that the head would advance to the
	// current location of the tail), we're about to overflow the buffer
	// and so we don't write the character or advance the head.
	if (i != rx_buffer->tail)
	{
		rx_buffer->buffer[rx_buffer->head] = c;
		rx_buffer->head = i;
	}
}
extern "C"
{
  void USART1_IRQHandler(void)
  {

    uint8_t rec;
    if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
   {
     rec= USART_ReceiveData(USART1);


	  store_char(rec, &rx_buffer1);

	
    /* while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
      USART_SendData(USART1, rec);*/
   }

  }
 
void USART2_IRQHandler(void)
{
  uint8_t rec;
  if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
  {
	  rec= USART_ReceiveData(USART2);
	  store_char(rec, &rx_buffer2);


  }

}

/*******************************************************************************
* Function Name  : USART3_IRQHandler
* Description    : This function handles USART3 global interrupt request.
* Input          : None
* Return         : None
*******************************************************************************/
void USART3_IRQHandler(void)
{
 uint8_t rec;
  if(USART_GetITStatus(USART3, USART_IT_RXNE) != RESET)
  {
	  rec= USART_ReceiveData(USART3);
	  store_char(rec, &rx_buffer3);

  }
}

}
	/*---------------------------------------------------------------------------------------------------------*/
	/* Interrupt Handler                                                                                 	   */
	/*---------------------------------------------------------------------------------------------------------*/

	// Constructors ////////////////////////////////////////////////////////////////

	HardwareSerial::HardwareSerial(ring_buffer *rx_buffer, USART_TypeDef *UART)
	{
		
		_port = UART;
		_rx_buffer = rx_buffer;
		

		
	}

	// Public Methods //////////////////////////////////////////////////////////////

	void HardwareSerial::begin(uint32_t speed)
	{
		

		if (_port == USART1)
		{
			CH32V_UART1_Init(speed);
		}
		else if (_port == USART2)
		{
			CH32V_UART2_Init(speed);
		}
		else
		{
			CH32V_UART3_Init(speed);
		}


	}

	int HardwareSerial::available(void)
	{
	  //while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
      //USART_SendData(USART1, (RX_BUFFER_SIZE + _rx_buffer->head - _rx_buffer->tail) % RX_BUFFER_SIZE);
		return (RX_BUFFER_SIZE + _rx_buffer->head - _rx_buffer->tail) % RX_BUFFER_SIZE;
	}
	int HardwareSerial::peek(void)
	{
		if (_rx_buffer->head == _rx_buffer->tail)
		{
			return -1;
		}
		else
		{
			return _rx_buffer->buffer[_rx_buffer->tail];
		}
	}

	int HardwareSerial::read(void)
	{
		// if the head isn't ahead of the tail, we don't have any characters
		if (_rx_buffer->head == _rx_buffer->tail)
		{
			return -1;
		}
		else
		{
			unsigned char c = _rx_buffer->buffer[_rx_buffer->tail];
			_rx_buffer->tail = (_rx_buffer->tail + 1) % RX_BUFFER_SIZE;
			return c;
		}
	}

	void HardwareSerial::flush()
	{
		// don't reverse this or there may be problems if the RX interrupt
		// occurs after reading the value of rx_buffer_head but before writing
		// the value to rx_buffer_tail; the previous value of rx_buffer_head
		// may be written to rx_buffer_tail, making it appear as if the buffer
		// don't reverse this or there may be problems if the RX interrupt
		// occurs after reading the value of rx_buffer_head but before writing
		// the value to rx_buffer_tail; the previous value of rx_buffer_head
		// may be written to rx_buffer_tail, making it appear as if the buffer
		// were full, not empty.
		_rx_buffer->head = _rx_buffer->tail;
	}
	/*
void HardwareSerial::end()
{
  // wait for transmission of outgoing data
  while (_tx_buffer_head != _tx_buffer_tail)
    ;
  
  // clear any received data
  _rx_buffer_head = _rx_buffer_tail;
}*/

	size_t HardwareSerial::write(uint8_t ch)
	{
		if (_port == USART1)
		{
			CH32V_UART1_SendCh(ch);
		}
		else if (_port == USART2)
		{
			CH32V_UART2_SendCh(ch);
		}
		else
		{
			CH32V_UART3_SendCh(ch);
		}
		return 1;
	}

	// Preinstantiate Objects //////////////////////////////////////////////////////

	HardwareSerial Serial1(&rx_buffer1, USART1);
	HardwareSerial Serial2(&rx_buffer2, USART2);
	HardwareSerial Serial3(&rx_buffer3, USART3);
