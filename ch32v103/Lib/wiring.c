#include "ch32v10x.h"
#include "pins_arduino.h"
#include "wiring.h"

uint32_t millis()
{
    return ((*(volatile uint32_t *)0xE000F004) / 9000);
}

uint32_t micros(void)
{
    return ((*(volatile uint32_t *)0xE000F004) / 9);
}

void delay(uint32_t ms)
{

    uint32_t temp = ms * 9000;
    uint32_t start = (*(__IO uint32_t *)0xE000F004);

    while ((*(__IO uint32_t *)0xE000F004) - start < temp)
        ;
}
void delayMicroseconds(uint32_t us)
{
    uint32_t temp = us * 9;
    uint32_t start = (*(__IO uint32_t *)0xE000F004);

    while ((*(__IO uint32_t *)0xE000F004) - start < temp)
        ;
}

void CH32V_UART1_Init(uint32_t baudrate)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA, ENABLE);
    /* USART2 TX-->A.9   RX-->A.10 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    USART_InitStructure.USART_BaudRate = baudrate;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
    USART_Init(USART1, &USART_InitStructure);
    USART_Cmd(USART1, ENABLE);

    USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}
void CH32V_UART1_SendCh(unsigned char c)
{

    while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET)
        ;
    USART1->DATAR = (c & (uint16_t)0x01FF);
}

void CH32V_UART2_Init(uint32_t baudrate)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    /* USART2 TX-->A.2   RX-->A.3 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    USART_InitStructure.USART_BaudRate = baudrate;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
    USART_Init(USART2, &USART_InitStructure);
    USART_Cmd(USART2, ENABLE);

    USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}
void CH32V_UART2_SendCh(unsigned char c)
{
    while (USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET)
        ;
    USART2->DATAR = (c & (uint16_t)0x01FF);
}

void CH32V_UART3_Init(uint32_t baudrate)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
    /* USART3 TX-->B.10  RX-->B.11 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    USART_InitStructure.USART_BaudRate = baudrate;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;

    USART_Init(USART3, &USART_InitStructure);
    USART_Cmd(USART3, ENABLE);
    USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}
void CH32V_UART3_SendCh(unsigned char c)
{
    while (USART_GetFlagStatus(USART3, USART_FLAG_TC) == RESET)
        ;
    USART3->DATAR = (c & (uint16_t)0x01FF);
}
void CH32V_GPIOX_Init(GPIO_TypeDef *GPIOx,  GPIOMode_TypeDef mode, GPIOSpeed_TypeDef speed)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Pin =GPIO_Pin_All;
    GPIO_InitStructure.GPIO_Mode = mode;
    GPIO_InitStructure.GPIO_Speed = speed;
    GPIO_Init(GPIOx, &GPIO_InitStructure);
}
void CH32V_GPIO_Init(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin, uint8_t mode, uint8_t speed)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Pin = (1 << GPIO_Pin);
    GPIO_InitStructure.GPIO_Mode = mode;
    GPIO_InitStructure.GPIO_Speed = speed;
    GPIO_Init(GPIOx, &GPIO_InitStructure);
}
void pinMode(uint8_t pin, uint8_t mode)
{
    CH32V_GPIO_Init((GPIO_TypeDef *)DIGITAL_PIN_PORT[pin], DIGITAL_PIN_NUM[pin], mode, GPIO_Speed_50MHz);
}

void digitalWrite(uint8_t pin, uint8_t state)
{
    if (state == 0x1)
    {
        GPIO_SetBits((GPIO_TypeDef *)DIGITAL_PIN_PORT[pin], 1 << DIGITAL_PIN_NUM[pin]);
    }
    else
    {
        GPIO_ResetBits((GPIO_TypeDef *)DIGITAL_PIN_PORT[pin], 1 << DIGITAL_PIN_NUM[pin]);
    }
}
uint8_t digitalRead(uint8_t pin)
{
    return GPIO_ReadInputDataBit((GPIO_TypeDef *)DIGITAL_PIN_PORT[pin], 1 << DIGITAL_PIN_NUM[pin]);
}
void CH32V_ADC_Init(ADC_Name adcn, uint32_t sample, uint32_t resolution)
{
   ADC_InitTypeDef ADC_InitStructure;          //定义ADC结构体
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
    RCC_ADCCLKConfig(RCC_PCLK2_Div6);
    if (adcn < 8)
    {
        pinMode(adcn, GPIO_Mode_AIN);
    }
    else if (adcn < 10)
    {
        pinMode(adcn + 8, GPIO_Mode_AIN);
    }
    else if (adcn < 15)
    {
        pinMode(adcn + 14, GPIO_Mode_AIN);
    }


    if (adcn < 10)
    {
        ADC1->SAMPTR2 = sample << (3 * adcn);
    }
    else
    {
        ADC1->SAMPTR1 = sample << ((adcn - 10) * 3);
    }

    ADC_DeInit(ADC1);                            //复位ADC1，重设为默认缺省值
    
    ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;                  //ADC工作模式:ADC1和ADC2工作在独立模式
    ADC_InitStructure.ADC_ScanConvMode = DISABLE;                       //模数转换工作在单通道模式
    ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;                 //模数转换工作在单次转换模式
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None; //转换由软件而不是外部触发启动
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;              //ADC数据右对齐
    ADC_InitStructure.ADC_NbrOfChannel = 1;                             //顺序进行规则转换的ADC通道的数目
    ADC_Init(ADC1,&ADC_InitStructure);                                  //初始化ADC1
    
    ADC_Cmd(ADC1,ENABLE);                        //使能ADC1
    
    ADC_ResetCalibration(ADC1);                  //使能复位校准
    while(ADC_GetResetCalibrationStatus(ADC1));  //等待复位校准结束
    ADC_StartCalibration(ADC1);                  //开启AD校准
    while(ADC_GetCalibrationStatus(ADC1));       //等待校准结束

}
uint16_t ADC_Read(ADC_Name adcn)
{
    //配置ADCx，ADC通道，规则采样顺序，采样时间
    ADC_RegularChannelConfig(ADC1,adcn,1,ADC_SampleTime_55Cycles5);
    ADC_SoftwareStartConvCmd(ADC1,ENABLE);          //使能ADC1的软件转换启动功能
    while(!ADC_GetFlagStatus(ADC1,ADC_FLAG_EOC));   //等待转换结束
    return ADC_GetConversionValue(ADC1);            //返回最近一次ADC1规则组的转换结果
}

void CH32_Init()
{
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD, ENABLE);
    SysTick->CNTL0 = 0;
    SysTick->CNTL1 = 0;
    SysTick->CNTL2 = 0;
    SysTick->CNTL3 = 0;

    SysTick->CNTH0 = 0;
    SysTick->CNTH1 = 0;
    SysTick->CNTH2 = 0;
    SysTick->CNTH3 = 0;

    SysTick->CTLR = 1;
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    
 if ((RCC->RSTSCKR & (1 << 26)))
    {
        NVIC_SystemReset();
    }
    RCC->RSTSCKR |= (1 << 24);
    //按复位键重启
}

void _init(void)
{
}

void _fini(void)
{
}


