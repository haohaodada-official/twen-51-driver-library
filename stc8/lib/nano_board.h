/*************  技术支持与购买说明    **************
产品主页：http://twen51.com
淘宝搜索：天问51，可购买。基础版，带彩屏标准版，旗舰版
技术支持QQ群三：206225033
******************************************/

#ifndef __NANO_BOARD_H
#define __NANO_BOARD_H	
#include <STC8HX.h>

#define D0 P1_0
#define D1 P1_1
#define D2 P3_3
#define D3 P3_4
#define D4 P3_5
#define D5 P2_0
#define D6 P2_1
#define D7 P2_6
#define D8 P2_7
#define D9 P3_6
#define D10 P2_2
#define D11 P2_3
#define D12 P2_4
#define D13 P2_5
#define A0 P0_0
#define A1 P0_1
#define A2 P1_2
#define A3 P1_3
#define A4 P1_4
#define A5 P1_5
#define A6 P0_2
#define A7 P0_3

#endif