

#ifndef _STC8H_H_
#define _STC8H_H_
#define _nop_() __asm nop __endasm
#define code __code
#define  bit __bit
#define interrupt __interrupt
#define xdata __xdata
#define idata __idata
#define using __using
#define at __at
#define  asm __asm
#define endasm  __endasm

#include <compiler.h>

/*  BYTE Register  */
SFR(P0,     0x80);                  /* P0 */
    SBIT(P0_0,  0x80, 0);
    SBIT(P0_1,  0x80, 1);
    SBIT(P0_2,  0x80, 2);
    SBIT(P0_3,  0x80, 3);
    SBIT(P0_4,  0x80, 4);
    SBIT(P0_5,  0x80, 5);
    SBIT(P0_6,  0x80, 6);
    SBIT(P0_7,  0x80, 7);

SFR(SP,     0x81);
SFR(DPL,    0x82);
SFR(DPH,    0x83);
SFR(S4CON,   0x84);
SFR(S4BUF,   0x85);
SFR(PCON,   0x87);
SFR(TCON,   0x88);               /*  TCON  */ 
    SBIT(IT0,   0x88, 0);
    SBIT(IE0,   0x88, 1);
    SBIT(IT1,   0x88, 2);
    SBIT(IE1,   0x88, 3);
    SBIT(TR0,   0x88, 4);
    SBIT(TF0,   0x88, 5);
    SBIT(TR1,   0x88, 6);
    SBIT(TF1,   0x88, 7);
SFR(TMOD,   0x89);
SFR(TL0,    0x8A);
SFR(TL1,    0x8B);
SFR(TH0,    0x8C);
SFR(TH1,    0x8D);
SFR(AUXR,    0x8E);
SFR(INTCLKO,    0x8F);
/* P1 */
SFR(P1,     0x90);  
    SBIT(P1_0,  0x90, 0);
    SBIT(P1_1,  0x90, 1);
    SBIT(P1_2,  0x90, 2);
    SBIT(P1_3,  0x90, 3);
    SBIT(P1_4,  0x90, 4);
    SBIT(P1_5,  0x90, 5);
    SBIT(P1_6,  0x90, 6);
    SBIT(P1_7,  0x90, 7);
SFR(P1M1, 0x91); 
SFR(P1M0, 0x92);
SFR(P0M1, 0x93);   
SFR(P0M0, 0x94);
SFR(P2M1, 0x95);
SFR(P2M0, 0x96);

SFR(SCON,   0x98);
/*  SCON  */
    SBIT(RI,    0x98, 0);
    SBIT(TI,    0x98, 1);
    SBIT(RB8,   0x98, 2);
    SBIT(TB8,   0x98, 3);
    SBIT(REN,   0x98, 4);
    SBIT(SM2,   0x98, 5);
    SBIT(SM1,   0x98, 6);
    SBIT(SM0,   0x98, 7);
SFR(SBUF,     0x99);
SFR(S2CON,    0x9a);
SFR(S2BUF,    0x9b);
SFR(IRCBAND,  0x9d);
SFR(LIRTRIM,  0x9e);
SFR(IRTRIM,   0x9f);

SFR(P2,     0xA0); 
/* P2 */
    SBIT(P2_0,  0xA0, 0);
    SBIT(P2_1,  0xA0, 1);
    SBIT(P2_2,  0xA0, 2);
    SBIT(P2_3,  0xA0, 3);
    SBIT(P2_4,  0xA0, 4);
    SBIT(P2_5,  0xA0, 5);
    SBIT(P2_6,  0xA0, 6);
    SBIT(P2_7,  0xA0, 7);
SFR(P_SW1,     0xA2); 
SFR(IE,     0xA8); 
/*  IE   */
    SBIT(EX0,   0xA8, 0);
    SBIT(ET0,   0xA8, 1);
    SBIT(EX1,   0xA8, 2);
    SBIT(ET1,   0xA8, 3);
    SBIT(ES,    0xA8, 4);
    SBIT(EADC,    0xA8, 5);
    SBIT(ELVD,    0xA8, 6);
    SBIT(EA,    0xA8, 7);
    
SFR(SADDR,     0xA9); 
SFR(WKTCL,     0xAA); 
SFR(WKTCH,     0xAB); 
SFR(S3CON,     0xAC); 
SFR(S3BUF,     0xAD); 
SFR(TA,     0xAE); 
SFR(IE2,     0xAF);     

SFR(P3,     0xB0);
/*  P3  */
    SBIT(P3_0,  0xB0, 0);
    SBIT(P3_1,  0xB0, 1);
    SBIT(P3_2,  0xB0, 2);
    SBIT(P3_3,  0xB0, 3);
    SBIT(P3_4,  0xB0, 4);
    SBIT(P3_5,  0xB0, 5);
    SBIT(P3_6,  0xB0, 6);
    SBIT(P3_7,  0xB0, 7);

SFR(P3M1, 0xB1); 
SFR(P3M0, 0xB2);
SFR(P4M1, 0xB3);   
SFR(P4M0, 0xB4);
SFR(IP2, 0xB5);
SFR(IP2H, 0xB6);
SFR(IPH, 0xB7);

SFR(IP, 0xB8);
/*  IP   */
    SBIT(PX0,   0xB8, 0);
    SBIT(PT0,   0xB8, 1);
    SBIT(PX1,   0xB8, 2);
    SBIT(PT1,   0xB8, 3);
    SBIT(PS,    0xB8, 4);
    SBIT(PADC,  0xB8, 5);
    SBIT(PLVD,  0xB8, 6);
    SBIT(PPCA,  0xB8, 7);

SFR(SADEN, 0xB9);
SFR(P_SW2, 0xBA);
SFR(ADC_CONTR, 0xBC);
SFR(ADC_RES, 0xBD);
SFR(ADC_RESL, 0xBE);

SFR(P4, 0xC0);
/*  P4  */
    SBIT(P4_0, 0xC0, 0);
    SBIT(P4_1, 0xC0, 1);
    SBIT(P4_2, 0xC0, 2);
    SBIT(P4_3, 0xC0, 3);
    SBIT(P4_4, 0xC0, 4);
    SBIT(P4_5, 0xC0, 5);
    SBIT(P4_6, 0xC0, 6);
    SBIT(P4_7, 0xC0, 7);

SFR(WDT_CONTR, 0xC1);
SFR(IAP_DATA, 0xC2);
SFR(IAP_ADDRH, 0xC3);
SFR(IAP_ADDRL, 0xC4);
SFR(IAP_CMD, 0xC5);
SFR(IAP_TRIG, 0xC6);
SFR(IAP_CONTR, 0xC7);

SFR(P5, 0xC8);
/*  P5  */
    SBIT(P5_0, 0xC8, 0);
    SBIT(P5_1, 0xC8, 1);
    SBIT(P5_2, 0xC8, 2);
    SBIT(P5_3, 0xC8, 3);
    SBIT(P5_4, 0xC8, 4);
    SBIT(P5_5, 0xC8, 5);
    SBIT(P5_6, 0xC8, 6);
    SBIT(P5_7, 0xC8, 7);
SFR(P5M1, 0xC9); 
SFR(P5M0, 0xCA);
SFR(P6M1, 0xCB);   
SFR(P6M0, 0xCC);
SFR(SPSTAT, 0xCD);
SFR(SPCTL, 0xCE);
SFR(SPDAT, 0xCF);

SFR(PSW, 0xD0);
/*  PSW   */
    SBIT(P,     0xD0, 0);
    SBIT(F1,    0xD0, 1);
    SBIT(OV,    0xD0, 2);
    SBIT(RS0,   0xD0, 3);
    SBIT(RS1,   0xD0, 4);
    SBIT(F0,    0xD0, 5);
    SBIT(AC,    0xD0, 6);
    SBIT(CY,    0xD0, 7);
SFR(T4T3M, 0xD1);
SFR(T4H, 0xD2);
SFR(T4L, 0xD3);
SFR(T3H, 0xD4);
SFR(T3L, 0xD5);
SFR(T2H, 0xD6);
SFR(T2L, 0xD7);

SFR(USBCLK, 0xDC);
SFR(ADCCFG, 0xDE);
SFR(IP3, 0xDF);

SFR(ACC, 0xE0);
SFR(P7M1, 0xE1);
SFR(P7M0, 0xE2);
SFR(DPS, 0xE3);
SFR(DPL1, 0xE4);
SFR(DPH1, 0xE5);
SFR(CMPCR1, 0xE6);
SFR(CMPCR2, 0xE7);

SFR(P6, 0xE8);
/*  P6  */
    SBIT(P6_0, 0xE8, 0);
    SBIT(P6_1, 0xE8, 1);
    SBIT(P6_2, 0xE8, 2);
    SBIT(P6_3, 0xE8, 3);
    SBIT(P6_4, 0xE8, 4);
    SBIT(P6_5, 0xE8, 5);
    SBIT(P6_6, 0xE8, 6);
    SBIT(P6_7, 0xE8, 7);

SFR(USBDAT, 0xEC);
SFR(IP3H, 0xEE);
SFR(AUXINTIF, 0xEF); 

SFR(B, 0xF0);
SFR(USBCON, 0xF4);
SFR(IAP_TPS, 0xF5); 

SFR(P7, 0xF8);
/*  P6  */
    SBIT(P7_0, 0xF8, 0);
    SBIT(P7_1, 0xF8, 1);
    SBIT(P7_2, 0xF8, 2);
    SBIT(P7_3, 0xF8, 3);
    SBIT(P7_4, 0xF8, 4);
    SBIT(P7_5, 0xF8, 5);
    SBIT(P7_6, 0xF8, 6);
    SBIT(P7_7, 0xF8, 7);
SFR(USBADR, 0xFC);
SFR(RSTCFG, 0xFF);     

/* BIT definitions for bits that are not directly accessible */
/* PCON bits */
#define IDL             0x01
#define PD              0x02
#define GF0             0x04
#define GF1             0x08
#define SMOD            0x80

/* TMOD bits */
#define T0_M0           0x01
#define T0_M1           0x02
#define T0_CT           0x04
#define T0_GATE         0x08
#define T1_M0           0x10
#define T1_M1           0x20
#define T1_CT           0x40
#define T1_GATE         0x80

#define T0_MASK         0x0F
#define T1_MASK         0xF0

/* Interrupt numbers: address = (number * 8) + 3 */
#define IE0_VECTOR      0       /* 0x03 external interrupt 0 */
#define TF0_VECTOR      1       /* 0x0b timer 0 */
#define IE1_VECTOR      2       /* 0x13 external interrupt 1 */
#define TF1_VECTOR      3       /* 0x1b timer 1 */
#define SI0_VECTOR      4       /* 0x23 serial port 0 */


//�������⹦�ܼĴ���λ����չRAM����
//������Щ�Ĵ���,���Ƚ�P_SW2��BIT7����Ϊ1,�ſ�������д
#define     CKSEL       (*(unsigned char        volatile    xdata       *)0xfe00)
#define     CLKDIV      (*(unsigned char        volatile    xdata       *)0xfe01)
#define     HIRCCR      (*(unsigned char        volatile    xdata       *)0xfe02)
#define     XOSCCR      (*(unsigned char        volatile    xdata       *)0xfe03)
#define     IRC32KCR    (*(unsigned char        volatile    xdata       *)0xfe04)
#define     MCLKOCR     (*(unsigned char        volatile    xdata       *)0xfe05)

#define     P0PU        (*(unsigned char        volatile    xdata       *)0xfe10)
#define     P1PU        (*(unsigned char        volatile    xdata       *)0xfe11)
#define     P2PU        (*(unsigned char        volatile    xdata       *)0xfe12)
#define     P3PU        (*(unsigned char        volatile    xdata       *)0xfe13)
#define     P4PU        (*(unsigned char        volatile    xdata       *)0xfe14)
#define     P5PU        (*(unsigned char        volatile    xdata       *)0xfe15)
#define     P6PU        (*(unsigned char        volatile    xdata       *)0xfe16)
#define     P7PU        (*(unsigned char        volatile    xdata       *)0xfe17)
#define     P0NCS       (*(unsigned char        volatile    xdata       *)0xfe18)
#define     P1NCS       (*(unsigned char        volatile    xdata       *)0xfe19)
#define     P2NCS       (*(unsigned char        volatile    xdata       *)0xfe1a)
#define     P3NCS       (*(unsigned char        volatile    xdata       *)0xfe1b)
#define     P4NCS       (*(unsigned char        volatile    xdata       *)0xfe1c)
#define     P5NCS       (*(unsigned char        volatile    xdata       *)0xfe1d)
#define     P6NCS       (*(unsigned char        volatile    xdata       *)0xfe1e)
#define     P7NCS       (*(unsigned char        volatile    xdata       *)0xfe1f)
#define     P0SR        (*(unsigned char        volatile    xdata       *)0xfe20)
#define     P1SR        (*(unsigned char        volatile    xdata       *)0xfe21)
#define     P2SR        (*(unsigned char        volatile    xdata       *)0xfe22)
#define     P3SR        (*(unsigned char        volatile    xdata       *)0xfe23)
#define     P4SR        (*(unsigned char        volatile    xdata       *)0xfe24)
#define     P5SR        (*(unsigned char        volatile    xdata       *)0xfe25)
#define     P6SR        (*(unsigned char        volatile    xdata       *)0xfe26)
#define     P7SR        (*(unsigned char        volatile    xdata       *)0xfe27)
#define     P0DR        (*(unsigned char        volatile    xdata       *)0xfe28)
#define     P1DR        (*(unsigned char        volatile    xdata       *)0xfe29)
#define     P2DR        (*(unsigned char        volatile    xdata       *)0xfe2a)
#define     P3DR        (*(unsigned char        volatile    xdata       *)0xfe2b)
#define     P4DR        (*(unsigned char        volatile    xdata       *)0xfe2c)
#define     P5DR        (*(unsigned char        volatile    xdata       *)0xfe2d)
#define     P6DR        (*(unsigned char        volatile    xdata       *)0xfe2e)
#define     P7DR        (*(unsigned char        volatile    xdata       *)0xfe2f)
#define     P0IE        (*(unsigned char        volatile    xdata       *)0xfe30)
#define     P1IE        (*(unsigned char        volatile    xdata       *)0xfe31)
#define     P3IE        (*(unsigned char        volatile    xdata       *)0xfe33)
#define     I2CCFG      (*(unsigned char        volatile    xdata       *)0xfe80)
#define     I2CMSCR     (*(unsigned char        volatile    xdata       *)0xfe81)
#define     I2CMSST     (*(unsigned char        volatile    xdata       *)0xfe82)
#define     I2CSLCR     (*(unsigned char        volatile    xdata       *)0xfe83)
#define     I2CSLST     (*(unsigned char        volatile    xdata       *)0xfe84)
#define     I2CSLADR    (*(unsigned char        volatile    xdata       *)0xfe85)
#define     I2CTXD      (*(unsigned char        volatile    xdata       *)0xfe86)
#define     I2CRXD      (*(unsigned char        volatile    xdata       *)0xfe87)
#define     I2CMSAUX    (*(unsigned char        volatile    xdata       *)0xfe88)
#define     TM2PS       (*(unsigned char        volatile    xdata       *)0xfea2)
#define     TM3PS       (*(unsigned char        volatile    xdata       *)0xfea3)
#define     TM4PS       (*(unsigned char        volatile    xdata       *)0xfea4)
#define     ADCTIM      (*(unsigned char        volatile    xdata       *)0xfea8)

#define     PWMA_ETRPS  (*(unsigned char volatile xdata *)0xfeb0)
#define     PWMA_ENO    (*(unsigned char volatile xdata *)0xfeb1)
#define     PWMA_PS     (*(unsigned char volatile xdata *)0xfeb2)
#define     PWMA_IOAUX  (*(unsigned char volatile xdata *)0xfeb3)
#define     PWMB_ETRPS  (*(unsigned char volatile xdata *)0xfeb4)
#define     PWMB_ENO    (*(unsigned char volatile xdata *)0xfeb5)
#define     PWMB_PS     (*(unsigned char volatile xdata *)0xfeb6)
#define     PWMB_IOAUX  (*(unsigned char volatile xdata *)0xfeb7)
#define     PWMA_CR1    (*(unsigned char volatile xdata *)0xfec0)
#define     PWMA_CR2    (*(unsigned char volatile xdata *)0xfec1)
#define     PWMA_SMCR   (*(unsigned char volatile xdata *)0xfec2)
#define     PWMA_ETR    (*(unsigned char volatile xdata *)0xfec3)
#define     PWMA_IER    (*(unsigned char volatile xdata *)0xfec4)
#define     PWMA_SR1    (*(unsigned char volatile xdata *)0xfec5)
#define     PWMA_SR2    (*(unsigned char volatile xdata *)0xfec6)
#define     PWMA_EGR    (*(unsigned char volatile xdata *)0xfec7)
#define     PWMA_CCMR1  (*(unsigned char volatile xdata *)0xfec8)
#define     PWMA_CCMR2  (*(unsigned char volatile xdata *)0xfec9)
#define     PWMA_CCMR3  (*(unsigned char volatile xdata *)0xfeca)
#define     PWMA_CCMR4  (*(unsigned char volatile xdata *)0xfecb)
#define     PWMA_CCER1  (*(unsigned char volatile xdata *)0xfecc)
#define     PWMA_CCER2  (*(unsigned char volatile xdata *)0xfecd)
#define     PWMA_CNTR   (*(unsigned int volatile xdata *)0xfece)
#define     PWMA_CNTRH  (*(unsigned char volatile xdata *)0xfece)
#define     PWMA_CNTRL  (*(unsigned char volatile xdata *)0xfecf)
#define     PWMA_PSCR   (*(unsigned int volatile xdata *)0xfed0)
#define     PWMA_PSCRH  (*(unsigned char volatile xdata *)0xfed0)
#define     PWMA_PSCRL  (*(unsigned char volatile xdata *)0xfed1)
#define     PWMA_ARR    (*(unsigned int volatile xdata *)0xfed2)
#define     PWMA_ARRH   (*(unsigned char volatile xdata *)0xfed2)
#define     PWMA_ARRL   (*(unsigned char volatile xdata *)0xfed3)
#define     PWMA_RCR    (*(unsigned char volatile xdata *)0xfed4)
#define     PWMA_CCR1   (*(unsigned int volatile xdata *)0xfed5)
#define     PWMA_CCR1H  (*(unsigned char volatile xdata *)0xfed5)
#define     PWMA_CCR1L  (*(unsigned char volatile xdata *)0xfed6)
#define     PWMA_CCR2   (*(unsigned int volatile xdata *)0xfed7)
#define     PWMA_CCR2H  (*(unsigned char volatile xdata *)0xfed7)
#define     PWMA_CCR2L  (*(unsigned char volatile xdata *)0xfed8)
#define     PWMA_CCR3   (*(unsigned int volatile xdata *)0xfed9)
#define     PWMA_CCR3H  (*(unsigned char volatile xdata *)0xfed9)
#define     PWMA_CCR3L  (*(unsigned char volatile xdata *)0xfeda)
#define     PWMA_CCR4   (*(unsigned int volatile xdata *)0xfedb)
#define     PWMA_CCR4H  (*(unsigned char volatile xdata *)0xfedb)
#define     PWMA_CCR4L  (*(unsigned char volatile xdata *)0xfedc)
#define     PWMA_BKR    (*(unsigned char volatile xdata *)0xfedd)
#define     PWMA_DTR    (*(unsigned char volatile xdata *)0xfede)
#define     PWMA_OISR   (*(unsigned char volatile xdata *)0xfedf)
#define     PWMB_CR1    (*(unsigned char volatile xdata *)0xfee0)
#define     PWMB_CR2    (*(unsigned char volatile xdata *)0xfee1)
#define     PWMB_SMCR   (*(unsigned char volatile xdata *)0xfee2)
#define     PWMB_ETR    (*(unsigned char volatile xdata *)0xfee3)
#define     PWMB_IER    (*(unsigned char volatile xdata *)0xfee4)
#define     PWMB_SR1    (*(unsigned char volatile xdata *)0xfee5)
#define     PWMB_SR2    (*(unsigned char volatile xdata *)0xfee6)
#define     PWMB_EGR    (*(unsigned char volatile xdata *)0xfee7)
#define     PWMB_CCMR1  (*(unsigned char volatile xdata *)0xfee8)
#define     PWMB_CCMR2  (*(unsigned char volatile xdata *)0xfee9)
#define     PWMB_CCMR3  (*(unsigned char volatile xdata *)0xfeea)
#define     PWMB_CCMR4  (*(unsigned char volatile xdata *)0xfeeb)
#define     PWMB_CCER1  (*(unsigned char volatile xdata *)0xfeec)
#define     PWMB_CCER2  (*(unsigned char volatile xdata *)0xfeed)
#define     PWMB_CNTR   (*(unsigned int volatile xdata *)0xfeee)
#define     PWMB_CNTRH  (*(unsigned char volatile xdata *)0xfeee)
#define     PWMB_CNTRL  (*(unsigned char volatile xdata *)0xfeef)
#define     PWMB_PSCR   (*(unsigned int volatile xdata *)0xfef0)
#define     PWMB_PSCRH  (*(unsigned char volatile xdata *)0xfef0)
#define     PWMB_PSCRL  (*(unsigned char volatile xdata *)0xfef1)
#define     PWMB_ARR    (*(unsigned int volatile xdata *)0xfef2)
#define     PWMB_ARRH   (*(unsigned char volatile xdata *)0xfef2)
#define     PWMB_ARRL   (*(unsigned char volatile xdata *)0xfef3)
#define     PWMB_RCR    (*(unsigned char volatile xdata *)0xfef4)
#define     PWMB_CCR1   (*(unsigned int volatile xdata *)0xfef5)
#define     PWMB_CCR1H  (*(unsigned char volatile xdata *)0xfef5)
#define     PWMB_CCR1L  (*(unsigned char volatile xdata *)0xfef6)
#define     PWMB_CCR2   (*(unsigned int volatile xdata *)0xfef7)
#define     PWMB_CCR2H  (*(unsigned char volatile xdata *)0xfef7)
#define     PWMB_CCR2L  (*(unsigned char volatile xdata *)0xfef8)
#define     PWMB_CCR3   (*(unsigned int volatile xdata *)0xfef9)
#define     PWMB_CCR3H  (*(unsigned char volatile xdata *)0xfef9)
#define     PWMB_CCR3L  (*(unsigned char volatile xdata *)0xfefa)
#define     PWMB_CCR4   (*(unsigned int volatile xdata *)0xfefb)
#define     PWMB_CCR4H  (*(unsigned char volatile xdata *)0xfefb)
#define     PWMB_CCR4L  (*(unsigned char volatile xdata *)0xfefc)
#define     PWMB_BKR    (*(unsigned char volatile xdata *)0xfefd)
#define     PWMB_DTR    (*(unsigned char volatile xdata *)0xfefe)
#define     PWMB_OISR   (*(unsigned char volatile xdata *)0xfeff)

typedef unsigned char   uint8;	//  8 bits 
typedef unsigned int  	uint16;	// 16 bits 
typedef unsigned long   uint32;	// 32 bits 


typedef signed char     int8;	//  8 bits 
typedef signed int      int16;	// 16 bits 

typedef signed long     int32;	// 32 bits 


typedef volatile int8   vint8;	//  8 bits 
typedef volatile int16  vint16;	// 16 bits 
typedef volatile int32  vint32;	// 32 bits 


typedef volatile uint8  vuint8;	//  8 bits 
typedef volatile uint16 vuint16;	// 16 bits 
typedef volatile uint32 vuint32;	// 32 bits 
typedef unsigned char BYTE;
typedef unsigned int WORD;
typedef unsigned long DWORD;
typedef unsigned char uint8_t;
typedef unsigned long UINT32;

typedef struct
{
    BYTE bmRequestType;
    BYTE bRequest;
    BYTE wValueL;
    BYTE wValueH;
    BYTE wIndexL;
    BYTE wIndexH;
    BYTE wLengthL;
    BYTE wLengthH;
} SETUP;
typedef struct
{
    BYTE bStage;
    WORD wResidue;
    BYTE* pData;
} EP0STAGE;
#define IRC48MCR (*(unsigned char volatile xdata *)0xfe07)
#define FADDR 0
#define POWER 1
#define INTRIN1 2
#define EP5INIF 0x20
#define EP4INIF 0x10
#define EP3INIF 0x08
#define EP2INIF 0x04
#define EP1INIF 0x02
#define EP0IF 0x01
#define INTROUT1 4
#define EP5OUTIF 0x20
#define EP4OUTIF 0x10
#define EP3OUTIF 0x08
#define EP2OUTIF 0x04
#define EP1OUTIF 0x02
#define INTRUSB 6
#define SOFIF 0x08
#define RSTIF 0x04
#define RSUIF 0x02
#define SUSIF 0x01
#define INTRIN1E 7
#define EP5INIE 0x20
#define EP4INIE 0x10
#define EP3INIE 0x08
#define EP2INIE 0x04
#define EP1INIE 0x02
#define EP0IE 0x01
#define INTROUT1E 9
#define EP5OUTIE 0x20
#define EP4OUTIE 0x10
#define EP3OUTIE 0x08
#define EP2OUTIE 0x04
#define EP1OUTIE 0x02
#define INTRUSBE 11
#define SOFIE 0x08
#define RSTIE 0x04
#define RSUIE 0x02
#define SUSIE 0x01
#define FRAME1 12
#define FRAME2 13
#define INDEX 14
#define INMAXP 16
#define CSR0 17
#define SSUEND 0x80
#define SOPRDY 0x40
#define SDSTL 0x20
#define SUEND 0x10
#define DATEND 0x08
#define STSTL 0x04
#define IPRDY 0x02
#define OPRDY 0x01
#define INCSR1 17
#define INCLRDT 0x40
#define INSTSTL 0x20
#define INSDSTL 0x10
#define INFLUSH 0x08
#define INUNDRUN 0x04
#define INFIFONE 0x02
#define INIPRDY 0x01
#define INCSR2 18
#define INAUTOSET 0x80
#define INISO 0x40
#define INMODEIN 0x20
#define INMODEOUT 0x00
#define INENDMA 0x10
#define INFCDT 0x08
#define OUTMAXP 19
#define OUTCSR1 20
#define OUTCLRDT 0x80
#define OUTSTSTL 0x40
#define OUTSDSTL 0x20
#define OUTFLUSH 0x10
#define OUTDATERR 0x08
#define OUTOVRRUN 0x04
#define OUTFIFOFUL 0x02
#define OUTOPRDY 0x01
#define OUTCSR2 21
#define OUTAUTOCLR 0x80
#define OUTISO 0x40
#define OUTENDMA 0x20
#define OUTDMAMD 0x10
#define COUNT0 22
#define OUTCOUNT1 22
#define OUTCOUNT2 23
#define FIFO0 32
#define FIFO1 33
#define FIFO2 34
#define FIFO3 35
#define FIFO4 36
#define FIFO5 37
#define UTRKCTL 48
#define UTRKSTS 49
#define EPIDLE 0
#define EPSTATUS 1
#define EPDATAIN 2
#define EPDATAOUT 3
#define EPSTALL -1
#define GET_STATUS 0x00
#define CLEAR_FEATURE 0x01
#define SET_FEATURE 0x03
#define SET_ADDRESS 0x05
#define GET_DESCRIPTOR 0x06
#define SET_DESCRIPTOR 0x07
#define GET_CONFIG 0x08
#define SET_CONFIG 0x09
#define GET_INTERFACE 0x0A
#define SET_INTERFACE 0x0B
#define SYNCH_FRAME 0x0C
#define GET_REPORT 0x01
#define GET_IDLE 0x02
#define GET_PROTOCOL 0x03
#define SET_REPORT 0x09
#define SET_IDLE 0x0A
#define SET_PROTOCOL 0x0B
#define DESC_DEVICE 0x01
#define DESC_CONFIG 0x02
#define DESC_STRING 0x03
#define DESC_HIDREPORT 0x22
#define STANDARD_REQUEST 0x00
#define CLASS_REQUEST 0x20
#define VENDOR_REQUEST 0x40
#define REQUEST_MASK 0x60

#define USB_REQ_RECIP_MASK      0x1F            /* bit mask of request recipient */
#define USB_REQ_RECIP_DEVICE    0x00
#define USB_REQ_RECIP_INTERF    0x01
#define USB_REQ_RECIP_ENDP      0x02



#endif /* _STC8_H_ */

