#ifndef __ULTRASONIC_H
#define __ULTRASONIC_H

#include <STC8HX.h>
#include "delay.h"

//========================================================================
// 描述: 获取超声波检测到距离值.
// 参数: none.
// 返回: 距离值(0~400).
//========================================================================
uint16 ultrasonic_get_distance()
{
    uint16 dis = 400;
    P5M1&=~0x01;P5M0|=0x01;   //P50推挽输出
    P5M1|=0x02;P5M0&=~0x02;   //P51高阻输入

    TMOD |= 0x01;	//定时器0不自动重载
    TL0 = 0x0;		//设定定时初值
    TH0 = 0x0;		//设定定时初值
    // AUXR &= ~0x40;
    
    P5_0 = 0;
    delay5us();
    P5_0 = 1;          //发射脉冲
    delay10us();
    P5_0 = 0;

    while(!P5_1);
    TR0 = 1;// 启动定时器
    while(P5_1&(TH0<0xB8)); 
    TR0 = 0;//关闭定时器
    dis = (TH0 << 8) | TL0;
    if(dis > 47059){
        dis = 400;
    }else{
        dis = (dis*0.52)/58;       
    }
    return dis;
}

#endif