/*************  技术支持与购买说明    **************
产品主页：http://tw51.haohaodada.com
淘宝搜索：天问51，可购买。目前基础版99元，带彩屏标准备版149元，旗舰版299
技术支持QQ群一：1138055784
******************************************/

#ifndef __TWEN_BOARD_H
#define __TWEN_BOARD_H  
#include <STC16F.h>
#include "lib/hc595.h"
#include "lib/rgb.h"
#include "lib/sysclock.h"

void twen_board_init()
{
  uint16 i=0;
  WTST=0;
	P_SW2 |= 0x80; //默认全部打开扩展SFR（实际使用时可以根据需要打开或者禁止扩展SFR）
  // sysclock_set_xosc(0);//设置系统时钟为外部晶振分频系数0，即24.000MHz
  i = 0;
	XOSCCR = 0xc0; //启动外部晶振
	while (!(XOSCCR & 1)) //等待时钟稳定
  {
      i++;
      delay1us();
      if(i >= 1000){
        break;
      }
  }
  if(i < 1000){
    CLKDIV = 0; //时钟分频
	  CLKSEL = 0x01; //选择外部晶振
  }else{
    XOSCCR = 0x00; //关闭外部晶振
    sysclock_set_hir_irc(0);  //系统内部高速时钟
  }
  P0M1=0x00;P0M0=0x00;//双向IO口
  P1M1=0x00;P1M0=0x00;//双向IO口
  P2M1=0x00;P2M0=0x00;//双向IO口
  P3M1=0x00;P3M0=0x00;//双向IO口
  P4M1=0x00;P4M0=0x00;//双向IO口
  P5M1=0x00;P5M0=0x00;//双向IO口
  P6M1=0x00;P6M0=0x00;//双向IO口
  P7M1=0x00;P7M0=0x00;//双向IO口
  hc595_init();//HC595初始化
  hc595_disable();//HC595禁止点阵和数码管输出
  rgb_init();//RGB初始化
  delay(10);
  for (i=0;i<6;i++) {
    rgb_show(i,0,0,0);//关闭RGB
  }
  delay(10);
}

#endif    //twen_board.h
