/*************  技术支持与购买说明    **************
产品主页：http://tw51.haohaodada.com
淘宝搜索：天问51，可购买。目前基础版99元，带彩屏标准备版149元，旗舰版299
技术支持QQ群一：1138055784
******************************************/

#ifndef _ADC_H_
#define _ADC_H_

uint8  setbit = 0;
typedef enum
{
	ADC_P10 = 0,
	ADC_P11,
	ADC_P12, //����ռλ
	ADC_P13,
	ADC_P14,
	ADC_P15,
	ADC_P16,
	ADC_P17,

	ADC_P00,
	ADC_P01,
	ADC_P02,
	ADC_P03,
	ADC_P04,
	ADC_P05,
	ADC_P06,
	ADC_REF = 0x0f, //�ڲ�AD 1.19V

	ADC_P30,
	ADC_P31,
	ADC_P32,
	ADC_P33,
	ADC_P34,
	ADC_P35,
	ADC_P36,
	ADC_P37,

} ADC_Name;

//��ö�ٶ��岻�����û��޸�
typedef enum
{
	ADC_SYSclk_DIV_2 = 0,
	ADC_SYSclk_DIV_4,
	ADC_SYSclk_DIV_6,
	ADC_SYSclk_DIV_8,
	ADC_SYSclk_DIV_10,
	ADC_SYSclk_DIV_12,
	ADC_SYSclk_DIV_14,
	ADC_SYSclk_DIV_16,
	ADC_SYSclk_DIV_18,
	ADC_SYSclk_DIV_20,
	ADC_SYSclk_DIV_22,
	ADC_SYSclk_DIV_24,
	ADC_SYSclk_DIV_26,
	ADC_SYSclk_DIV_28,
	ADC_SYSclk_DIV_30,
	ADC_SYSclk_DIV_32,
} ADC_CLK;


//��ö�ٶ��岻�����û��޸�
typedef enum    // ö��ADCͨ��
{

	ADC_12BIT = 0,    //12λ�ֱ���
	ADC_11BIT,		//11λ�ֱ���
	ADC_10BIT,		//10λ�ֱ���
	ADC_9BIT,    	//9λ�ֱ���
	ADC_8BIT,     	//8λ�ֱ���

}ADC_bit;

//-------------------------------------------------------------------------------------------------------------------
//  @brief      ADC��ʼ��
//  @param      adcn            ѡ��ADCͨ��
//  @param      speed      		ADCʱ��Ƶ��
//  @return     void
//  Sample usage:               adc_init(ADC_P10,ADC_SYSclk_DIV_2);//��ʼ��P1.0ΪADC����,ADCʱ��Ƶ�ʣ�SYSclk/2
//-------------------------------------------------------------------------------------------------------------------
void adc_init(ADC_Name adcn, ADC_CLK speed, ADC_bit _sbit)
{
	setbit = _sbit;
	ADC_CONTR |= 1 << 7;	//1 ���� ADC ��Դ
	if (adcn > 15) 
	{
		adcn = adcn - 16;
		//IO����Ҫ����Ϊ��������
		P3M0 &= ~(1 << (adcn & 0x07));
		P3M1 |= (1 << (adcn & 0x07));


	}
	else {
		if ((adcn >> 3) == 1) //P0.0
		{
			//IO����Ҫ����Ϊ��������
			P0M0 &= ~(1 << (adcn & 0x07));
			P0M1 |= (1 << (adcn & 0x07));
		}
		else if ((adcn >> 3) == 0) //P1.0	
		{
			//IO����Ҫ����Ϊ��������
			P1M0 &= ~(1 << (adcn & 0x07));
			P1M1 |= (1 << (adcn & 0x07));
		}
	}

	ADCCFG |= speed & 0x0F;	//ADCʱ��Ƶ��SYSclk/2/speed&0x0F;

	ADCCFG |= 1 << 5;		//ת������Ҷ��롣 ADC_RES �������ĸ� 2 λ�� ADC_RESL �������ĵ� 8 λ��

}



//-------------------------------------------------------------------------------------------------------------------
//  @brief      ADCת��һ��
//  @param      adcn            ѡ��ADCͨ��
//  @param      resolution      �ֱ���
//  @return     void
//  Sample usage:               adc_convert(ADC_P10, ADC_10BIT);
//-------------------------------------------------------------------------------------------------------------------
uint16 adc_read(ADC_Name adcn)
{
	uint16 adc_value;
	if (adcn > 15)adcn = adcn - 8;

	ADC_CONTR &= (0xF0);	//���ADC_CHS[3:0] �� ADC ģ��ͨ��ѡ��λ
	ADC_CONTR |= adcn;

	ADC_CONTR |= 0x40;  // ���� AD ת��
	while (!(ADC_CONTR & 0x20));  // ��ѯ ADC ��ɱ�־
	ADC_CONTR &= ~0x20;  // ����ɱ�־


	adc_value = ADC_RES;  //�洢 ADC �� 10 λ����ĸ� 2 λ
	adc_value <<= 8;
	adc_value |= ADC_RESL;  //�洢 ADC �� 10 λ����ĵ� 8 λ

	ADC_RES = 0;
	ADC_RESL = 0;

	adc_value >>= setbit;//ȡ����λ


	return adc_value;
}

#endif