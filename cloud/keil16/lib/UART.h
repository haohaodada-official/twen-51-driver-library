/*************  技术支持与购买说明    **************
产品主页：http://tw51.haohaodada.com
淘宝搜索：天问51，可购买。目前基础版99元，带彩屏标准备版149元，旗舰版299
技术支持QQ群一：1138055784
******************************************/

#ifndef _UART_H_
#define _UART_H_

#include <stc16f.h>

#define	UART1_CLEAR_RX_FLAG (SCON  &= ~0x01)
#define	UART2_CLEAR_RX_FLAG (S2CON &= ~0x01)
#define	UART3_CLEAR_RX_FLAG (S3CON &= ~0x01)
#define	UART4_CLEAR_RX_FLAG (S4CON &= ~0x01)

#define	UART1_CLEAR_TX_FLAG (SCON  &= ~0x02)
#define	UART2_CLEAR_TX_FLAG (S2CON &= ~0x02)
#define	UART3_CLEAR_TX_FLAG (S3CON &= ~0x02)
#define	UART4_CLEAR_TX_FLAG (S4CON &= ~0x02)

#define UART1_GET_RX_FLAG   (SCON  & 0x01)
#define UART2_GET_RX_FLAG   (S2CON & 0x01)
#define UART3_GET_RX_FLAG   (S3CON & 0x01)
#define UART4_GET_RX_FLAG   (S4CON & 0x01)

#define UART1_GET_TX_FLAG   (SCON  & 0x02)
#define UART2_GET_TX_FLAG   (S2CON & 0x02)
#define UART3_GET_TX_FLAG   (S3CON & 0x02)
#define UART4_GET_TX_FLAG   (S4CON & 0x02)

//��ö�ٶ��岻�����û��޸�
typedef enum    // ö��ADCͨ��
{
	TIM_0,
	TIM_1,
	TIM_2,
	TIM_3,
	TIM_4,
}UART_TIMN;
//��ö�ٶ��岻�����û��޸�
typedef enum //ö�ٴ��ں�
{
	UART_1,
	UART_2,
	UART_3,
	UART_4,
}UART_Name;


//��ö�ٶ��岻�����û��޸�
typedef enum //ö�ٴ�������
{
	UART1_RX_P30, UART1_TX_P31,		//ֻ��ʹ��ͬһ�е�RX��TX���źš�����������
	UART1_RX_P36, UART1_TX_P37,		//����:UART1_RX_P30,UART1_TX_P37���������С�
	UART1_RX_P16, UART1_TX_P17,
	UART1_RX_P43, UART1_TX_P44,

	UART2_RX_P10, UART2_TX_P11,
	UART2_RX_P46, UART2_TX_P47,

	UART3_RX_P00, UART3_TX_P01,
	UART3_RX_P50, UART3_TX_P51,

	UART4_RX_P02, UART4_TX_P03,
	UART4_RX_P52, UART4_TX_P53,
}UART_PIN;
uint8 busy[5];

//-------------------------------------------------------------------------------------------------------------------
//  @brief      ���ڳ�ʼ��
//  @param      uart_n          ����ģ���(USART_1,USART_2,USART_3,USART_4)
//  @param      uart_rx_pin     ���ڲ�����
//  @param      uart_tx_pin     ���ڽ��շ�������
//  @param      baud      		���ڽ��շ�������
//  @param      tim_n      		ʹ��tim_n��Ϊ���ڲ����ʷ�����(TIM1-TIM4)
//  @return     NULL          	
//  Sample usage:               uart_init(USART_1,115200,UART1_RX_P30_TX_P31);       // ��ʼ������1 ������115200 ��������ʹ��P31 ��������ʹ��P30
//  @note                       ����1ʹ�� ��ʱ��1���߶�ʱ��2 ��Ϊ�����ʷ�������
//								����2ʹ�� ��ʱ��2 			 ��Ϊ�����ʷ�������
//								����3ʹ�� ��ʱ��3���߶�ʱ��2 ��Ϊ�����ʷ�������
//								����4ʹ�� ��ʱ��4���߶�ʱ��2 ��Ϊ�����ʷ�������
//                              STC8H���� ��ʱ��0-��ʱ��4����5����ʱ����
//								�������ɼ�����Ҳ��Ҫ��ʱ����Ϊ�ⲿ������
//-------------------------------------------------------------------------------------------------------------------
void uart_init(UART_Name uart_n, UART_PIN uart_rx_pin, UART_PIN uart_tx_pin, uint32 baud, UART_TIMN tim_n)
{
	uint16 brt;

	brt = (uint16)(65536 - sys_clk / 4 / baud);

	//brt=64911;

	switch (uart_n)
	{
		case UART_1:
		{
			if (TIM_1 == tim_n)
			{
				SCON |= 0x50;
				TMOD |= 0x00;
				//TMOD=0x00;
				TL1 = brt;
				TH1 = brt >> 8;
				AUXR &= ~0x01;//��ʱ��1�л�Ϊ����1
				AUXR |= 0x40;
				TR1 = 1;
				busy[1] = 0;
			}
			else if (TIM_2 == tim_n)
			{
				SCON |= 0x50;
				T2L = brt;
				T2H = brt >> 8;
				AUXR |= 0x15;
			}
			P_SW1 &= ~(0x03 << 6);
			if ((UART1_RX_P30 == uart_rx_pin) && (UART1_TX_P31 == uart_tx_pin))
			{
				P3M1&=~0x01;P3M0&=~0x01;//P30˫��IO��
  				P3M1&=~0x02;P3M0&=~0x02;//P31˫��IO��
				P_SW1 |= 0x00;
			}
			else if ((UART1_RX_P36 == uart_rx_pin) && (UART1_TX_P37 == uart_tx_pin))
			{
				P3M1&=~0x40;P3M0&=~0x40;//P36˫��IO��
  				P3M1&=~0x80;P3M0&=~0x80;//P37˫��IO��
				P_SW1 |= 0x40;
			}
			else if ((UART1_RX_P16 == uart_rx_pin) && (UART1_TX_P17 == uart_tx_pin))
			{
				P1M1&=~0x40;P1M0&=~0x40;//P16˫��IO��
  				P1M1&=~0x80;P1M0&=~0x80;//P17˫��IO��
				P_SW1 |= 0x80;
			}
			else if ((UART1_RX_P43 == uart_rx_pin) && (UART1_TX_P44 == uart_tx_pin))
			{
				P4M1&=~0x08;P4M0&=~0x08;//P43˫��IO��
  				P4M1&=~0x10;P4M0&=~0x10;//P44˫��IO��
				P_SW1 |= 0xc0;
			}
			busy[1] = 0;
			// ES = 1;
			break;
		}

		case UART_2:
		{
			if (TIM_2 == tim_n)
			{
				S2CON |= 0x10;
				T2L = brt;
				T2H = brt >> 8;
				AUXR |= 0x14;
			}

			P_SW2 &= ~(0x01 << 0);
			if ((UART2_RX_P10 == uart_rx_pin) && (UART2_TX_P11 == uart_tx_pin))
			{
				P1M1&=~0x01;P1M0&=~0x01;//˫��IO��
  				P1M1&=~0x02;P1M0&=~0x02;//˫��IO��
				P_SW2 |= 0x00;
			}
			else if ((UART2_RX_P46 == uart_rx_pin) && (UART2_TX_P47 == uart_tx_pin))
			{
				P4M1&=~0x40;P4M0&=~0x40;//˫��IO��
  				P4M1&=~0x80;P4M0&=~0x80;//˫��IO��
				P_SW2 |= 0x01;
			}

			// IE2 |= 0x01 << 0;	//�������п�2�ж�
			busy[2] = 0;
			break;
		}

		case UART_3:
		{

			if (TIM_2 == tim_n)
			{
				S2CON |= 0x10;
				T2L = brt;
				T2H = brt >> 8;
				AUXR |= 0x14;
			}
			else if (TIM_3 == tim_n)
			{
				S3CON |= 0x50;
				T3L = brt;
				T3H = brt >> 8;
				T4T3M |= 0x0a;
			}

			P_SW2 &= ~(0x01 << 1);
			if ((UART3_RX_P00 == uart_rx_pin) && (UART3_TX_P01 == uart_tx_pin))
			{
				P0M1&=~0x01;P0M0&=~0x01;//P00˫��IO��
  				P0M1&=~0x02;P0M0&=~0x02;//P01˫��IO��
				P_SW2 |= 0x00;
			}
			else if ((UART3_RX_P50 == uart_rx_pin) && (UART3_TX_P51 == uart_tx_pin))
			{
				P5M1&=~0x01;P5M0&=~0x01;//P50˫��IO��
  				P5M1&=~0x02;P5M0&=~0x02;//P51˫��IO��
				P_SW2 |= 0x02;
			}

			// IE2 |= 0x01 << 3;	//�������п�3�ж�
			busy[3] = 0;
			break;
		}

		case UART_4:
		{
			if (TIM_2 == tim_n)
			{
				S4CON |= 0x10;
				T2L = brt;
				T2H = brt >> 8;
				AUXR |= 0x14;
			}
			else if (TIM_4 == tim_n)
			{
				S4CON |= 0x50;
				T4H = brt >> 8;
				T4L = brt;
				T4T3M |= 0xa0;
			}

			P_SW2 &= ~(0x01 << 2);
			if ((UART4_RX_P02 == uart_rx_pin) && (UART4_TX_P03 == uart_tx_pin))
			{
				P0M1&=~0x04;P0M0&=~0x04;//P02˫��IO��
  				P0M1&=~0x08;P0M0&=~0x08;//P03˫��IO��
				P_SW2 |= 0x00;
			}
			else if ((UART4_RX_P52 == uart_rx_pin) && (UART4_TX_P53 == uart_tx_pin))
			{
				P5M1&=~0x04;P5M0&=~0x04;//P52˫��IO��
				P5M1&=~0x08;P5M0&=~0x08;//P53˫��IO��
				P_SW2 |= 0x04;
			}

			// IE2 |= 0x01 << 4;	//�������п�4�ж�

			busy[4] = 0;
			break;
		}
	}
	// EA = 1;
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      �����ֽ����
//  @param      uart_n          ����ģ���(USART_1,USART_2,USART_3,USART_4)
//  @param      dat             ��Ҫ���͵��ֽ�
//  @return     void        
//  Sample usage:               uart_putchar(UART_1,0xA5);       // ����1����0xA5
//-------------------------------------------------------------------------------------------------------------------
void uart_putchar(UART_Name uart_n, uint8 dat)
{
	switch (uart_n)
	{
		case UART_1:
			/*while (busy[1]);
			busy[1] = 1;*/
			SBUF = dat;
			while (UART1_GET_TX_FLAG == 0);
			UART1_CLEAR_TX_FLAG;
			break;
		case UART_2:
			/*while (busy[2]);
			busy[2] = 1;*/
			S2BUF = dat;
			while (UART2_GET_TX_FLAG == 0);
			UART2_CLEAR_TX_FLAG;
			break;
		case UART_3:
			/*while (busy[3]);
			busy[3] = 1;*/
			S3BUF = dat;
			while (UART3_GET_TX_FLAG == 0);
			UART3_CLEAR_TX_FLAG;
			break;
		case UART_4:
			/*while (busy[4]);
			busy[4] = 1;*/
			S4BUF = dat;
			while (UART4_GET_TX_FLAG == 0);
			UART4_CLEAR_TX_FLAG;
			break;
	}
}


//-------------------------------------------------------------------------------------------------------------------
//  @brief      ���ڷ�������
//  @param      uart_n          ����ģ���(USART_1,USART_2,USART_3,USART_4)
//  @param      *buff           Ҫ���͵������ַ
//  @param      len             ���ͳ���
//  @return     void
//  Sample usage:               uart_putbuff(UART_1,&a[0],5);
//-------------------------------------------------------------------------------------------------------------------
void uart_putbuff(UART_Name uart_n, uint8* p, uint16 len)
{
	while (len--)
		uart_putchar(uart_n, *p++);
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      ���ڷ����ַ���
//  @param      uart_n          ����ģ���(USART_1,USART_2,USART_3,USART_4)
//  @param      *str            Ҫ���͵��ַ�����ַ
//  @return     void
//  Sample usage:               uart_putstr(UART_1,"i lvoe you"); 
//-------------------------------------------------------------------------------------------------------------------
void uart_putstr(UART_Name uart_n, uint8* str)
{
	while (*str)
	{
		uart_putchar(uart_n, *str++);
	}
}

char putchar(char c)
{
  if (c == '\n')
  {
    uart_putchar(UART_1, 0x0d);
    uart_putchar(UART_1, 0x0a);
  } else {
    uart_putchar(UART_1, (uint8)c);
  }
	return c;
}

#endif /* _STC12_H_ */

