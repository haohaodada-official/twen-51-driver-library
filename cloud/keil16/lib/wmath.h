/*************  技术支持与购买说明    **************
产品主页：http://tw51.haohaodada.com
淘宝搜索：天问51，可购买。目前基础版99元，带彩屏标准备版149元，旗舰版299
技术支持QQ群一：1138055784
******************************************/

#ifndef _WIRING_MATH_
#define _WIRING_MATH_
#include <stc16f.h>
#include "stdlib.h"

#define round(x)     ((x)>=0?(long)((x)+0.5):(long)((x)-0.5))

void randomSeed( uint32 dwSeed )
{
  if ( dwSeed != 0 )
  {
    srand( dwSeed ) ;
  }
}

long random( long howsmall, long howbig )
{
  long diff;
  if (howsmall >= howbig)
  {
    return howsmall;
  }

  diff = howbig - howsmall;

  if ( diff == 0 )
  {
    return 0 ;
  }

  return rand() % diff + howsmall;
}

long map(long x, long in_min, long in_max, long out_min, long out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

#endif