/*************  技术支持与购买说明    **************
产品主页：http://tw51.haohaodada.com
淘宝搜索：天问51，可购买。目前基础版99元，带彩屏标准备版149元，旗舰版299
技术支持QQ群一：1138055784
******************************************/

#ifndef _GPIO_H_
#define _GPIO_H_

typedef enum
{
	NOPULL = 0,
	PULL4k = 1,
}PULLUP;


typedef enum
{
	P00 = 0x00, P01, P02, P03, P04, P05, P06, P07,
	P10 = 0x10, P11, P12, P13, P14, P15, P16, P17,
	P20 = 0x20, P21, P22, P23, P24, P25, P26, P27,
	P30 = 0x30, P31, P32, P33, P34, P35, P36, P37,
	P40 = 0x40, P41, P42, P43, P44, P45, P46, P47,
	P50 = 0x50, P51, P52, P53, P54, P55, P56, P57,
	P60 = 0x60, P61, P62, P63, P64, P65, P66, P67,
	P70 = 0x70, P71, P72, P73, P74, P75, P76, P77,

}PIN_name;

typedef enum
{

	GPIO = 0,			//׼˫���(������)
	GPIO_PP = 1,			//�������
	INPUT = 2,	//��������
	GPIO_OD = 3,			//��©���
}GPIOMODE;

#define PxPU_BASE_ADDR  0x7EFE10 

//-------------------------------------------------------------------------------------------------------------------
//  @brief      GPIO��������������������
//  @param      pin         ѡ�����ţ�P00-P77��
//  @param      pull        �������������� NOPULL:������ PULL4k:����
//  @return     void
//  Sample usage:           gpio_pull_set(P00,NOPULL);       // ����P0.0����û������������
//-------------------------------------------------------------------------------------------------------------------
void pullMode(PIN_name pin, PULLUP pull)
{
	if (PULL4k == pull)
	{
		(*(unsigned char volatile far*)(PxPU_BASE_ADDR + (pin >> 4))) |= (1 << (pin & 0x0F));
	}
	else if (NOPULL == pull)
	{
		(*(unsigned char volatile far*)(PxPU_BASE_ADDR + (pin >> 4))) &= ~(1 << (pin & 0x0F));
	}
}



//-------------------------------------------------------------------------------------------------------------------
//  @brief      GPIO��������ģʽ
//  @param      pin         ѡ�����ţ�P0_0-P5_4��
//  @param      mode        ����ģʽ GPIO:׼˫���, GPIO_PP:�������, INPUT:��������, GPIO_OD:��©���
//  @return     void
//  Sample usage:           pinMode(P00,GPIO);       // ����P0.0����Ϊ˫��IO
//-------------------------------------------------------------------------------------------------------------------
void pinMode(PIN_name pin, GPIOMODE mode)
{
	if (GPIO == mode)
	{
		if (0x00 == (pin & 0xF0))	//P0
		{
			P0M1 &= ~(1 << (pin & 0xF));
			P0M0 &= ~(1 << (pin & 0xF));
		}
		if (0x10 == (pin & 0xF0))	//P1
		{
			P1M1 &= ~(1 << (pin & 0xF));
			P1M0 &= ~(1 << (pin & 0xF));
		}
		if (0x20 == (pin & 0xF0))	//P2
		{
			P2M1 &= ~(1 << (pin & 0xF));
			P2M0 &= ~(1 << (pin & 0xF));
		}

		if (0x30 == (pin & 0xF0))	//P3
		{
			P3M1 &= ~(1 << (pin & 0xF));
			P3M0 &= ~(1 << (pin & 0xF));
		}
		if (0x40 == (pin & 0xF0))	//P4
		{
			P4M1 &= ~(1 << (pin & 0xF));
			P4M0 &= ~(1 << (pin & 0xF));
		}
		if (0x50 == (pin & 0xF0))	//P5
		{
			P5M1 &= ~(1 << (pin & 0xF));
			P5M0 &= ~(1 << (pin & 0xF));
		}
	}
	else if (GPIO_PP == mode)
	{
		if (0x00 == (pin & 0xF0))	//P0
		{
			P0M1 &= ~(1 << (pin & 0xF));
			P0M0 |= (1 << (pin & 0xF));
		}
		if (0x10 == (pin & 0xF0))	//P1
		{
			P1M1 &= ~(1 << (pin & 0xF));
			P1M0 |= (1 << (pin & 0xF));
		}
		if (0x20 == (pin & 0xF0))	//P2
		{
			P2M1 &= ~(1 << (pin & 0xF));
			P2M0 |= (1 << (pin & 0xF));
		}

		if (0x30 == (pin & 0xF0))	//P3
		{
			P3M1 &= ~(1 << (pin & 0xF));
			P3M0 |= (1 << (pin & 0xF));
		}
		if (0x40 == (pin & 0xF0))	//P4
		{
			P4M1 &= ~(1 << (pin & 0xF));
			P4M0 |= (1 << (pin & 0xF));
		}
		if (0x50 == (pin & 0xF0))	//P5
		{
			P5M1 &= ~(1 << (pin & 0xF));
			P5M0 |= (1 << (pin & 0xF));
		}
	}
	else if (INPUT == mode)
	{
		if (0x00 == (pin & 0xF0))	//P0
		{
			P0M1 |= (1 << (pin & 0xF));
			P0M0 &= ~(1 << (pin & 0xF));
		}
		if (0x10 == (pin & 0xF0))	//P1
		{
			P1M1 |= (1 << (pin & 0xF));
			P1M0 &= ~(1 << (pin & 0xF));
		}
		if (0x20 == (pin & 0xF0))	//P2
		{
			P2M1 |= (1 << (pin & 0xF));
			P2M0 &= ~(1 << (pin & 0xF));
		}

		if (0x30 == (pin & 0xF0))	//P3
		{
			P3M1 |= (1 << (pin & 0xF));
			P3M0 &= ~(1 << (pin & 0xF));
		}
		if (0x40 == (pin & 0xF0))	//P4
		{
			P4M1 |= (1 << (pin & 0xF));
			P4M0 &= ~(1 << (pin & 0xF));
		}
		if (0x50 == (pin & 0xF0))	//P5
		{
			P5M1 |= (1 << (pin & 0xF));
			P5M0 &= ~(1 << (pin & 0xF));
		}
	}
	else if (GPIO_OD == mode)
	{
		if (0x00 == (pin & 0xF0))	//P0
		{
			P0M1 |= (1 << (pin & 0xF));
			P0M0 |= (1 << (pin & 0xF));
		}
		if (0x10 == (pin & 0xF0))	//P1
		{
			P1M1 |= (1 << (pin & 0xF));
			P1M0 |= (1 << (pin & 0xF));
		}
		if (0x20 == (pin & 0xF0))	//P2
		{
			P2M1 |= (1 << (pin & 0xF));
			P2M0 |= (1 << (pin & 0xF));
		}

		if (0x30 == (pin & 0xF0))	//P3
		{
			P3M1 |= (1 << (pin & 0xF));
			P3M0 |= (1 << (pin & 0xF));
		}
		if (0x40 == (pin & 0xF0))	//P4
		{
			P4M1 |= (1 << (pin & 0xF));
			P4M0 |= (1 << (pin & 0xF));
		}
		if (0x50 == (pin & 0xF0))	//P5
		{
			P5M1 |= (1 << (pin & 0xF));
			P5M0 |= (1 << (pin & 0xF));
		}
	}
}


#endif /* _STC12_H_ */

