/*************  技术支持与购买说明    **************
产品主页：http://tw51.haohaodada.com
淘宝搜索：天问51，可购买。目前基础版99元，带彩屏标准备版149元，旗舰版299
技术支持QQ群一：1138055784
******************************************/
#ifndef __STC12_SOFTSPI_H
#define __STC12_SOFTSPI_H
#include <STC12X.h>
#include "STC12_delay.h"

#ifndef    SOFTSPI_SCK_PIN
#define    SOFTSPI_SCK_PIN     P2_5
#endif
#ifndef    SOFTSPI_SCK_MODE 
#define    SOFTSPI_SCK_MODE    {P2M1&=~0x20;P2M0&=~0x20;}  //双向IO口
#endif

#ifndef    SOFTSPI_MISO_PIN
#define    SOFTSPI_MISO_PIN      P2_4
#endif
#ifndef    SOFTSPI_MISO_MODE 
#define    SOFTSPI_MISO_MODE    {P2M1&=~0x10;P2M0&=~0x10;}  //双向IO口
#endif

#ifndef    SOFTSPI_MOSI_PIN
#define    SOFTSPI_MOSI_PIN      P2_3
#endif
#ifndef    SOFTSPI_MOSI_MODE 
#define    SOFTSPI_MOSI_MODE    {P2M1&=~0x08;P2M0&=~0x08;}  //双向IO口
#endif

#ifndef    SOFTSPI_CPHA
#define    SOFTSPI_CPHA    0    //SPI模式选择
#endif

#ifndef    SOFTSPI_CPOL
#define    SOFTSPI_CPOL    0
#endif



void softspi_init();    //初始化
uint8 softspi_wr_data(uint8 data); //SPI写入一个字节后返回一个字节数据
void softspi_write_byte(uint8 out); //SPI写一个字节数据
uint8 softspi_read_byte(void);  //SPI读取一个字节数据

//========================================================================
// 描述: SPI初始化.
// 参数: none.
// 返回: none.
//========================================================================
void softspi_init()
{
    SOFTSPI_SCK_MODE;//P25双向IO口
    SOFTSPI_MISO_MODE;//P24双向IO口
    SOFTSPI_MOSI_MODE;//P23双向IO口
    SOFTSPI_SCK_PIN = 0;        //set clock to low initial state
}

//========================================================================
// 描述: SPI写入一个字节后读取一个字节数据.
// 参数: none.
// 返回: none.
//========================================================================
uint8 softspi_wr_data(uint8 data)
{
    uint8 datavalue=0,i,wr_data;
    wr_data = data;
    #if SOFTSPI_CPHA==0 && SOFTSPI_CPOL==0
    for(i=0;i<8;i++){
        if(wr_data&0x80){
            SOFTSPI_MOSI_PIN = 1;
        }else{
            SOFTSPI_MOSI_PIN = 0;
        }
        wr_data <<=1;
        SOFTSPI_SCK_PIN = 1;
        datavalue <<=1;
        if(SOFTSPI_MISO_PIN == 1){
            datavalue |= 0x01;
        }
         SOFTSPI_SCK_PIN = 0;
        //delay(1);
    }
    
    #elif SOFTSPI_CPHA==0 && SOFTSPI_CPOL==1
    for(i=0;i<8;i++){
        if(wr_data&0x80){
            SOFTSPI_MOSI_PIN = 1;
        }else{
            SOFTSPI_MOSI_PIN = 0;
        }
        wr_data <<=1;
        SOFTSPI_SCK_PIN = 0;
        datavalue <<=1;
        if(SOFTSPI_MISO_PIN == 1){
            datavalue |= 0x01;
        }
        SOFTSPI_SCK_PIN = 1;
        //delay(1);
    }  
    
    #elif SOFTSPI_CPHA==1 && SOFTSPI_CPOL==0
    for(i=0;i<8;i++){
        SOFTSPI_SCK_PIN = 1;
        if(wr_data&0x80){
            SOFTSPI_MOSI_PIN = 1;
        }else{
            SOFTSPI_MOSI_PIN = 0;
        }
        wr_data <<=1;
        SOFTSPI_SCK_PIN = 0;
        datavalue <<=1;
        if(SOFTSPI_MISO_PIN == 1){
            datavalue |= 0x01;
        }
        //delay(1);
    } 
    
    #elif SOFTSPI_CPHA==1 && SOFTSPI_CPOL==1
    for(i=0;i<8;i++){
        SOFTSPI_SCK_PIN = 0;
        if(wr_data&0x80){
            SOFTSPI_MOSI_PIN = 1;
        }else{
             SOFTSPI_MOSI_PIN = 0;
        }
        wr_data <<=1;
        SOFTSPI_SCK_PIN = 1;
        datavalue <<=1;
        if(SOFTSPI_MISO_PIN == 1){
            datavalue |= 0x01;
        }
        SOFTSPI_SCK_PIN = 1;
        // delay(1);
    } 
    #endif
    return datavalue;
}

//========================================================================
// 描述: SPI写一个字节.
// 参数: none.
// 返回: none.
//========================================================================
void softspi_write_byte(uint8 out)
{
    softspi_wr_data(out);
}

//========================================================================
// 描述: SPI读一个字节.
// 参数: none.
// 返回: none.
//========================================================================
uint8 softspi_read_byte(void)
{
    return softspi_wr_data(0xff);;
}

#endif 