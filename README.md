# 天问Block驱动库

## 天问Blcok
  
一站式的开发工具，通过简单易用的图形化模式和代码模式编程，让开发变得简单和高效。天问Block主要包括项目创建和云保存，代码编辑，调试配置，程序下载和调试等功能，结合图形化、代码编程以及丰富的软件资源，减少重复工作，提高开发效率。
![twenblock](/about/twenblock.jpg)

天问Block官方网站
http://twen51.com/

天问51官方淘宝店：
https://item.taobao.com/item.htm?id=625872295830

技术支持QQ群一：1138055784

微信公众号：

![wechat](/about/wechat.jpg)

## 天问Block驱动库
现已支持STC全系列单片机，STC89/90、STC12 系列、STC15 系列、STC8 系列，STC16 系列，CH32V、TWEN32、CH57X、TWEN-ASR等32位RSIC-V内核芯片。

天问51硬件和驱动全部开源，可以商业，为了更好的推广天问Block，代码头部请保留天问相关信息。

## 驱动库文件格式说明
为了方便调用编译器编译代码，天问Block的底层驱动库都是头文件的形式提供，而不是xxx.c和xxx.h两个文件组成，即只需要把原本xxx.c里的函数直接放到xxx.h就可以。

## 代码风格约定
为了保持代码风格一致，我们做如下约定。

### 驱动名
驱动名称可以使用芯片型号或者具体功能命名，和单片机直接相关的比如ADC/PWM/TIN/UART等直接用大写，其它一律用小写，多个单词之间用下划线“_”分隔。针对C51、STC12、STC15请加型号前缀，方便和云上服务器里的驱动库做区分。

### 变量

#### 变量类型
每个单片机类型的头文件里都已经重定义了变量类型，详见如下，
```c
typedef unsigned char   uint8;	//  8 bits 
typedef unsigned int  	uint16;	// 16 bits 
typedef unsigned long   uint32;	// 32 bits 

typedef signed char     int8;	//  8 bits 
typedef signed int      int16;	// 16 bits 
typedef signed long     int32;	// 32 bits 

typedef volatile int8   vint8;	//  8 bits 
typedef volatile int16  vint16;	// 16 bits 
typedef volatile int32  vint32;	// 32 bits 

#define _nop_() __asm nop __endasm
#define code __code
#define  bit __bit
#define interrupt __interrupt
#define xdata __xdata
#define idata __idata
#define using __using
#define at __at
#define  asm __asm
#define endasm  __endasm

```
#### 变量命名
全小写字母，单词之间用下划线“_”分隔，需要加上模块名称前缀。

### 宏定义
全部大写字母，单词之间用下划线“_”分隔，需要加上模块名称前缀。

### 函数
全小写字母，单词之间用下划线“_”分隔，需要加上模块名称前缀。

### 注释
写清楚参数范围，方便调用者使用。
```c
//========================================================================
// 描述: xxxx.
// 参数: none.
// 返回: none.
//========================================================================
```
### 函数
全小写字母，单词之间用下划线“_”分隔，需要加上模块名称前缀。

### 范例

比如：xxx.h
```c
#ifndef __XXX_H
#define __XXX_H

//引用芯片头文件 
//C51: #include <reg52.h>
//STC12:#include <STC12X.h>
//STC15:#include <STC15X.h>
//STC8:#include <STC8HX.h>
//STC16: #include <stc16f.h>
#include <xxxxx.h>

//宏定义
#define XXX_PORT         P6  

//模块内部全局变量
uint8 _xxx_count = 0;

//模块供外部调用全局变量
uint8 xxx_flag = 0;

//========================================================================
// 描述: xxx初始化.
// 参数: none.
// 返回: none.
//========================================================================
void xxx_init(void)
{

}

#endif
```
