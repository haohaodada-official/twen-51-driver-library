#ifndef _UART_H_
#define _UART_H_

#include <STC12X.h>

#ifndef SYS_CLK 
#define SYS_CLK 11059200L
#endif

#define	UART1_CLEAR_RX_FLAG (SCON  &= ~0x01)
#define	UART2_CLEAR_RX_FLAG (S2CON &= ~0x01)

#define	UART1_CLEAR_TX_FLAG (SCON  &= ~0x02)
#define	UART2_CLEAR_TX_FLAG (S2CON &= ~0x02)

#define UART1_GET_RX_FLAG   (SCON  & 0x01)
#define UART2_GET_RX_FLAG   (S2CON & 0x01)

#define UART1_GET_TX_FLAG   (SCON  & 0x02)
#define UART2_GET_TX_FLAG   (S2CON & 0x02)

//此枚举定义不允许用户修改
typedef enum    // 枚举ADC通道
{
	TIM_BRT,
	TIM_1,
}UART_TIMN;

//此枚举定义不允许用户修改
typedef enum //枚举串口号
{
	UART_1,
	UART_2,
}UART_Name;

//此枚举定义不允许用户修改
typedef enum //枚举串口引脚
{
	UART1_RX_P30, UART1_TX_P31,		//只能使用同一行的RX和TX引脚号。不允许混用
	UART2_RX_P12, UART2_TX_P13,
	UART2_RX_P42, UART2_TX_P43,
}UART_PIN;

//11059200频率下 SM0D为0不加倍情况下，波特率范围为115200 57600 9600 4800 2400
void uart_init(UART_Name uart_n, UART_PIN uart_rx_pin, UART_PIN uart_tx_pin, uint32 baud, UART_TIMN tim_n)
{
	switch (uart_n)
	{
		case UART_1:
		{
            SCON = 0x50;           //8-bit variable UART
            if (TIM_1 == tim_n)
			{
                TMOD &= 0x0f;
                TMOD |= 0x20;            //Set Timer1 as 8-bit auto reload mode
                TH1 = TL1 = -(SYS_CLK/32/baud); //Set auto-reload vaule
                TR1 = 1;                //Timer1 start run
                AUXR = 0x40;            //1T mode
            }
            else if (TIM_BRT == tim_n)
			{
                BRT = -(SYS_CLK/32/baud);  //Set auto-reload vaule of baudrate generator
                AUXR = 0x15;            //Baudrate generator work in 1T mode
            }
					
			if ((UART1_RX_P30 == uart_rx_pin) && (UART1_TX_P31 == uart_tx_pin))
			{
				P3M1&=~0x01;P3M0&=~0x01;//P30双向IO口
  				P3M1&=~0x02;P3M0&=~0x02;//P31双向IO口
			}
	
			break;
		}
		case UART_2:
		{
			S2CON = 0x50;           //8-bit variable UART
			BRT = -(SYS_CLK/32/baud);  //Set auto-reload vaule of baudrate generator
			AUXR = 0x14;            //Baudrate generator work in 1T mode
			
			AUXR1 &= ~(0x01 << 4);
			if ((UART2_RX_P12 == uart_rx_pin) && (UART2_TX_P13 == uart_tx_pin))
			{
				P1M1&=~0x04;P1M0&=~0x04;//双向IO口
			    P1M1&=~0x08;P1M0&=~0x08;//双向IO口
			}
			else if ((UART2_RX_P42 == uart_rx_pin) && (UART2_TX_P43 == uart_tx_pin))
			{
				P4M1&=~0x04;P4M0&=~0x04;//双向IO口
				P4M1&=~0x08;P4M0&=~0x08;//双向IO口
				AUXR1 |= (0x01 << 4);
			}

			break;
		}
	}
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      串口字节输出
//  @param      uart_n          串口模块号(USART_1,USART_2)
//  @param      dat             需要发送的字节
//  @return     void        
//  Sample usage:               uart_putchar(UART_1,0xA5);       // 串口1发送0xA5
//-------------------------------------------------------------------------------------------------------------------
void uart_putchar(UART_Name uart_n, uint8 dat)
{
	switch (uart_n)
	{
		case UART_1:
			SBUF = dat;
			while (UART1_GET_TX_FLAG == 0);
			UART1_CLEAR_TX_FLAG;
			break;
		case UART_2:
			S2BUF = dat;
			while (UART2_GET_TX_FLAG == 0);
			UART2_CLEAR_TX_FLAG;
			break;
	}
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      串口发送数组
//  @param      uart_n          串口模块号(USART_1,USART_2)
//  @param      *buff           要发送的数组地址
//  @param      len             发送长度
//  @return     void
//  Sample usage:               uart_putbuff(UART_1,&a[0],5);
//-------------------------------------------------------------------------------------------------------------------
void uart_putbuff(UART_Name uart_n, uint8* p, uint16 len)
{
	while (len--)
		uart_putchar(uart_n, *p++);
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      串口发送字符串
//  @param      uart_n          串口模块号(USART_1,USART_2,USART_3,USART_4)
//  @param      *str            要发送的字符串地址
//  @return     void
//  Sample usage:               uart_putstr(UART_1,"i lvoe you"); 
//-------------------------------------------------------------------------------------------------------------------
void uart_putstr(UART_Name uart_n, uint8* str)
{
	while (*str)
	{
		uart_putchar(uart_n, *str++);
	}
}

#endif /* _STC12_H_ */

