# This file is maked by run generate_makefile.lua
OBJS += build/objs/asrpro_init.o
-include build/objs/asrpro_init.d
build/objs/asrpro_init.o : $(ROOT_DIR)/startup/asrpro_init.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asrpro_vtable.o
-include build/objs/asrpro_vtable.d
build/objs/asrpro_vtable.o : $(ROOT_DIR)/startup/asrpro_vtable.S
	$(CC_PREFIX)$(AS) $(S_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asrpro_startup.o
-include build/objs/asrpro_startup.d
build/objs/asrpro_startup.o : $(ROOT_DIR)/startup/asrpro_startup.S
	$(CC_PREFIX)$(AS) $(S_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asrpro_it.o
-include build/objs/asrpro_it.d
build/objs/asrpro_it.o : $(ROOT_DIR)/system/asrpro_it.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asrpro_system.o
-include build/objs/asrpro_system.d
build/objs/asrpro_system.o : $(ROOT_DIR)/system/asrpro_system.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/platform_config.o
-include build/objs/platform_config.d
build/objs/platform_config.o : $(ROOT_DIR)/system/platform_config.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asrpro_handlers.o
-include build/objs/asrpro_handlers.d
build/objs/asrpro_handlers.o : $(ROOT_DIR)/system/asrpro_handlers.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/baudrate_calibrate.o
-include build/objs/baudrate_calibrate.d
build/objs/baudrate_calibrate.o : $(ROOT_DIR)/system/baudrate_calibrate.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/debug_time_consuming.o
-include build/objs/debug_time_consuming.d
build/objs/debug_time_consuming.o : $(ROOT_DIR)/components/assist/debug_time_consuming.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/croutine.o
-include build/objs/croutine.d
build/objs/croutine.o : $(ROOT_DIR)/components/freertos/croutine.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/event_groups.o
-include build/objs/event_groups.d
build/objs/event_groups.o : $(ROOT_DIR)/components/freertos/event_groups.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/list.o
-include build/objs/list.d
build/objs/list.o : $(ROOT_DIR)/components/freertos/list.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/queue.o
-include build/objs/queue.d
build/objs/queue.o : $(ROOT_DIR)/components/freertos/queue.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/stream_buffer.o
-include build/objs/stream_buffer.d
build/objs/stream_buffer.o : $(ROOT_DIR)/components/freertos/stream_buffer.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/tasks.o
-include build/objs/tasks.d
build/objs/tasks.o : $(ROOT_DIR)/components/freertos/tasks.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/timers.o
-include build/objs/timers.d
build/objs/timers.o : $(ROOT_DIR)/components/freertos/timers.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/heap_3.o
-include build/objs/heap_3.d
build/objs/heap_3.o : $(ROOT_DIR)/components/freertos/portable/MemMang/heap_3.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/ci_log.o
-include build/objs/ci_log.d
build/objs/ci_log.o : $(ROOT_DIR)/components/log/ci_log.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/ci_flash_data_info.o
-include build/objs/ci_flash_data_info.d
build/objs/ci_flash_data_info.o : $(ROOT_DIR)/components/flash_control/flash_control_src/ci_flash_data_info.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/flash_control_inner_port.o
-include build/objs/flash_control_inner_port.d
build/objs/flash_control_inner_port.o : $(ROOT_DIR)/components/flash_control/flash_control_src/flash_control_inner_port.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/audio_play_api.o
-include build/objs/audio_play_api.d
build/objs/audio_play_api.o : $(ROOT_DIR)/components/player/audio_play/audio_play_api.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/audio_play_decoder.o
-include build/objs/audio_play_decoder.d
build/objs/audio_play_decoder.o : $(ROOT_DIR)/components/player/audio_play/audio_play_decoder.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/audio_play_process.o
-include build/objs/audio_play_process.d
build/objs/audio_play_process.o : $(ROOT_DIR)/components/player/audio_play/audio_play_process.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/audio_play_os_port.o
-include build/objs/audio_play_os_port.d
build/objs/audio_play_os_port.o : $(ROOT_DIR)/components/player/audio_play/audio_play_os_port.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/audio_play_device.o
-include build/objs/audio_play_device.d
build/objs/audio_play_device.o : $(ROOT_DIR)/components/player/audio_play/audio_play_device.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/get_play_data.o
-include build/objs/get_play_data.d
build/objs/get_play_data.o : $(ROOT_DIR)/components/player/audio_play/get_play_data.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/adpcmdec.o
-include build/objs/adpcmdec.d
build/objs/adpcmdec.o : $(ROOT_DIR)/components/player/adpcm/adpcmdec.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/adpcm.o
-include build/objs/adpcm.d
build/objs/adpcm.o : $(ROOT_DIR)/components/player/adpcm/adpcm.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/parse_m4a_atom_containers_port.o
-include build/objs/parse_m4a_atom_containers_port.d
build/objs/parse_m4a_atom_containers_port.o : $(ROOT_DIR)/components/player/m4a/parse_m4a_atom_containers_port.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/parse_m4a_atom_containers.o
-include build/objs/parse_m4a_atom_containers.d
build/objs/parse_m4a_atom_containers.o : $(ROOT_DIR)/components/player/m4a/parse_m4a_atom_containers.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/bitstreamf.o
-include build/objs/bitstreamf.d
build/objs/bitstreamf.o : $(ROOT_DIR)/components/player/flacdec/bitstreamf.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/flacdecoder.o
-include build/objs/flacdecoder.d
build/objs/flacdecoder.o : $(ROOT_DIR)/components/player/flacdec/flacdecoder.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/tables.o
-include build/objs/tables.d
build/objs/tables.o : $(ROOT_DIR)/components/player/flacdec/tables.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/sonic.o
-include build/objs/sonic.d
build/objs/sonic.o : $(ROOT_DIR)/components/player/sonic/sonic.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/status_share.o
-include build/objs/status_share.d
build/objs/status_share.o : $(ROOT_DIR)/components/status_share/status_share.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/ci_nvdata_manage.o
-include build/objs/ci_nvdata_manage.d
build/objs/ci_nvdata_manage.o : $(ROOT_DIR)/components/ci_nvdm/ci_nvdata_manage.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/ci_nvdata_port.o
-include build/objs/ci_nvdata_port.d
build/objs/ci_nvdata_port.o : $(ROOT_DIR)/components/ci_nvdm/ci_nvdata_port.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/command_file_reader.o
-include build/objs/command_file_reader.d
build/objs/command_file_reader.o : $(ROOT_DIR)/components/cmd_info/command_file_reader.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/command_info.o
-include build/objs/command_info.d
build/objs/command_info.o : $(ROOT_DIR)/components/cmd_info/command_info.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/prompt_player.o
-include build/objs/prompt_player.d
build/objs/prompt_player.o : $(ROOT_DIR)/components/cmd_info/prompt_player.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/audio_in_manage_inner.o
-include build/objs/audio_in_manage_inner.d
build/objs/audio_in_manage_inner.o : $(ROOT_DIR)/components/audio_in_manage/audio_in_manage_inner.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asr_malloc_port.o
-include build/objs/asr_malloc_port.d
build/objs/asr_malloc_port.o : $(ROOT_DIR)/components/asr/asr_malloc_port.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asr_process_callback_decoder.o
-include build/objs/asr_process_callback_decoder.d
build/objs/asr_process_callback_decoder.o : $(ROOT_DIR)/components/asr/asr_process_callback_decoder.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asr_process_callback.o
-include build/objs/asr_process_callback.d
build/objs/asr_process_callback.o : $(ROOT_DIR)/components/asr/asr_process_callback.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/codec_manager.o
-include build/objs/codec_manager.d
build/objs/codec_manager.o : $(ROOT_DIR)/components/codec_manager/codec_manager.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/codec_manage_inner_port.o
-include build/objs/codec_manage_inner_port.d
build/objs/codec_manage_inner_port.o : $(ROOT_DIR)/components/codec_manager/codec_manage_inner_port.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/voice_module_uart_protocol_g1.o
-include build/objs/voice_module_uart_protocol_g1.d
build/objs/voice_module_uart_protocol_g1.o : $(ROOT_DIR)/components/msg_com/voice_module_uart_protocol_g1.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/i2c_protocol_module.o
-include build/objs/i2c_protocol_module.d
build/objs/i2c_protocol_module.o : $(ROOT_DIR)/components/msg_com/i2c_protocol_module.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/voice_module_uart_protocol.o
-include build/objs/voice_module_uart_protocol.d
build/objs/voice_module_uart_protocol.o : $(ROOT_DIR)/components/msg_com/voice_module_uart_protocol.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/color_light_control.o
-include build/objs/color_light_control.d
build/objs/color_light_control.o : $(ROOT_DIR)/components/led/color_light_control.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/led_light_control.o
-include build/objs/led_light_control.d
build/objs/led_light_control.o : $(ROOT_DIR)/components/led/led_light_control.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asrpro_audio_pre_rslt_out.o
-include build/objs/asrpro_audio_pre_rslt_out.d
build/objs/asrpro_audio_pre_rslt_out.o : $(ROOT_DIR)/components/audio_pre_rslt_iis_out/asrpro_audio_pre_rslt_out.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asrpro_core_eclic.o
-include build/objs/asrpro_core_eclic.d
build/objs/asrpro_core_eclic.o : $(ROOT_DIR)/driver/asrpro_chip_driver/src/asrpro_core_eclic.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asrpro_core_timer.o
-include build/objs/asrpro_core_timer.d
build/objs/asrpro_core_timer.o : $(ROOT_DIR)/driver/asrpro_chip_driver/src/asrpro_core_timer.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asrpro_dma.o
-include build/objs/asrpro_dma.d
build/objs/asrpro_dma.o : $(ROOT_DIR)/driver/asrpro_chip_driver/src/asrpro_dma.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asrpro_codec.o
-include build/objs/asrpro_codec.d
build/objs/asrpro_codec.o : $(ROOT_DIR)/driver/asrpro_chip_driver/src/asrpro_codec.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asrpro_pdm.o
-include build/objs/asrpro_pdm.d
build/objs/asrpro_pdm.o : $(ROOT_DIR)/driver/asrpro_chip_driver/src/asrpro_pdm.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asrpro_alc.o
-include build/objs/asrpro_alc.d
build/objs/asrpro_alc.o : $(ROOT_DIR)/driver/asrpro_chip_driver/src/asrpro_alc.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asrpro_iisdma.o
-include build/objs/asrpro_iisdma.d
build/objs/asrpro_iisdma.o : $(ROOT_DIR)/driver/asrpro_chip_driver/src/asrpro_iisdma.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asrpro_core_misc.o
-include build/objs/asrpro_core_misc.d
build/objs/asrpro_core_misc.o : $(ROOT_DIR)/driver/asrpro_chip_driver/src/asrpro_core_misc.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asrpro_iis.o
-include build/objs/asrpro_iis.d
build/objs/asrpro_iis.o : $(ROOT_DIR)/driver/asrpro_chip_driver/src/asrpro_iis.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asrpro_adc.o
-include build/objs/asrpro_adc.d
build/objs/asrpro_adc.o : $(ROOT_DIR)/driver/asrpro_chip_driver/src/asrpro_adc.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asrpro_gpio.o
-include build/objs/asrpro_gpio.d
build/objs/asrpro_gpio.o : $(ROOT_DIR)/driver/asrpro_chip_driver/src/asrpro_gpio.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asrpro_iic.o
-include build/objs/asrpro_iic.d
build/objs/asrpro_iic.o : $(ROOT_DIR)/driver/asrpro_chip_driver/src/asrpro_iic.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asrpro_pwm.o
-include build/objs/asrpro_pwm.d
build/objs/asrpro_pwm.o : $(ROOT_DIR)/driver/asrpro_chip_driver/src/asrpro_pwm.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asrpro_timer.o
-include build/objs/asrpro_timer.d
build/objs/asrpro_timer.o : $(ROOT_DIR)/driver/asrpro_chip_driver/src/asrpro_timer.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asrpro_uart.o
-include build/objs/asrpro_uart.d
build/objs/asrpro_uart.o : $(ROOT_DIR)/driver/asrpro_chip_driver/src/asrpro_uart.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asrpro_lowpower.o
-include build/objs/asrpro_lowpower.d
build/objs/asrpro_lowpower.o : $(ROOT_DIR)/driver/asrpro_chip_driver/src/asrpro_lowpower.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asrpro_iwdg.o
-include build/objs/asrpro_iwdg.d
build/objs/asrpro_iwdg.o : $(ROOT_DIR)/driver/asrpro_chip_driver/src/asrpro_iwdg.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asrpro_spiflash.o
-include build/objs/asrpro_spiflash.d
build/objs/asrpro_spiflash.o : $(ROOT_DIR)/driver/asrpro_chip_driver/src/asrpro_spiflash.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asrpro_dtrflash.o
-include build/objs/asrpro_dtrflash.d
build/objs/asrpro_dtrflash.o : $(ROOT_DIR)/driver/asrpro_chip_driver/src/asrpro_dtrflash.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/board.o
-include build/objs/board.d
build/objs/board.o : $(ROOT_DIR)/driver/boards/board.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/board_default.o
-include build/objs/board_default.d
build/objs/board_default.o : $(ROOT_DIR)/driver/boards/board_default.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/dichotomy_find.o
-include build/objs/dichotomy_find.d
build/objs/dichotomy_find.o : $(ROOT_DIR)/utils/dichotomy_find.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/crc.o
-include build/objs/crc.d
build/objs/crc.o : $(ROOT_DIR)/utils/crc.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/main.o
-include build/objs/main.d
build/objs/main.o : $(ROOT_DIR)/projects/cwsl_sample/src/main.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/system_hook.o
-include build/objs/system_hook.d
build/objs/system_hook.o : $(ROOT_DIR)/projects/cwsl_sample/src/system_hook.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/system_msg_deal.o
-include build/objs/system_msg_deal.d
build/objs/system_msg_deal.o : $(ROOT_DIR)/projects/cwsl_sample/src/system_msg_deal.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/user_msg_deal.o
-include build/objs/user_msg_deal.d
build/objs/user_msg_deal.o : $(ROOT_DIR)/projects/cwsl_sample/src/user_msg_deal.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/ci_ssp_config.o
-include build/objs/ci_ssp_config.d
build/objs/ci_ssp_config.o : $(ROOT_DIR)/projects/cwsl_sample/src/ci_ssp_config.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/cwsl_app_sample1.o
-include build/objs/cwsl_app_sample1.d
build/objs/cwsl_app_sample1.o : $(ROOT_DIR)/projects/cwsl_sample/src/cwsl_app_sample1.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/wiring.o
-include build/objs/wiring.d
build/objs/wiring.o : $(ROOT_DIR)/projects/cwsl_sample/src/wiring.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/dtostrf.o
-include build/objs/dtostrf.d
build/objs/dtostrf.o : $(ROOT_DIR)/projects/cwsl_sample/src/CString/dtostrf.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/itoa.o
-include build/objs/itoa.d
build/objs/itoa.o : $(ROOT_DIR)/projects/cwsl_sample/src/CString/itoa.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/HardwareSerial.o
-include build/objs/HardwareSerial.d
build/objs/HardwareSerial.o : $(ROOT_DIR)/projects/cwsl_sample/src/CString/HardwareSerial.cpp
	$(CC_PREFIX)$(CPP) $(CPP_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/Stream.o
-include build/objs/Stream.d
build/objs/Stream.o : $(ROOT_DIR)/projects/cwsl_sample/src/CString/Stream.cpp
	$(CC_PREFIX)$(CPP) $(CPP_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/Print.o
-include build/objs/Print.d
build/objs/Print.o : $(ROOT_DIR)/projects/cwsl_sample/src/CString/Print.cpp
	$(CC_PREFIX)$(CPP) $(CPP_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/WString.o
-include build/objs/WString.d
build/objs/WString.o : $(ROOT_DIR)/projects/cwsl_sample/src/CString/WString.cpp
	$(CC_PREFIX)$(CPP) $(CPP_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/ir_remote_driver.o
-include build/objs/ir_remote_driver.d
build/objs/ir_remote_driver.o : $(ROOT_DIR)/components/ir_remote_driver/ir_remote_driver.c
	$(CC_PREFIX)$(CC) $(C_FLAGS) -c -o "$@" "$<"

OBJS += build/objs/asr.o
-include build/objs/asr.d
build/objs/asr.o : $(ROOT_DIR)/../asr.cpp
	$(CC_PREFIX)$(CPP) $(CPP_FLAGS) -c -o "$@" "$<"

LIB_FILES += $(ROOT_DIR)/$(LIBS_PATH)/libasr.a
LD_FLAGS += -L$(ROOT_DIR)/$(LIBS_PATH)
LIBS += -lasr
LIB_FILES += $(ROOT_DIR)/$(LIBS_PATH)/libnewlib_port.a
LD_FLAGS += -L$(ROOT_DIR)/$(LIBS_PATH)
LIBS += -lnewlib_port
LIB_FILES += $(ROOT_DIR)/$(LIBS_PATH)/libfreertos_port.a
LD_FLAGS += -L$(ROOT_DIR)/$(LIBS_PATH)
LIBS += -lfreertos_port
LIB_FILES += $(ROOT_DIR)/$(LIBS_PATH)/libdsu.a
LD_FLAGS += -L$(ROOT_DIR)/$(LIBS_PATH)
LIBS += -ldsu
LIB_FILES += $(ROOT_DIR)/$(LIBS_PATH)/libflash_encrypt.a
LD_FLAGS += -L$(ROOT_DIR)/$(LIBS_PATH)
LIBS += -lflash_encrypt
LIB_FILES += $(ROOT_DIR)/$(LIBS_PATH)/libcwsl.a
LD_FLAGS += -L$(ROOT_DIR)/$(LIBS_PATH)
LIBS += -lcwsl
LIB_FILES += $(ROOT_DIR)/$(LIBS_PATH)/libir_data.a
LD_FLAGS += -L$(ROOT_DIR)/$(LIBS_PATH)
LIBS += -lir_data
C_FLAGS += -I$(ROOT_DIR)/driver/asrpro_chip_driver/inc
CPP_FLAGS += -I$(ROOT_DIR)/driver/asrpro_chip_driver/inc
C_FLAGS += -I$(ROOT_DIR)/driver/boards
CPP_FLAGS += -I$(ROOT_DIR)/driver/boards
C_FLAGS += -I$(ROOT_DIR)/driver/third_device_driver/outside_codec
CPP_FLAGS += -I$(ROOT_DIR)/driver/third_device_driver/outside_codec
C_FLAGS += -I$(ROOT_DIR)/system
CPP_FLAGS += -I$(ROOT_DIR)/system
C_FLAGS += -I$(ROOT_DIR)/components/log
CPP_FLAGS += -I$(ROOT_DIR)/components/log
C_FLAGS += -I$(ROOT_DIR)/components/assist
CPP_FLAGS += -I$(ROOT_DIR)/components/assist
C_FLAGS += -I$(ROOT_DIR)/components/freertos/include
CPP_FLAGS += -I$(ROOT_DIR)/components/freertos/include
C_FLAGS += -I$(ROOT_DIR)/components/freertos/portable/GCC/N307
CPP_FLAGS += -I$(ROOT_DIR)/components/freertos/portable/GCC/N307
C_FLAGS += -I$(ROOT_DIR)/components
CPP_FLAGS += -I$(ROOT_DIR)/components
C_FLAGS += -I$(ROOT_DIR)/components/asr
CPP_FLAGS += -I$(ROOT_DIR)/components/asr
C_FLAGS += -I$(ROOT_DIR)/components/asr/asr_top
CPP_FLAGS += -I$(ROOT_DIR)/components/asr/asr_top
C_FLAGS += -I$(ROOT_DIR)/components/asr/asr_top/asr_top_inc
CPP_FLAGS += -I$(ROOT_DIR)/components/asr/asr_top/asr_top_inc
C_FLAGS += -I$(ROOT_DIR)/components/asr/decoder/decoder_inc
CPP_FLAGS += -I$(ROOT_DIR)/components/asr/decoder/decoder_inc
C_FLAGS += -I$(ROOT_DIR)/components/asr/vad_fe
CPP_FLAGS += -I$(ROOT_DIR)/components/asr/vad_fe
C_FLAGS += -I$(ROOT_DIR)/components/asr/vad_fe/vad_fe_inc
CPP_FLAGS += -I$(ROOT_DIR)/components/asr/vad_fe/vad_fe_inc
C_FLAGS += -I$(ROOT_DIR)/components/asr/dnn
CPP_FLAGS += -I$(ROOT_DIR)/components/asr/dnn
C_FLAGS += -I$(ROOT_DIR)/components/asr/dnn/dnn_inc
CPP_FLAGS += -I$(ROOT_DIR)/components/asr/dnn/dnn_inc
C_FLAGS += -I$(ROOT_DIR)/components/asr/cinn/cinn_inc
CPP_FLAGS += -I$(ROOT_DIR)/components/asr/cinn/cinn_inc
C_FLAGS += -I$(ROOT_DIR)/components/asr/npu/npu_inc
CPP_FLAGS += -I$(ROOT_DIR)/components/asr/npu/npu_inc
C_FLAGS += -I$(ROOT_DIR)/components/asr/nn_and_flash
CPP_FLAGS += -I$(ROOT_DIR)/components/asr/nn_and_flash
C_FLAGS += -I$(ROOT_DIR)/components/asr/nn_and_flash/nn_and_flash_inc
CPP_FLAGS += -I$(ROOT_DIR)/components/asr/nn_and_flash/nn_and_flash_inc
C_FLAGS += -I$(ROOT_DIR)/components/fft
CPP_FLAGS += -I$(ROOT_DIR)/components/fft
C_FLAGS += -I$(ROOT_DIR)/components/msg_com
CPP_FLAGS += -I$(ROOT_DIR)/components/msg_com
C_FLAGS += -I$(ROOT_DIR)/components/led
CPP_FLAGS += -I$(ROOT_DIR)/components/led
C_FLAGS += -I$(ROOT_DIR)/components/player/audio_play
CPP_FLAGS += -I$(ROOT_DIR)/components/player/audio_play
C_FLAGS += -I$(ROOT_DIR)/components/player/mp3lib/mp3pub
CPP_FLAGS += -I$(ROOT_DIR)/components/player/mp3lib/mp3pub
C_FLAGS += -I$(ROOT_DIR)/components/player/aaclib/aacpub
CPP_FLAGS += -I$(ROOT_DIR)/components/player/aaclib/aacpub
C_FLAGS += -I$(ROOT_DIR)/components/player/flacdec
CPP_FLAGS += -I$(ROOT_DIR)/components/player/flacdec
C_FLAGS += -I$(ROOT_DIR)/components/player/m4a
CPP_FLAGS += -I$(ROOT_DIR)/components/player/m4a
C_FLAGS += -I$(ROOT_DIR)/components/player/adpcm
CPP_FLAGS += -I$(ROOT_DIR)/components/player/adpcm
C_FLAGS += -I$(ROOT_DIR)/components/status_share
CPP_FLAGS += -I$(ROOT_DIR)/components/status_share
C_FLAGS += -I$(ROOT_DIR)/components/flash_control/flash_control_inc
CPP_FLAGS += -I$(ROOT_DIR)/components/flash_control/flash_control_inc
C_FLAGS += -I$(ROOT_DIR)/components/flash_encrypt
CPP_FLAGS += -I$(ROOT_DIR)/components/flash_encrypt
C_FLAGS += -I$(ROOT_DIR)/components/codec_manager
CPP_FLAGS += -I$(ROOT_DIR)/components/codec_manager
C_FLAGS += -I$(ROOT_DIR)/components/ci_nvdm
CPP_FLAGS += -I$(ROOT_DIR)/components/ci_nvdm
C_FLAGS += -I$(ROOT_DIR)/components/cmd_info
CPP_FLAGS += -I$(ROOT_DIR)/components/cmd_info
C_FLAGS += -I$(ROOT_DIR)/components/sys_monitor
CPP_FLAGS += -I$(ROOT_DIR)/components/sys_monitor
C_FLAGS += -I$(ROOT_DIR)/components/ota
CPP_FLAGS += -I$(ROOT_DIR)/components/ota
C_FLAGS += -I$(ROOT_DIR)/components/audio_pre_rslt_iis_out
CPP_FLAGS += -I$(ROOT_DIR)/components/audio_pre_rslt_iis_out
C_FLAGS += -I$(ROOT_DIR)/components/audio_in_manage
CPP_FLAGS += -I$(ROOT_DIR)/components/audio_in_manage
C_FLAGS += -I$(ROOT_DIR)/components/assist/SEGGER
CPP_FLAGS += -I$(ROOT_DIR)/components/assist/SEGGER
C_FLAGS += -I$(ROOT_DIR)/components/assist/SEGGER/config
CPP_FLAGS += -I$(ROOT_DIR)/components/assist/SEGGER/config
C_FLAGS += -I$(ROOT_DIR)/components/nuclear_com
CPP_FLAGS += -I$(ROOT_DIR)/components/nuclear_com
C_FLAGS += -I$(ROOT_DIR)/components/audio_pre_rslt_iis_out
CPP_FLAGS += -I$(ROOT_DIR)/components/audio_pre_rslt_iis_out
C_FLAGS += -I$(ROOT_DIR)/components/ci_cwsl
CPP_FLAGS += -I$(ROOT_DIR)/components/ci_cwsl
C_FLAGS += -I$(ROOT_DIR)/projects/cwsl_sample/src
CPP_FLAGS += -I$(ROOT_DIR)/projects/cwsl_sample/src
C_FLAGS += -I$(ROOT_DIR)/utils
CPP_FLAGS += -I$(ROOT_DIR)/utils
C_FLAGS += -I$(ROOT_DIR)/projects/cwsl_sample/src
CPP_FLAGS += -I$(ROOT_DIR)/projects/cwsl_sample/src
C_FLAGS += -I$(ROOT_DIR)/projects/cwsl_sample/src/CString
CPP_FLAGS += -I$(ROOT_DIR)/projects/cwsl_sample/src/CString
C_FLAGS += -I$(ROOT_DIR)/../myLib
CPP_FLAGS += -I$(ROOT_DIR)/../myLib
C_FLAGS += -I$(ROOT_DIR)/components/alg
CPP_FLAGS += -I$(ROOT_DIR)/components/alg
C_FLAGS += -I$(ROOT_DIR)/components/alg/denoise
CPP_FLAGS += -I$(ROOT_DIR)/components/alg/denoise
C_FLAGS += -I$(ROOT_DIR)/components/alg/alc_auto_switch
CPP_FLAGS += -I$(ROOT_DIR)/components/alg/alc_auto_switch
C_FLAGS += -I$(ROOT_DIR)/components/alg/basic_alg
CPP_FLAGS += -I$(ROOT_DIR)/components/alg/basic_alg
C_FLAGS += -I$(ROOT_DIR)/components/alg/aec
CPP_FLAGS += -I$(ROOT_DIR)/components/alg/aec
C_FLAGS += -I$(ROOT_DIR)/components/alg/beamforming
CPP_FLAGS += -I$(ROOT_DIR)/components/alg/beamforming
C_FLAGS += -I$(ROOT_DIR)/components/alg/dereverb
CPP_FLAGS += -I$(ROOT_DIR)/components/alg/dereverb
C_FLAGS += -I$(ROOT_DIR)/components/alg/doa
CPP_FLAGS += -I$(ROOT_DIR)/components/alg/doa
C_FLAGS += -I$(ROOT_DIR)/components/ir_remote_driver
CPP_FLAGS += -I$(ROOT_DIR)/components/ir_remote_driver
C_FLAGS += -I$(ROOT_DIR)/projects/cwsl_sample/src/ir_src
CPP_FLAGS += -I$(ROOT_DIR)/projects/cwsl_sample/src/ir_src
