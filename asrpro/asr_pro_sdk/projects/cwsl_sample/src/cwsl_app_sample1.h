#ifndef __CWSL_APP_SAMPLE1_H__
#define __CWSL_APP_SAMPLE1_H__

#include "system_msg_deal.h"
#include "cwsl_manage.h"


typedef enum
{
    CWSL_REGISTRATION_WAKE          = 10400,      ///< 命令词：学习唤醒词
    CWSL_REGISTRATION_CMD           = 10401,      ///< 命令词：学习命令词
    CWSL_REGISTER_AGAIN             = 10402,      ///< 命令词：重新学习
    CWSL_EXIT_REGISTRATION          = 10403,      ///< 命令词：退出学习
    CWSL_DELETE_FUNC                = 10404,      ///< 命令词：我要删除
    CWSL_DELETE_WAKE                = 10405,      ///< 命令词：删除唤醒词
    CWSL_DELETE_CMD                 = 10406,      ///< 命令词：删除命令词
    CWSL_EXIT_DELETE                = 10407,      ///< 命令词：退出删除模式
    CWSL_DELETE_ALL                 = 10408,      ///< 命令词：全部删除

    CWSL_REGISTRATION_NEXT          = 10499,      ///< 播报：学习下一个
    CWSL_DATA_ENTERY_SUCCESSFUL     = 10409,      ///< 播报：学习成功
    CWSL_DATA_ENTERY_FAILED         = 10410,      ///< 播报：学习失败
    CWSL_REGISTRATION_SUCCESSFUL    = 10411,      ///< 播报：学习成功
    CWSL_TEMPLATE_FULL              = 10412,      ///< 播报：学习模板超过上限
    CWSL_SPEAK_AGAIN                = 10419,     ///< 播报：请再说一次>
    CWSL_TOO_SHORT                  = 10420,     ///< 播报：语音长度不够，请再说一次>

    CWSL_DELETE_SUCCESSFUL          = 10413,      ///< 播报：删除成功
    CWSL_DELETE_FAILED              = 10414,      ///< 播报：删除失败
    CWSL_DELETING                   = 10415,      ///< 播报：删除中
    CWSL_DONT_FIND_WORD             = 10416,      ///< 播报：找不到删除的词
    CWSL_REGISTRATION_ALL           = 10417,      ///< 播报：学习完成
    CWSL_REG_FAILED                 = 10418,      ///< 播报：学习失败
    CWSL_REG_FAILED_DEFAULT_CMD_CONFLICT = 10421, ///< 播报：与默认指令冲突，请换种说法
}cicwsl_func_index;


////cwsl process ASR message///////////////////////////////////////////////
/**
 * @brief 命令词自学习消息处理函数
 * 
 * @param asr_msg ASR识别结果消息
 * @param cmd_handle 命令词handle
 * @param cmd_id 命令词ID
 * @retval 1 数据有效,消息已处理
 * @retval 0 数据无效,消息未处理
 */
uint32_t cwsl_app_process_asr_msg(sys_msg_asr_data_t *asr_msg, cmd_handle_t *cmd_handle, uint16_t cmd_id);

// cwsl_manage模块复位，用于系统退出唤醒状态时调用
int cwsl_app_reset();

#endif  // __CWSL_APP_SAMPLE1_H__
