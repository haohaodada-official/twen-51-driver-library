

#ifndef __STC15X_H_
#define __STC15X_H_

#define _nop_() __asm nop __endasm
#define code __code
#define  bit __bit
#define interrupt __interrupt
#define xdata __xdata
#define idata __idata
#define using __using
#define at __at
#define  asm __asm
#define endasm  __endasm

#include <compiler.h>

/*  BYTE Register  */
SFR(P0,     0x80);                  /* P0 */
    SBIT(P0_0,  0x80, 0);
    SBIT(P0_1,  0x80, 1);
    SBIT(P0_2,  0x80, 2);
    SBIT(P0_3,  0x80, 3);
    SBIT(P0_4,  0x80, 4);
    SBIT(P0_5,  0x80, 5);
    SBIT(P0_6,  0x80, 6);
    SBIT(P0_7,  0x80, 7);

SFR(SP,     0x81);
SFR(DPL,    0x82);
SFR(DPH,    0x83);
SFR(S4CON,   0x84);
SFR(S4BUF,   0x85);
SFR(PCON,   0x87);
SFR(TCON,   0x88); 
/*  TCON  */ 
    SBIT(IT0,   0x88, 0);
    SBIT(IE0,   0x88, 1);
    SBIT(IT1,   0x88, 2);
    SBIT(IE1,   0x88, 3);
    SBIT(TR0,   0x88, 4);
    SBIT(TF0,   0x88, 5);
    SBIT(TR1,   0x88, 6);
    SBIT(TF1,   0x88, 7);
SFR(TMOD,   0x89);
SFR(TL0,    0x8A);
SFR(TL1,    0x8B);
SFR(TH0,    0x8C);
SFR(TH1,    0x8D);
SFR(AUXR,    0x8E);
SFR(INTCLKO,    0x8F);
/* P1 */
SFR(P1,     0x90);  
    SBIT(P1_0,  0x90, 0);
    SBIT(P1_1,  0x90, 1);
    SBIT(P1_2,  0x90, 2);
    SBIT(P1_3,  0x90, 3);
    SBIT(P1_4,  0x90, 4);
    SBIT(P1_5,  0x90, 5);
    SBIT(P1_6,  0x90, 6);
    SBIT(P1_7,  0x90, 7);
SFR(P1M1, 0x91); 
SFR(P1M0, 0x92);
SFR(P0M1, 0x93);   
SFR(P0M0, 0x94);
SFR(P2M1, 0x95);
SFR(P2M0, 0x96);
SFR(CLK_DIV, 0x97);
SFR(SCON,   0x98);
/*  SCON  */
    SBIT(RI,    0x98, 0);
    SBIT(TI,    0x98, 1);
    SBIT(RB8,   0x98, 2);
    SBIT(TB8,   0x98, 3);
    SBIT(REN,   0x98, 4);
    SBIT(SM2,   0x98, 5);
    SBIT(SM1,   0x98, 6);
    SBIT(SM0,   0x98, 7);
SFR(SBUF,     0x99);
SFR(S2CON,    0x9a);
SFR(S2BUF,    0x9b);
SFR(P1ASF,  0x9d);

SFR(P2,     0xA0); 
/* P2 */
    SBIT(P2_0,  0xA0, 0);
    SBIT(P2_1,  0xA0, 1);
    SBIT(P2_2,  0xA0, 2);
    SBIT(P2_3,  0xA0, 3);
    SBIT(P2_4,  0xA0, 4);
    SBIT(P2_5,  0xA0, 5);
    SBIT(P2_6,  0xA0, 6);
    SBIT(P2_7,  0xA0, 7);
SFR(BUS_SPEED,     0xA1);   
SFR(P_SW1,     0xA2); 
SFR(AUXR1,     0xA2); 
SFR(IE,     0xA8); 
/*  IE   */
    SBIT(EX0,   0xA8, 0);
    SBIT(ET0,   0xA8, 1);
    SBIT(EX1,   0xA8, 2);
    SBIT(ET1,   0xA8, 3);
    SBIT(ES,    0xA8, 4);
    SBIT(EADC,    0xA8, 5);
    SBIT(ELVD,    0xA8, 6);
    SBIT(EA,    0xA8, 7);
    
SFR(SADDR,     0xA9); 
SFR(WKTCL,     0xAA); 
SFR(WKTCH,     0xAB); 
SFR(S3CON,     0xAC); 
SFR(S3BUF,     0xAD); 

SFR(IE2,     0xAF);     

SFR(P3,     0xB0);
/*  P3  */
    SBIT(P3_0,  0xB0, 0);
    SBIT(P3_1,  0xB0, 1);
    SBIT(P3_2,  0xB0, 2);
    SBIT(P3_3,  0xB0, 3);
    SBIT(P3_4,  0xB0, 4);
    SBIT(P3_5,  0xB0, 5);
    SBIT(P3_6,  0xB0, 6);
    SBIT(P3_7,  0xB0, 7);

SFR(P3M1, 0xB1); 
SFR(P3M0, 0xB2);
SFR(P4M1, 0xB3);   
SFR(P4M0, 0xB4);
SFR(IP2, 0xB5);

SFR(IP, 0xB8);
/*  IP   */
    SBIT(PX0,   0xB8, 0);
    SBIT(PT0,   0xB8, 1);
    SBIT(PX1,   0xB8, 2);
    SBIT(PT1,   0xB8, 3);
    SBIT(PS,    0xB8, 4);
    SBIT(PADC,  0xB8, 5);
    SBIT(PLVD,  0xB8, 6);
    SBIT(PPCA,  0xB8, 7);

SFR(SADEN, 0xB9);
SFR(P_SW2, 0xBA);
SFR(ADC_CONTR, 0xBC);
SFR(ADC_RES, 0xBD);
SFR(ADC_RESL, 0xBE);

SFR(P4, 0xC0);
/*  P4  */
    SBIT(P4_0, 0xC0, 0);
    SBIT(P4_1, 0xC0, 1);
    SBIT(P4_2, 0xC0, 2);
    SBIT(P4_3, 0xC0, 3);
    SBIT(P4_4, 0xC0, 4);
    SBIT(P4_5, 0xC0, 5);
    SBIT(P4_6, 0xC0, 6);
    SBIT(P4_7, 0xC0, 7);

SFR(WDT_CONTR, 0xC1);
SFR(IAP_DATA, 0xC2);
SFR(IAP_ADDRH, 0xC3);
SFR(IAP_ADDRL, 0xC4);
SFR(IAP_CMD, 0xC5);
SFR(IAP_TRIG, 0xC6);
SFR(IAP_CONTR, 0xC7);

SFR(P5, 0xC8);
/*  P5  */
    SBIT(P5_0, 0xC8, 0);
    SBIT(P5_1, 0xC8, 1);
    SBIT(P5_2, 0xC8, 2);
    SBIT(P5_3, 0xC8, 3);
    SBIT(P5_4, 0xC8, 4);
    SBIT(P5_5, 0xC8, 5);
    SBIT(P5_6, 0xC8, 6);
    SBIT(P5_7, 0xC8, 7);
SFR(P5M1, 0xC9); 
SFR(P5M0, 0xCA);
SFR(P6M1, 0xCB);   
SFR(P6M0, 0xCC);
SFR(SPSTAT, 0xCD);
SFR(SPCTL, 0xCE);
SFR(SPDAT, 0xCF);

SFR(PSW, 0xD0);
/*  PSW   */
    SBIT(P,     0xD0, 0);
    SBIT(F1,    0xD0, 1);
    SBIT(OV,    0xD0, 2);
    SBIT(RS0,   0xD0, 3);
    SBIT(RS1,   0xD0, 4);
    SBIT(F0,    0xD0, 5);
    SBIT(AC,    0xD0, 6);
    SBIT(CY,    0xD0, 7);
SFR(T4T3M, 0xD1);
SFR(T3T4M, 0xD1);
SFR(T4H, 0xD2);
SFR(T4L, 0xD3);
SFR(T3H, 0xD4);
SFR(T3L, 0xD5);
SFR(T2H, 0xD6);
SFR(T2L, 0xD7);

SFR(CCON, 0xD8);
//PCA
    SBIT(CCF0,     0xD8, 0);
    SBIT(CCF1,    0xD8, 1);
    SBIT(CCF2,    0xD8, 2);
    SBIT(CR,   0xD8, 6);
    SBIT(CF,   0xD8, 7);

SFR(CMOD, 0xD9);
SFR(CL, 0xE9);
SFR(CH, 0xF9);
SFR(CCAPM0, 0xDA);
SFR(CCAPM1, 0xDB);
SFR(CCAPM2, 0xDC);
SFR(CCAP0L, 0xEA);
SFR(CCAP1L, 0xEB);
SFR(CCAP2L, 0xEC);
SFR(CCAP0H, 0xFA);
SFR(CCAP1H, 0xFB);
SFR(CCAP2H, 0xFC);
SFR(PCA_PWM0, 0xF2);
SFR(PCA_PWM1, 0xF3);
SFR(PCA_PWM2, 0xF4);

SFR(PWMCFG, 0xF1);
SFR(PWMCR, 0xF5);
SFR(PWMIF, 0xF6);
SFR(PWMFDCR, 0xF7);


SFR(ACC, 0xE0);
SFR(P7M1, 0xE1);
SFR(P7M0, 0xE2);
SFR(DPS, 0xE3);
SFR(DPL1, 0xE4);
SFR(DPH1, 0xE5);
SFR(CMPCR1, 0xE6);
SFR(CMPCR2, 0xE7);

SFR(P6, 0xE8);
/*  P6  */
    SBIT(P6_0, 0xE8, 0);
    SBIT(P6_1, 0xE8, 1);
    SBIT(P6_2, 0xE8, 2);
    SBIT(P6_3, 0xE8, 3);
    SBIT(P6_4, 0xE8, 4);
    SBIT(P6_5, 0xE8, 5);
    SBIT(P6_6, 0xE8, 6);
    SBIT(P6_7, 0xE8, 7);

SFR(B, 0xF0);

SFR(P7, 0xF8);
/*  P6  */
    SBIT(P7_0, 0xF8, 0);
    SBIT(P7_1, 0xF8, 1);
    SBIT(P7_2, 0xF8, 2);
    SBIT(P7_3, 0xF8, 3);
    SBIT(P7_4, 0xF8, 4);
    SBIT(P7_5, 0xF8, 5);
    SBIT(P7_6, 0xF8, 6);
    SBIT(P7_7, 0xF8, 7);



/* BIT definitions for bits that are not directly accessible */
/* PCON bits */
#define IDL             0x01
#define PD              0x02
#define GF0             0x04
#define GF1             0x08
#define SMOD            0x80

/* TMOD bits */
#define T0_M0           0x01
#define T0_M1           0x02
#define T0_CT           0x04
#define T0_GATE         0x08
#define T1_M0           0x10
#define T1_M1           0x20
#define T1_CT           0x40
#define T1_GATE         0x80

#define T0_MASK         0x0F
#define T1_MASK         0xF0

/* Interrupt numbers: address = (number * 8) + 3 */
#define IE0_VECTOR      0       /* 0x03 external interrupt 0 */
#define TF0_VECTOR      1       /* 0x0b timer 0 */
#define IE1_VECTOR      2       /* 0x13 external interrupt 1 */
#define TF1_VECTOR      3       /* 0x1b timer 1 */
#define SI0_VECTOR      4       /* 0x23 serial port 0 */


//�������⹦�ܼĴ���λ����չRAM����
//������Щ�Ĵ���,���Ƚ�P_SW2��BIT7����Ϊ1,�ſ�������д
//�������⹦�ܼĴ���λ����չRAM����
//������Щ�Ĵ���,���Ƚ�P_SW2��BIT7����Ϊ1,�ſ�������д
//#define PWMC        (*(unsigned int  volatile xdata *)0xfff0)
#define PWMCH       (*(unsigned char volatile xdata *)0xfff0)
#define PWMCL       (*(unsigned char volatile xdata *)0xfff1)
#define PWMCKS      (*(unsigned char volatile xdata *)0xfff2)
//#define PWM2T1      (*(unsigned int  volatile xdata *)0xff00)
#define PWM2T1H     (*(unsigned char volatile xdata *)0xff00)
#define PWM2T1L     (*(unsigned char volatile xdata *)0xff01)
//#define PWM2T2      (*(unsigned int  volatile xdata *)0xff02)
#define PWM2T2H     (*(unsigned char volatile xdata *)0xff02)
#define PWM2T2L     (*(unsigned char volatile xdata *)0xff03)
#define PWM2CR      (*(unsigned char volatile xdata *)0xff04)
//#define PWM3T1      (*(unsigned int  volatile xdata *)0xff10)
#define PWM3T1H     (*(unsigned char volatile xdata *)0xff10)
#define PWM3T1L     (*(unsigned char volatile xdata *)0xff11)
//#define PWM3T2      (*(unsigned int  volatile xdata *)0xff12)
#define PWM3T2H     (*(unsigned char volatile xdata *)0xff12)
#define PWM3T2L     (*(unsigned char volatile xdata *)0xff13)
#define PWM3CR      (*(unsigned char volatile xdata *)0xff14)
//#define PWM4T1      (*(unsigned int  volatile xdata *)0xff20)
#define PWM4T1H     (*(unsigned char volatile xdata *)0xff20)
#define PWM4T1L     (*(unsigned char volatile xdata *)0xff21)
//#define PWM4T2      (*(unsigned int  volatile xdata *)0xff22)
#define PWM4T2H     (*(unsigned char volatile xdata *)0xff22)
#define PWM4T2L     (*(unsigned char volatile xdata *)0xff23)
#define PWM4CR      (*(unsigned char volatile xdata *)0xff24)
//#define PWM5T1      (*(unsigned int  volatile xdata *)0xff30)
#define PWM5T1H     (*(unsigned char volatile xdata *)0xff30)
#define PWM5T1L     (*(unsigned char volatile xdata *)0xff31)
//#define PWM5T2      (*(unsigned int  volatile xdata *)0xff32)
#define PWM5T2H     (*(unsigned char volatile xdata *)0xff32)
#define PWM5T2L     (*(unsigned char volatile xdata *)0xff33)
#define PWM5CR      (*(unsigned char volatile xdata *)0xff34)
//#define PWM6T1      (*(unsigned int  volatile xdata *)0xff40)
#define PWM6T1H     (*(unsigned char volatile xdata *)0xff40)
#define PWM6T1L     (*(unsigned char volatile xdata *)0xff41)
//#define PWM6T2      (*(unsigned int  volatile xdata *)0xff42)
#define PWM6T2H     (*(unsigned char volatile xdata *)0xff42)
#define PWM6T2L     (*(unsigned char volatile xdata *)0xff43)
#define PWM6CR      (*(unsigned char volatile xdata *)0xff44)
//#define PWM7T1      (*(unsigned int  volatile xdata *)0xff50)        
#define PWM7T1H     (*(unsigned char volatile xdata *)0xff50)        
#define PWM7T1L     (*(unsigned char volatile xdata *)0xff51)
//#define PWM7T2      (*(unsigned int  volatile xdata *)0xff52)
#define PWM7T2H     (*(unsigned char volatile xdata *)0xff52)
#define PWM7T2L     (*(unsigned char volatile xdata *)0xff53)
#define PWM7CR      (*(unsigned char volatile xdata *)0xff54)

typedef unsigned char   uint8;	//  8 bits 
typedef unsigned int  	uint16;	// 16 bits 
typedef unsigned long   uint32;	// 32 bits 


typedef signed char     int8;	//  8 bits 
typedef signed int      int16;	// 16 bits 

typedef signed long     int32;	// 32 bits 


typedef volatile int8   vint8;	//  8 bits 
typedef volatile int16  vint16;	// 16 bits 
typedef volatile int32  vint32;	// 32 bits 


typedef volatile uint8  vuint8;	//  8 bits 
typedef volatile uint16 vuint16;	// 16 bits 
typedef volatile uint32 vuint32;	// 32 bits 
typedef unsigned char BYTE;
typedef unsigned int WORD;
typedef unsigned long DWORD;
typedef unsigned char uint8_t;
typedef unsigned long UINT32;

#endif /* _STC15_H_ */

