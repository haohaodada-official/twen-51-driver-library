/*************  技术支持与购买说明    **************
产品主页：http://twen51.com
淘宝搜索：天问51，可购买。基础版，带彩屏标准版，旗舰版
技术支持QQ群三：206225033
******************************************/

#ifndef __STC15_HC595_H
#define __STC15_HC595_H	
	
#include "STC15X.h"
#include "STC15_delay.h"

/*引脚定义*/
#ifndef    HC595_DS
#define    HC595_DS          P0_2        //SI
#endif

#ifndef    HC595_DS_MODE
#define    HC595_DS_MODE     {P0M1&=~0x04;P0M0|=0x04;}	//推挽输出
#endif

#ifndef    HC595_STCP
#define    HC595_STCP         P0_1        //RCK
#endif

#ifndef    HC595_STCP_MODE 
#define    HC595_STCP_MODE    {P0M1&=~0x02;P0M0|=0x02;}	//推挽输出
#endif

#ifndef     HC595_SHCP
#define     HC595_SHCP          P0_0        //SCK
#endif  

#ifndef     HC595_SHCP_MODE
#define     HC595_SHCP_MODE    {P0M1&=~0x01;P0M0|=0x01;}	//推挽输出
#endif

void hc595_init();          //595初始化
void hc595_out_enable();    //595使能输出
void hc595_send_byte(uint8 outdata);    //595发送一个字节数据
void hc595_send_multi_byte(uint8 *ddata,uint16 len);    //595发送多个数据，ddata为发送数据指针，len为长度

//========================================================================
// 描述: 595初始化引脚.
// 参数: none.
// 返回: none.
//========================================================================
void hc595_init()
{	
 	HC595_DS_MODE;        //引脚初始化
    HC595_STCP_MODE;
    HC595_SHCP_MODE;

    HC595_DS = 0;
	HC595_STCP = 0;
	HC595_SHCP = 0;
}

//========================================================================
// 描述: 595使能输出.
// 参数: none.
// 返回: none.
//========================================================================
void hc595_out_enable()
{
    HC595_STCP = 0;
    delay10us();delay10us();
    HC595_STCP = 1;
    delay10us();delay10us();
    HC595_STCP = 0;
}

//========================================================================
// 描述: 595发送8位数据.
// 参数: 8位数据.
// 返回: none.
//========================================================================
void hc595_send_byte(uint8 outdata)
{
    uint8 i;
    for(i=0;i<8;i++)  //将8位数据按位发送，先发高字节再发低字节
	{      
        if((outdata&0x80)==0x80)
	    {
            HC595_DS = 1;
        }
	    else
	    {
            HC595_DS = 0;
        }
        HC595_SHCP = 0; //时钟线低电平
        HC595_SHCP = 1; //时钟线高电平
        outdata = outdata<<1;
    }
}

//========================================================================
// 描述: 595发送数组.
// 参数: 数组地址，数据长度.
// 返回: none.
//========================================================================
void hc595_send_multi_byte(uint8 *ddata,uint16 len)
{
    uint16 i;
    for(i=0;i<len;i++)
	{
        hc595_send_byte(ddata[i]);
    }
    // hc595_out_enable();
}

#endif
